import 'package:familia_mobile/app/pages/home_page/view/home_page.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:fimber/fimber.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'App/pages/login/view/login_view.dart';
import 'app/services/graphql_service/graphql_service.dart';
import 'package:timezone/timezone.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';


var firstPage;
var currentTimezone;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp();
  Fimber.plantTree(DebugTree());

  ByteData tzf = await rootBundle.load('assets/latest_10y.tzf');
  initializeDatabase(tzf.buffer.asUint8List());
  var currentTimeZone = await FlutterNativeTimezone.getLocalTimezone();
  if (currentTimeZone == 'GMT')
    currentTimeZone = 'Europe/London';
  currentTimezone = getLocation(currentTimeZone);
  setLocalLocation(currentTimezone);

  bool isCookieSet = await sessionService.isCookieSet();

  if (isCookieSet)
    firstPage = HomePage();
  else
    firstPage = LoginPage();

  GraphQLService.httpLink = HttpLink('http://274790.w90.wedos.ws/familia-api/public/graphql', defaultHeaders: { 'cookie': null });
  GraphQLService.policies = Policies(
    fetch: FetchPolicy.noCache,
  );

  runApp(
      GraphQLProvider(
        client: graphQLService.client,
        child: MyApp(),
      )
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('sk'),
      ],
      locale: const Locale('sk'),
      theme: ThemeData(
        primarySwatch: Colors.orange,
        primaryColor: Color(0xFF2F4B61),
        primaryColorLight: Color(0x962F4B61),
        accentColor: Color(0xFFF2994B),
        backgroundColor: Color(0xFFF9F7F4),
        textTheme: TextTheme(
          headline2: GoogleFonts.montserrat(fontSize: 24, fontWeight: FontWeight.w800, color: Colors.white),
          headline3: GoogleFonts.montserrat(fontSize: 20, fontWeight: FontWeight.w700, color: Color(0xFF2F4B61)),
          headline4: GoogleFonts.montserrat(fontSize: 18, fontWeight: FontWeight.w700, color: Color(0xFF2F4B61)),
          headline5: GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.w700, color: Color(0xFF2F4B61)),
          bodyText1: GoogleFonts.montserrat(fontSize: 16, fontWeight: FontWeight.w400, color: Color(0x962F4B61))
        )
      ),
      home: firstPage,
    );
  }
}