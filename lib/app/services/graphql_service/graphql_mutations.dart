class Mutations {
  static const LOGIN_USER = 'loginUser';
  static const CREATE_USER = 'createUser';
  static const DELETE_USER_FROM_FIREBASE = 'deleteUserFromFirebase';
  static const UPDATE_USER = 'updateUser';
  static const LOGOUT_USER = 'logoutUser';
  static const CREATE_TWO_WAY_RELATIONSHIP = 'createTwoWayRelationship';
  static const SUCCESS = 'success';
  static const CONFIRM_RELATIONSHIP = 'confirmRelationship';
  static const DELETE_RELATIONSHIP = 'deleteUserRelationship';
  static const DELETE_NOTIFICATION = 'deleteNotification';
  static const UPDATE_TWO_WAY_RELATIONSHIP = 'updateTwoWayRelationship';
  static const CREATE_TASK = 'createTask';
  static const SET_TASK = 'setTask';
  static const DELETE_TASK = 'deleteTask';
  static const CREATE_RECIPE = 'createRecipe';
  static const SET_RECIPE = 'setRecipe';
  static const DELETE_RECIPE = 'deleteUserRecipe';
  static const SHARE_RECIPE = 'shareRecipe';
  static const CREATE_ONE_WAY_RELATIONSHIP = 'createOneWayRelationship';
  static const UPDATE_ONE_WAY_RELATIONSHIP = 'updateOneWayRelationship';
  static const CREATE_SHOPPING_LIST = 'createShoppingList';
  static const SET_SHOPPING_LIST = 'setShoppingList';
  static const SET_SHOPPING_LIST_ITEM_CHECKED_VALUE = 'setShoppingListItemCheckedValue';
  static const DELETE_SHOPPING_LIST = 'deleteShoppingList';
  static const CREATE_CONTACT = 'createContact';
  static const SET_CONTACT = 'setUserContact';
  static const DELETE_CONTACT = 'deleteUserContact';

  static const loginMutation = """ 
      mutation $LOGIN_USER (\$inputData: LoginUserInput) {
          $LOGIN_USER (inputData: \$inputData) {
              success
              errorMessage
              timestamp
              errorCode
              data {
                  id
                  uid
                  firstName
                  lastName
                  birthDate
                  emails {                                                                  
                    email
                  }
                  phoneNumbers {                        
                    phoneNumber
                  }
                  gender
                  street
                  descriptiveNumber
                  routingNumber
                  postcode
                  city
                  country
                  relationshipsFrom {
                      id
                      created
                      userFrom {
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  relationshipsTo {
                      id
                      created
                      userFrom {
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  notifications {
                      id
                      title
                      text
                      name
                      userFrom {
                          id
                          uid
                          state
                          firstName
                          lastName
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      type                
                  }
                  created
                  modified
              }
          }
      }
    """;

  static const createUserMutation = """ 
      mutation $CREATE_USER (\$inputData: CreateUserInput) {
        $CREATE_USER (inputData: \$inputData) {
            success
            errorMessage
            timestamp
            errorCode
            data {
                id
                uid
                state
                firstName
                lastName
                emails {
                    email
                }
                phoneNumbers {               
                    phoneNumber
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                relationshipsFrom {
                      id
                      created
                      userFrom {
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  relationshipsTo {
                      id
                      created
                      userFrom {
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  notifications {
                      id
                      title
                      text
                      name
                      userFrom {
                          id
                          uid
                          state
                          firstName
                          lastName
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      type                
                  }
            }
        }
}
    """;

  static const createTwoWayRelationshipMutation = """
    mutation $CREATE_TWO_WAY_RELATIONSHIP (\$inputData: CreateTwoWayRelationshipInput) {
    $CREATE_TWO_WAY_RELATIONSHIP (inputData: \$inputData) {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            userFrom {
                id
                uid
                state
                firstName
                lastName
                emails {
                    id
                    email
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                phoneNumbers {
                    id
                    phoneNumber
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                relationshipsFrom {
                    id
                    created
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userTo {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    role
                    state
                    side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                    userToGender
                    bio
                    created
                    modified
                }
                relationshipsTo {
                    id
                    created
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userTo {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    role
                    state
                    side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                    userToGender
                    bio
                    created
                    modified
                }
                notifications {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                notificationsFrom {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            userTo {
                id
                uid
                state
                firstName
                lastName
                emails {
                    id
                    email
                    created
                    modified
                }
                phoneNumbers {
                    id
                    phoneNumber
                    created
                    modified
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                relationshipsFrom {
                    id
                    created
                    role
                    state
                    side
                    userToGender
                    bio
                    created
                    modified
                }
                relationshipsTo {
                    id
                    created
                    role
                    state
                    side
                    userToGender
                    bio
                    created
                    modified
                }
                notifications {
                    id
                    title
                    text
                    name
                    type
                    created
                    modified
                }
                notificationsFrom {
                    id
                    title
                    text
                    name
                    type
                    created
                    modified
                }
                created
                modified
            }
            role
            state
            side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
            userToGender
            bio
            created
            modified
        }
    }
}
  """;

  static const confirmRelationshipMutation = """ 
    mutation $CONFIRM_RELATIONSHIP (\$relationshipId: Int!) {
    $CONFIRM_RELATIONSHIP (relationshipId: \$relationshipId) {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            userFrom {
                id
                uid
                state
                firstName
                lastName
                emails {
                    id
                    email
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                phoneNumbers {
                    id
                    phoneNumber
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                notifications {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    created
                    modified
                }
                notificationsFrom {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            userTo {
                id
                uid
                state
                firstName
                lastName
                emails {
                    id
                    email
                    created
                    modified
                }
                phoneNumbers {
                    id
                    phoneNumber
                    created
                    modified
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                notifications {
                    id
                    title
                    text
                    name
                    type
                    created
                    modified
                }
                notificationsFrom {
                    id
                    title
                    text
                    name
                    type
                    created
                    modified
                }
                created
                modified
            }
            role
            state
            side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
            userToGender
            bio
            created
            modified
        }
    }
}
  """;

  static const deleteRelationshipMutation = """ 
    mutation $DELETE_RELATIONSHIP (\$relationshipId: Int!) {
    $DELETE_RELATIONSHIP (relationshipId: \$relationshipId) {
        success
        errorMessage
        timestamp
        errorCode
    }
}
  """;

  static const deleteUserFromFirebaseMutation = """ 
      mutation $DELETE_USER_FROM_FIREBASE (\$email: String!) {
        $DELETE_USER_FROM_FIREBASE (email: \$email) {
            success
            errorMessage
        }
}
    """;

  static const updateUserMutation = """ 
      mutation $UPDATE_USER (\$inputData: UpdateUserInput) {
        $UPDATE_USER (inputData: \$inputData) {
            success
              errorMessage
              timestamp
              errorCode
              data {
                  id
                  uid
                  firstName
                  lastName
                  birthDate
                  emails {                                                                  
                    email
                  }
                  phoneNumbers {                        
                    phoneNumber
                  }
                  gender
                  street
                  descriptiveNumber
                  routingNumber
                  postcode
                  city
                  country
                  relationshipsFrom {
                      id
                      created
                      userFrom {
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  relationshipsTo {
                      id
                      created
                      userFrom {
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  notifications {
                      id
                      title
                      text
                      name
                      userFrom {
                          id
                          uid
                          state
                          firstName
                          lastName
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      type                
                  }
                  created
                  modified
            }
        }
    } 
    """;

  static const logoutUserMutation = """ 
      mutation $LOGOUT_USER {
          $LOGOUT_USER {
              success
              errorMessage
              timestamp
              errorCode
          }
      }
    """;

  static const deleteNotificationMutation = """
  mutation $DELETE_NOTIFICATION (\$notificationId: Int!) {
    $DELETE_NOTIFICATION (notificationId: \$notificationId) {
        success
        errorMessage
        timestamp
        errorCode
      }
  }
  """;

  static const updateTwoWayRelationshipMutation = """
    mutation $UPDATE_TWO_WAY_RELATIONSHIP (\$inputData: UpdateTwoWayRelationshipInput) {
    $UPDATE_TWO_WAY_RELATIONSHIP (inputData: \$inputData) {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            userFrom {
                id
                uid
                state
                firstName
                lastName
                emails {
                    id
                    email
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                phoneNumbers {
                    id
                    phoneNumber
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                relationshipsFrom {
                    id
                    created
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userTo {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    role
                    state
                    side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                    userToGender
                    bio
                    created
                    modified
                }
                relationshipsTo {
                    id
                    created
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userTo {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    role
                    state
                    side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                    userToGender
                    bio
                    created
                    modified
                }
                notifications {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    state
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                notificationsFrom {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    state
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            userTo {
                id
                uid
                state
                firstName
                lastName
                emails {
                    id
                    email
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                phoneNumbers {
                    id
                    phoneNumber
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    created
                    modified
                }
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
                relationshipsFrom {
                    id
                    created
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userTo {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    role
                    state
                    side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                    userToGender
                    bio
                    created
                    modified
                }
                relationshipsTo {
                    id
                    created
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userTo {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    role
                    state
                    side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                    userToGender
                    bio
                    created
                    modified
                }
                notifications {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    state
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                notificationsFrom {
                    id
                    title
                    text
                    name
                    user {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    userFrom {
                        id
                        uid
                        state
                        firstName
                        lastName
                        birthDate
                        gender
                        street
                        descriptiveNumber
                        routingNumber
                        postcode
                        city
                        country
                        created
                        modified
                    }
                    type
                    state
                    sentNotifications {
                        sendTime
                        errorMessage
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            role
            state
            side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
            userToGender
            bio
            created
            modified
        }
    }
}
  """;

  static const createTaskMutation = """
  mutation $CREATE_TASK (\$inputData: CreateTaskInput) {
    $CREATE_TASK (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const setTaskMutation = """
  mutation $SET_TASK (\$inputData: SetTaskInput) {
    $SET_TASK (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const deleteTaskMutation = """
  mutation $DELETE_TASK (\$taskId: Int!) {
    $DELETE_TASK (taskId: \$taskId) {
        success
        errorMessage
        timestamp
        errorCode
      }
  }
  """;

  static const createRecipeMutation = """
  mutation $CREATE_RECIPE (\$inputData: CreateRecipeInput) {
    $CREATE_RECIPE (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const setRecipeMutation = """
  mutation $SET_RECIPE (\$inputData: SetRecipeInput) {
    $SET_RECIPE (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const shareRecipeMutation = """
  mutation $SHARE_RECIPE (\$userRecipeId: Int!, \$uid: String!) {
    $SHARE_RECIPE (userRecipeId: \$userRecipeId, uid: \$uid) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const createOneWayRelationshipMutation = """
    mutation $CREATE_ONE_WAY_RELATIONSHIP (\$inputData: CreateOneWayRelationshipInput) {
        $CREATE_ONE_WAY_RELATIONSHIP (inputData: \$inputData) {
            success
            errorMessage
            timestamp
            errorCode
        }
    }
  """;
  static const updateOneWayRelationshipMutation = """
    mutation $UPDATE_ONE_WAY_RELATIONSHIP (\$inputData: UpdateOneWayRelationshipInput) {
        $UPDATE_ONE_WAY_RELATIONSHIP (inputData: \$inputData) {
            success
            errorMessage
            timestamp
            errorCode
        }
    }
  """;

  static const createShoppingListMutation = """
  mutation $CREATE_SHOPPING_LIST (\$inputData: CreateShoppingListInput) {
    $CREATE_SHOPPING_LIST (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const setShoppingListMutation = """
  mutation $SET_SHOPPING_LIST (\$inputData: SetShoppingListInput) {
    $SET_SHOPPING_LIST (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const deleteRecipeMutation = """
    mutation $DELETE_RECIPE (\$userRecipeId: Int!) {
      $DELETE_RECIPE (userRecipeId: \$userRecipeId) {
          success
          errorMessage
          timestamp
          errorCode
      }
    }
  """;

  static const setShoppingListItemCheckedValue = """
  mutation $SET_SHOPPING_LIST_ITEM_CHECKED_VALUE (\$shoppingListItemId: Int!, \$shoppingListItemCheckedValue: Boolean!) {
    $SET_SHOPPING_LIST_ITEM_CHECKED_VALUE (shoppingListItemId: \$shoppingListItemId, shoppingListItemCheckedValue: \$shoppingListItemCheckedValue) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const deleteShoppingListMutation = """
    mutation $DELETE_SHOPPING_LIST (\$shoppingListId: Int!) {
      $DELETE_SHOPPING_LIST (shoppingListId: \$shoppingListId) {
          success
          errorMessage
          timestamp
          errorCode
      }
    }
  """;

  static const createContactMutation = """
  mutation $CREATE_CONTACT (\$inputData: CreateUserContactInput) {
    $CREATE_CONTACT (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    }
  }
  """;

  static const setContactMutation = """
  mutation $SET_CONTACT (\$inputData: SetUserContactInput) {
    $SET_CONTACT (inputData: \$inputData) {
      success
      errorMessage
      errorCode
    } 
  }
  """;

  static const deleteUserContact = """
    mutation $DELETE_CONTACT (\$userContactId: Int!) {
      $DELETE_CONTACT (userContactId: \$userContactId) {
          success
          errorMessage
          timestamp
          errorCode
      }
    }
  """;

//#region Test

//#endregion

}