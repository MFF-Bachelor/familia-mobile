import '../../pages/common/text_constants.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import '../../pages/common/build_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class GraphGLHelper {
  ///Returns MutationOptions for Mutation widget
  MutationOptions entityMutationOptions(
      BuildContext context,
      {
        @required entityService,
        @required mutationDefinition,
        @required mutationName,
        @required onSuccess(result),
        @required onError(OperationException error)
      }) {
    return MutationOptions(
        document: gql(mutationDefinition),
        onError: onError,
        onCompleted: (result) {
          CustomMutationResult mutationResult = entityService.checkMutationResult(result, mutation: mutationName);
          switch (mutationResult) {
            case CustomMutationResult.SUCCESS:
              onSuccess(result);
              break;
            case CustomMutationResult.FAIL:
              onError(null);
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              break;
            default:
              onError(null);
              if (result == null || (result[mutationName] != null && result[mutationName]['errorMessage'] != null))
                BuildWidgets.showCustomSnackBar(context, error: true, message: result[mutationName]['errorMessage']);
              else
                BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
          }
        }
    );
  }
}