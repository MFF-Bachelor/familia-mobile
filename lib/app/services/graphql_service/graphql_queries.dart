class Queries {
  static const GET_USER = 'getUser';
  static const SUCCESS = 'success';
  static const GET_USER_RELATIONSHIPS = 'getUser';
  static const GET_USER_NOTIFICATIONS = 'getUser';
  static const GET_USER_TASKS = 'getUser';
  static const GET_USER_RECIPES = 'getUser';
  static const GET_RECIPE_CATEGORIES = 'getRecipeCategories';
  static const GET_USER_SHOPPING_LISTS = 'getUser';
  static const GET_USER_CONTACTS = 'getUser';
  static const GET_USER_CALENDAR_DATA = 'getUser';
  static const GET_USER_FAMILY_TREE_DATA = 'getUser';

  static const getUserQuery = """ 
      query $GET_USER (\$uid: String, \$email: String) {
        $GET_USER (uid: \$uid, email: \$email) {
            success
              errorMessage
              timestamp
              errorCode
              data {
                  id
                  uid
                  firstName
                  lastName
                  birthDate
                  emails {                                                                  
                    email
                  }
                  phoneNumbers {                        
                    phoneNumber
                  }
                  gender
                  street
                  descriptiveNumber
                  routingNumber
                  postcode
                  city
                  country
                  relationshipsFrom {
                      id
                      created
                      userFrom {
                          id
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          id
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  relationshipsTo {
                      id
                      created
                      userFrom {
                          id
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          id
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  notifications {
                      id
                      title
                      text
                      name
                      userFrom {
                          id
                          uid
                          state
                          firstName
                          lastName
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      type                
                  }
                  createdShoppingLists {
                      id
                      title
                      description
                      dueTime
                      state
                      created
                      modified
                  }
                  assigneeShoppingLists {
                      id
                      title
                      description
                      dueTime
                      state
                      created
                      modified
                  }
                  created
                  modified
              }
        }
    }
    """;

  static const getUserRelationshipsQuery = """ 
      query $GET_USER_RELATIONSHIPS (\$uid: String, \$email: String) {
        $GET_USER_RELATIONSHIPS (uid: \$uid, email: \$email) {
            success
              errorMessage
              timestamp
              errorCode
              data {
                  relationshipsFrom {
                      id
                      created
                      userFrom {
                          id
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          id
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country 
                          emails {
                            email
                          }                   
                      }
                      inactiveUserTo {
                          id
                          firstName
                          lastName
                          birthDate
                          gender
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  relationshipsTo {
                      id
                      created
                      userFrom {
                          id
                          uid
                          firstName
                          lastName                   
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      userTo {
                          id
                          uid
                          firstName
                          lastName                    
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country                    
                      }
                      role
                      state
                      side
                      inactiveUserTo {
                          id
                          firstName
                          lastName                    
                          birthDate
                          gender
                      }
                      userToGender
                      bio
                  }
                  created
                  modified
              }
        }
    }
    """;

  static const getUserNotificationsQuery = """
  query $GET_USER_NOTIFICATIONS (\$uid: String, \$email: String) {
        $GET_USER_NOTIFICATIONS (uid: \$uid, email: \$email) {
            success
              errorMessage
              timestamp
              errorCode
              data {
                  id
                  uid                  
                  notifications {
                      id
                      title
                      text
                      name
                      userFrom {
                          id
                          uid
                          state
                          firstName
                          lastName
                          birthDate
                          gender
                          street
                          descriptiveNumber
                          routingNumber
                          postcode
                          city
                          country
                      }
                      type 
                      created
                      modified               
                  }
                  created
                  modified
              }
        }
        }
    """;

  static const getUserTasksQuery = """
  query $GET_USER_TASKS (\$uid: String, \$email: String) {
        $GET_USER_TASKS (uid: \$uid, email: \$email) {
    success
     errorMessage
     timestamp
     errorCode
     data {
         id
         uid
         state
         firstName
         lastName
         birthDate
         gender
         street
         descriptiveNumber
         routingNumber
         postcode
         city
         country
         otherCreatedTasks {
             id
             title
             text
             dueTime
             creator {
                 id
                 uid
                 state
                 firstName
                 lastName
                 birthDate
                 gender
                 street
                 descriptiveNumber
                 routingNumber
                 postcode
                 city
                 country
             }
             assignee {
                 id
                 uid
                 firstName
                 lastName
                 birthDate
                 gender
                 street
                 descriptiveNumber
                 routingNumber
                 postcode
                 city
                 country
             }
             state
             priority
             canAssigneeEdit
             created
             modified
         }
         assignedTasks {
             id
             title
             text
             dueTime
             creator {
                id
                uid
                state
                firstName
                lastName
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
             }
             assignee {
                id
                uid
                firstName
                lastName
                birthDate
                gender
                street
                descriptiveNumber
                routingNumber
                postcode
                city
                country
             }
             state
             priority
             canAssigneeEdit
             created
             modified
         }
         created
         modified
     }
  }
  }
  """;

  static const getUserRecipesQuery = """
  query $GET_USER_RECIPES (\$uid: String, \$email: String) {
      $GET_USER_RECIPES (uid: \$uid, email: \$email) {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            uid
            state
            firstName
            lastName
            recipes {
                id
                recipe {
                    id
                    title
                    servings
                    caloriesPerServing
                    source
                    preparationTime
                    preparationTimeUnit
                    difficulty
                    description
                    recipeCategories {
                        id
                        name
                    }
                    recipeIngredients {
                        id
                        amount
                        unit
                        name
                    }
                    recipeInstructions {
                        id
                        description
                        ordering
                    }
                    recipeUsers {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    created
                    modified
                }
                favourite
                rating
                comment
                type
                willCook
                state
            }
            created
            modified
        }
    }
  }
  """;

  static const getRecipeCategories = """
  query $GET_RECIPE_CATEGORIES () {
      $GET_RECIPE_CATEGORIES () {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            recipeSubCategories {
                id
                recipeSubCategories {
                    id
                    name
                }
                parentRecipeCategory {
                    id
                    name
                }
                name
            }
            name
        }
    }
  }
  """;

  static const getUserShoppingListsQuery = """
  query $GET_USER_SHOPPING_LISTS (\$uid: String, \$email: String) {
        $GET_USER_SHOPPING_LISTS (uid: \$uid, email: \$email) {
            success
        errorMessage
        timestamp
        errorCode
        data {
            id
            uid
            state
            firstName
            lastName
            emails {
                id
                email
                user {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            phoneNumbers {
                id
                phoneNumber
                user {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            birthDate
            gender
            street
            descriptiveNumber
            routingNumber
            postcode
            city
            country
            createdShoppingLists {
                id
                title
                description
                dueTime
                creator {
                    id
                    uid
                    firstName
                    lastName                    
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country   
                }
                shoppingListItems {
                    id
                    title
                    shoppingList {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    checked
                    created
                    modified
                }
                assignee {
                    id
                    uid
                    firstName
                    lastName                    
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country   
                }
                state
                created
                modified
            }
            assigneeShoppingLists {
                id
                title
                description
                dueTime
                creator {
                    id
                    uid
                    firstName
                    lastName                    
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country    
                }
                shoppingListItems {
                    id
                    title
                    shoppingList {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    checked
                    created
                    modified
                }
                assignee {
                    id
                    uid
                    firstName
                    lastName                    
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country   
                }
                state
                created
                modified
            }
            created
            modified
        }
        }
        }
    """;

  static const getUserContactsQuery = """ 
  query $GET_USER_CONTACTS (\$uid: String, \$email: String) {
    $GET_USER_CONTACTS (uid: \$uid, email: \$email) {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            uid
            state
            firstName
            lastName
            emails {
                id
                email
                user {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            phoneNumbers {
                id
                phoneNumber
                user {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                created
                modified
            }
            birthDate
            gender
            street
            descriptiveNumber
            routingNumber
            postcode
            city
            country
            contacts {
                id
                user {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                email
                phoneNumber
                label
                name
                state
                created
                modified
            }
            created
            modified
        }
    }
}
""";

  static const getUserCalendarDataQuery = """ 
  query $GET_USER_CALENDAR_DATA (\$uid: String, \$email: String) {
    $GET_USER_CALENDAR_DATA (uid: \$uid, email: \$email) {
        success
        errorMessage
        timestamp
        errorCode
        data {
            id
            uid
            createdShoppingLists {
                id
                title
                description
                dueTime
                created
                modified
            }
            assigneeShoppingLists {
                id
                title
                description
                dueTime
                created
                modified
            }
            assignedTasks {
               id
               title
               text
               dueTime
               creator {
                  id
                  uid
                  state
                  firstName
                  lastName
                  birthDate
                  gender
                  street
                  descriptiveNumber
                  routingNumber
                  postcode
                  city
                  country
               }
               assignee {
                  id
                  uid
                  firstName
                  lastName
                  birthDate
                  gender
                  street
                  descriptiveNumber
                  routingNumber
                  postcode
                  city
                  country
               }
               state
               priority
               canAssigneeEdit
               created
               modified
            }
            created
            modified
        }
    }
}
""";

  static const getUserFamilyTreeData = """ 
      query $GET_USER_FAMILY_TREE_DATA (\$uid: String, \$email: String) {
        $GET_USER_FAMILY_TREE_DATA (uid: \$uid, email: \$email) {
            success
              errorMessage
              timestamp
              errorCode
              data {
            id
            uid
            state
            firstName
            lastName
            birthDate
            gender
            street
            descriptiveNumber
            routingNumber
            postcode
            city
            country
            relationshipsFrom {
                id
                created
                userFrom {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                        userTo {
                            id
                            uid
                            firstName
                            lastName
                            gender   
                            birthDate                          
                        }
                        inactiveUserTo {
                            id
                            firstName
                            lastName
                            gender   
                            birthDate                          
                        }
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    contacts {
                        id
                        email
                        phoneNumber
                        label
                        name
                        state
                        created
                        modified
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                userTo {
                    id
                    uid
                    state
                    firstName
                    lastName
                    emails {
                        id
                        email
                        created
                        modified
                    }
                    phoneNumbers {
                        id
                        phoneNumber
                        created
                        modified
                    }
                    birthDate
                    gender
                    street
                    descriptiveNumber
                    routingNumber
                    postcode
                    city
                    country
                    relationshipsFrom {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                        userTo {
                            id
                            uid
                            firstName
                            lastName
                            gender   
                            birthDate                          
                        }
                        inactiveUserTo {
                            id
                            firstName
                            lastName
                            gender   
                            birthDate                          
                        }
                    }
                    relationshipsTo {
                        id
                        created
                        role
                        state
                        side
                        userToGender
                        bio
                        created
                        modified
                        userTo {
                            id
                            uid
                            firstName
                            lastName
                            gender   
                            birthDate                          
                        }
                        inactiveUserTo {
                            id
                            firstName
                            lastName
                            gender   
                            birthDate                          
                        }
                    }
                    notifications {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    notificationsFrom {
                        id
                        title
                        text
                        name
                        type
                        state
                        created
                        modified
                    }
                    otherCreatedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    recipes {
                        id
                        favourite
                        rating
                        comment
                        type
                        willCook
                        state
                    }
                    contacts {
                        id
                        email
                        phoneNumber
                        label
                        name
                        state
                        created
                        modified
                    }
                    createdShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assigneeShoppingLists {
                        id
                        title
                        description
                        dueTime
                        state
                        created
                        modified
                    }
                    assignedTasks {
                        id
                        title
                        text
                        dueTime
                        state
                        priority
                        canAssigneeEdit
                        created
                        modified
                    }
                    created
                    modified
                }
                role
                state
                side
                inactiveUserTo {
                    id
                    state
                    firstName
                    lastName
                    birthDate
                    gender
                    created
                    modified
                }
                userToGender
                bio
                created
                modified
            }
            created
            modified
          }
        }
    }
    """;
}