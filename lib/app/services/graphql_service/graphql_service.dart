import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class GraphQLService {
  static HttpLink httpLink;

  static Policies policies;

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: GraphQLCache(),
      link: httpLink,
      defaultPolicies: DefaultPolicies(
          watchQuery: policies,
          query: policies,
          mutate: policies
      ),
    ),
  );

  /// Singleton Factory Pattern
  static final GraphQLService _graphqlService = new GraphQLService._internal();

  factory GraphQLService() {
    return _graphqlService;
  }

  GraphQLService._internal();
}

GraphQLService graphQLService = new GraphQLService();