
import 'package:fimber/fimber.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'graphql_service/graphql_service.dart';

class SessionService {
  static final String _cookieCode = 'familia_cookie';
  static final String _setCookie = 'set-cookie';
  static final String _cookie = 'cookie';
  Future<SharedPreferences> _sharedPreferences;

  Future<String> _getCookieFromSharedPreferences() async {
    var result = (await _sharedPreferences).getString(_cookieCode);
    Fimber.d('Cookie from shared preferences: $result');
    return result;
  }

  Future<void> _setCookieToSharedPreferences(String cookies) async {
    Fimber.d('Set cookie to shared preferences: $_cookie');
    (await _sharedPreferences).setString(_cookieCode, cookies);
  }

  ///Removes cookie from shared preferences store
  Future<void> deleteCookieFromSharedPreferences() async {
    Fimber.d('Delete cookie from shared preferences');
    (await _sharedPreferences).remove(_cookieCode);
  }
  ///Saves cookie from API to shared preferences store
  void saveCookieFromApiResponse(QueryResult queryResult) {
    if (queryResult.context.entry<HttpLinkResponseContext>().headers.containsKey(_setCookie)) {
      String cookie = queryResult.context.entry<HttpLinkResponseContext>().headers[_setCookie];
      Fimber.d('Cookie from api response: $_cookie');
      if (cookie.contains("PHPSESSID=deleted;"))
        deleteCookieFromSharedPreferences();
      else {
        setCookieToApiRequest();
        _setCookieToSharedPreferences(cookie);
      }
    }
  }

  ///Returns wheater or not is cookie set in shared preferences
  Future<bool> isCookieSet() async{
    return  (await _getCookieFromSharedPreferences() != null);
  }

  Future<void> setCookieToApiRequest() async {
    String cookie = await _getCookieFromSharedPreferences();
    GraphQLService.httpLink.defaultHeaders[_cookie] = cookie;
    Fimber.d('Cookie to api request: $cookie');
  }

  /// Singleton Factory Pattern
  static final SessionService _sessionService = new SessionService._internal();

  factory SessionService() {
    return _sessionService;
  }

  SessionService._internal() {
    _sharedPreferences = SharedPreferences.getInstance();
  }
}

SessionService sessionService = new SessionService();