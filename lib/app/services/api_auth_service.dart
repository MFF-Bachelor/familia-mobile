import 'package:familia_mobile/app/entities/user.dart';

class ApiAuthService {
  User _currentUser;

  User get currentUser => _currentUser;

  void setCurrentUser(User user) {
    this._currentUser = user;
  }

  /// Singleton Factory Pattern
  static final ApiAuthService _apiAuthService = new ApiAuthService._internal();

  factory ApiAuthService() {
    return _apiAuthService;
  }

  ApiAuthService._internal();
}

ApiAuthService apiAuthService = new ApiAuthService();
