import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class FirebaseService {
  ///Calls FirebaseAuth service and signes in with email and pass
  static Future<User> signInWithEmailAndPass(String email, String pass) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    UserCredential userCredential;

    try {
      userCredential = await auth.signInWithEmailAndPassword(
          email: email,
          password: pass
      );

      return userCredential.user;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
      }
    }

    return null;
  }

  ///Calls FirebaseAuth service and registres with email and pass
  static registrationWithEmailAndPass({@required String email, @required String pass, @required Function onEmailInUse}) async {
    FirebaseAuth auth = FirebaseAuth.instance;
    UserCredential userCredential;

    try {
      userCredential = await auth.createUserWithEmailAndPassword(
          email: email,
          password: pass
      );

      return [true, userCredential.user];
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        onEmailInUse(email);
        return [true, null];
      }
    } catch (e) {
      print(e);
    }
    return [false, null];
  }

  ///Calls FirebaseAuth service and sends email with reset password instruction
  static Future<bool> sendPasswordResetEmail({@required String email}) async {
    FirebaseAuth auth = FirebaseAuth.instance;

    try {
      await auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (e) {
      print(e.code);
      return false;
    }

    return true;
  }
}