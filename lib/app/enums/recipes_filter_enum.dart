enum RecipesFilterEnum {
  EASY,
  MEDIUM,
  HARD,
  FAVOURITE,
  QUICK_PREPARATION,
  HIGH_RATED
}

extension RecipesFilterEnumExtension on RecipesFilterEnum {
  static dynamic get definition {
    return {
      RecipesFilterEnum.EASY: {
        'label': 'Jednoduché',
        'active': false,
      },
      RecipesFilterEnum.MEDIUM: {
        'label': 'Stredne ťažké',
        'active': false,
      },
      RecipesFilterEnum.HARD: {
        'label': 'Ťažké',
        'active': false,
      },
      RecipesFilterEnum.FAVOURITE: {
        'label': 'Obľúbené',
        'active': false,
      },
      RecipesFilterEnum.QUICK_PREPARATION: {
        'label': 'Rýchle (< 30 min.)',
        'active': false,
      },
      RecipesFilterEnum.HIGH_RATED: {
        'label': 'Dobre hodnotené (> 3)',
        'active': false,
      },
    };
  }
}

enum RecipesSortEnum {
  CREATED_DATE_ASC,
  CREATED_DATE_DESC,
  NAME,
  RATING,
  CALORIES,
  DIFFICULTY,
  PREPARATION_TIME
}

extension RecipesSortEnumExtension on RecipesSortEnum {
  static dynamic get definition {
    return {
      RecipesSortEnum.CREATED_DATE_ASC: {
        'label': 'Dátum vytvorenia - vzostupne'
      },
      RecipesSortEnum.CREATED_DATE_DESC: {
        'label': 'Dátum vytvorenia - zostupne'
      },
      RecipesSortEnum.NAME: {
        'label': 'Názov'
      },
      RecipesSortEnum.RATING: {
        'label': 'Hodnotenie'
      },
      RecipesSortEnum.CALORIES: {
        'label': 'Kalórie'
      },
      RecipesSortEnum.DIFFICULTY: {
        'label': 'Náročnosť'
      },
      RecipesSortEnum.PREPARATION_TIME: {
        'label': 'Doba prípravy'
      },
    };
  }
}