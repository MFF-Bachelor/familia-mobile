enum RelationshipSideEnum {
  MATERNAL,
  PATERNAL
}

extension RelationshipSideEnumExtension on RelationshipSideEnum {

  String get title {
    switch (this) {
      case RelationshipSideEnum.PATERNAL:
        return "PATERNAL";
        break;
      case RelationshipSideEnum.MATERNAL:
        return "MATERNAL";
        break;
    }
  }

  String get titleSK {
    switch (this) {
      case RelationshipSideEnum.PATERNAL:
        return "otca";
        break;
      case RelationshipSideEnum.MATERNAL:
        return "matky";
        break;
    }
  }
}