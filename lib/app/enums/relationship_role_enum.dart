import 'package:familia_mobile/app/enums/gender_enum.dart';

enum RelationshipRoleEnum {
  PARENT,
  CHILD,
  UNMARRIED_PARTNER,
  FRIEND,
  PARTNER,
  COUSIN,
  GRAND_PARENT,
  GRAND_GRAND_PARENT,
  PARENT_IN_LAW,
  GRAND_CHILD,
  GRAND_GRAND_CHILD,
  SIBLING,
  STEP_SIBLING,
  SIBLING_IN_LAW,
  NEPHEW_NIECE,
  AUNT_UNCLE
}

extension RelationshipRoleEnumExtension on RelationshipRoleEnum {

  String get title {
    switch (this) {
      case RelationshipRoleEnum.PARENT:
        return "PARENT";
        break;
      case RelationshipRoleEnum.CHILD:
        return "CHILD";
        break;
      case RelationshipRoleEnum.UNMARRIED_PARTNER:
        return "UNMARRIED_PARTNER";
        break;
      case RelationshipRoleEnum.FRIEND:
        return "FRIEND";
        break;
      case RelationshipRoleEnum.PARTNER:
        return "PARTNER";
        break;
      case RelationshipRoleEnum.COUSIN:
        return "COUSIN";
        break;
      case RelationshipRoleEnum.GRAND_PARENT:
        return "GRAND_PARENT";
        break;
      case RelationshipRoleEnum.GRAND_GRAND_PARENT:
        return "GRAND_GRAND_PARENT";
        break;
      case RelationshipRoleEnum.PARENT_IN_LAW:
        return "PARENT_IN_LAW";
        break;
      case RelationshipRoleEnum.GRAND_CHILD:
        return "GRAND_CHILD";
        break;
      case RelationshipRoleEnum.GRAND_GRAND_CHILD:
        return "GRAND_GRAND_CHILD";
        break;
      case RelationshipRoleEnum.SIBLING:
        return "SIBLING";
        break;
      case RelationshipRoleEnum.STEP_SIBLING:
        return "STEP_SIBLING";
        break;
      case RelationshipRoleEnum.SIBLING_IN_LAW:
        return "SIBLING_IN_LAW";
        break;
      case RelationshipRoleEnum.NEPHEW_NIECE:
        return "NEPHEW_NIECE";
        break;
      case RelationshipRoleEnum.AUNT_UNCLE:
        return "AUNT_UNCLE";
        break;
    }
  }

  String get titleSK {
    switch (this) {
      case RelationshipRoleEnum.PARENT:
        return 'RODIČ';
        break;
      case RelationshipRoleEnum.CHILD:
        return 'DIEŤA';
        break;
      case RelationshipRoleEnum.UNMARRIED_PARTNER:
        return 'DRUH ALEBO DRUŽKA';
        break;
      case RelationshipRoleEnum.FRIEND:
        return 'KAMARÁT';
        break;
      case RelationshipRoleEnum.PARTNER:
        return 'MANŽEL ALEBO MANŽELKA';
        break;
      case RelationshipRoleEnum.COUSIN:
        return 'BRATRANEC ALEBO SESTERNICA';
        break;
      case RelationshipRoleEnum.GRAND_PARENT:
        return 'STARÝ RODIČ';
        break;
      case RelationshipRoleEnum.GRAND_GRAND_PARENT:
        return 'PRASTARÝ RODIČ';
        break;
      case RelationshipRoleEnum.PARENT_IN_LAW:
        return 'SVOKOR ALEBO SVOKRA';
        break;
      case RelationshipRoleEnum.GRAND_CHILD:
        return 'VNUK ALEBO VNUČKA';
        break;
      case RelationshipRoleEnum.GRAND_GRAND_CHILD:
        return 'PRAVNUK';
        break;
      case RelationshipRoleEnum.SIBLING:
        return 'SÚRODENEC';
        break;
      case RelationshipRoleEnum.STEP_SIBLING:
        return 'NEVLASTNÝ SÚRODENEC';
        break;
      case RelationshipRoleEnum.SIBLING_IN_LAW:
        return 'ŠVAGOR ALEBO ŠVAGRINÁ';
        break;
      case RelationshipRoleEnum.NEPHEW_NIECE:
        return 'SYNOVEC ALEBO NETER';
        break;
      case RelationshipRoleEnum.AUNT_UNCLE:
        return 'UJO ALEBO TETA';
        break;
    }
  }

  String titleByGender(GenderEnum gender) {
    switch (this) {
      case RelationshipRoleEnum.PARENT:
        if (gender == GenderEnum.MALE)
          return 'Otec';
        else
          return 'Mama';
        break;
      case RelationshipRoleEnum.CHILD:
        if (gender == GenderEnum.MALE)
          return 'Syn';
        else
          return 'Dcéra';
        break;
      case RelationshipRoleEnum.UNMARRIED_PARTNER:
        if (gender == GenderEnum.MALE)
          return 'Druh';
        else
          return 'Družka';
        break;
      case RelationshipRoleEnum.FRIEND:
        if (gender == GenderEnum.MALE)
          return 'Kamarát';
        else
          return 'Kamarátka';
        break;
      case RelationshipRoleEnum.PARTNER:
        if (gender == GenderEnum.MALE)
          return 'Manžel';
        else
          return 'Manželka';
        break;
      case RelationshipRoleEnum.COUSIN:
        if (gender == GenderEnum.MALE)
          return 'Bratranec';
        else
          return 'Sesternica';
        break;
      case RelationshipRoleEnum.GRAND_PARENT:
        if (gender == GenderEnum.MALE)
          return 'Starý otec';
        else
          return 'Stará mama';
        break;
      case RelationshipRoleEnum.GRAND_GRAND_PARENT:
        if (gender == GenderEnum.MALE)
          return 'Prastarý otec';
        else
          return 'Prastará mama';
        break;
      case RelationshipRoleEnum.PARENT_IN_LAW:
        if (gender == GenderEnum.MALE)
          return 'Svokor';
        else
          return 'Svokra';
        break;
      case RelationshipRoleEnum.GRAND_CHILD:
        if (gender == GenderEnum.MALE)
          return 'Vnuk';
        else
          return 'Vnučka';
        break;
      case RelationshipRoleEnum.GRAND_GRAND_CHILD:
        if (gender == GenderEnum.MALE)
          return 'Pravnuk';
        else
          return 'Pravnučka';
        break;
      case RelationshipRoleEnum.SIBLING:
        if (gender == GenderEnum.MALE)
          return 'Brat';
        else
          return 'Sestra';
        break;
      case RelationshipRoleEnum.STEP_SIBLING:
        if (gender == GenderEnum.MALE)
          return 'Nevlastný brat';
        else
          return 'Nevlastná sestra';
        break;
      case RelationshipRoleEnum.SIBLING_IN_LAW:
        if (gender == GenderEnum.MALE)
          return 'Švagor';
        else
          return 'Švagriná';
        break;
      case RelationshipRoleEnum.NEPHEW_NIECE:
        if (gender == GenderEnum.MALE)
          return 'Synovec';
        else
          return 'Neter';
        break;
      case RelationshipRoleEnum.AUNT_UNCLE:
        if (gender == GenderEnum.MALE)
          return 'Ujo';
        else
          return 'Teta';
        break;
    }
  }

  bool get sideValueVisibility {
    switch (this) {
      case RelationshipRoleEnum.PARENT:
        return false;
        break;
      case RelationshipRoleEnum.CHILD:
        return false;
        break;
      case RelationshipRoleEnum.UNMARRIED_PARTNER:
        return false;
        break;
      case RelationshipRoleEnum.FRIEND:
        return false;
        break;
      case RelationshipRoleEnum.PARTNER:
        return false;
        break;
      case RelationshipRoleEnum.COUSIN:
        return true;
        break;
      case RelationshipRoleEnum.GRAND_PARENT:
        return true;
        break;
      case RelationshipRoleEnum.GRAND_GRAND_PARENT:
        return true;
        break;
      case RelationshipRoleEnum.PARENT_IN_LAW:
        return false;
        break;
      case RelationshipRoleEnum.GRAND_CHILD:
        return false;
        break;
      case RelationshipRoleEnum.GRAND_GRAND_CHILD:
        return false;
        break;
      case RelationshipRoleEnum.SIBLING:
        return false;
        break;
      case RelationshipRoleEnum.STEP_SIBLING:
        return false;
        break;
      case RelationshipRoleEnum.SIBLING_IN_LAW:
        return false;
        break;
      case RelationshipRoleEnum.NEPHEW_NIECE:
        return false;
        break;
      case RelationshipRoleEnum.AUNT_UNCLE:
        return true;
        break;
    }
  }

  static List<String>  get enumValues {
    List<String> result = [];
    for (var value in RelationshipRoleEnum.values) {
      result.add(value.titleSK.toString());
    }
    return result;
  }
}