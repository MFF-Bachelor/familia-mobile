import '../pages/common/text_constants.dart';
import 'package:flutter/material.dart';

enum TaskStateEnum {
  DONE,
  ACTIVE,
  DELETED,
}
extension TaskStateEnumExtension on TaskStateEnum {
  String get title {
    switch (this) {
      case TaskStateEnum.DONE:
        return TextConstants.task_state_enum_done;
      case TaskStateEnum.ACTIVE:
        return TextConstants.task_state_enum_active;
      case TaskStateEnum.DELETED:
        return TextConstants.task_state_enum_deleted;
    }
  }

  Widget get icon {
    switch (this) {
      case TaskStateEnum.DONE:
        return Icon(Icons.check, color: Color(0xFF69B04A),);
        break;
      case TaskStateEnum.ACTIVE:
        return SizedBox();
        break;
      case TaskStateEnum.DELETED:
        return Icon(Icons.clear, color: Color(0xFFFC5451),);
        break;
    }
  }
}