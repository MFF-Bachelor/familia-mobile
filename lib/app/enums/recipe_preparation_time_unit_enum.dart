import '../pages/common/text_constants.dart';
import 'package:flutter/material.dart';

enum RecipePreparationTimeUnitEnum {
  SECOND,
  MINUTE,
  HOUR,
  DAY
}

extension RecipePreparationTimeUnitEnumExtension on RecipePreparationTimeUnitEnum {

  String get title {
    switch (this) {
      case RecipePreparationTimeUnitEnum.SECOND:
        return "SECOND";
        break;
      case RecipePreparationTimeUnitEnum.MINUTE:
        return "MINUTE";
        break;
      case RecipePreparationTimeUnitEnum.HOUR:
        return "HOUR";
        break;
      case RecipePreparationTimeUnitEnum.DAY:
        return "DAY";
        break;
    }
  }

  String get titleSK {
    switch (this) {
      case RecipePreparationTimeUnitEnum.SECOND:
        return "sekunda";
        break;
      case RecipePreparationTimeUnitEnum.MINUTE:
        return "minúta";
        break;
      case RecipePreparationTimeUnitEnum.HOUR:
        return "hodina";
        break;
      case RecipePreparationTimeUnitEnum.DAY:
        return "deň";
        break;
    }
  }

  String get titleSKAbbr {
    switch (this) {
      case RecipePreparationTimeUnitEnum.SECOND:
        return "sek";
        break;
      case RecipePreparationTimeUnitEnum.MINUTE:
        return "min";
        break;
      case RecipePreparationTimeUnitEnum.HOUR:
        return "hod";
        break;
      case RecipePreparationTimeUnitEnum.DAY:
        return "d";
        break;
    }
  }
}