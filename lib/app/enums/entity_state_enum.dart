import '../pages/common/text_constants.dart';

enum EntityStateEnum {
  ACTIVE,
  INACTIVE,
  DELETED,
}
extension EntityStateEnumExtension on EntityStateEnum {
  String get title {
    switch (this) {
      case EntityStateEnum.INACTIVE:
        return TextConstants.entity_state_enum_inactive;
      case EntityStateEnum.ACTIVE:
        return TextConstants.entity_state_enum_active;
      case EntityStateEnum.DELETED:
        return TextConstants.entity_state_enum_deleted;
    }
  }

  // Widget get icon {
  //   switch (this) {
  //     case EntityStateEnum.INACTIVE:
  //       return Icon(Icons.check, color: Color(0xFF69B04A),);
  //       break;
  //     case EntityStateEnum.ACTIVE:
  //       return SizedBox();
  //       break;
  //     case EntityStateEnum.DELETED:
  //       return Icon(Icons.clear, color: Color(0xFFFC5451),);
  //       break;
  //   }
  // }
}