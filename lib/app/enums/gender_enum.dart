enum GenderEnum {
  MALE,
  FEMALE
}

extension GenderEnumExtension on GenderEnum {

  String get title {
    switch (this) {
      case GenderEnum.MALE:
        return "MALE";
        break;
      case GenderEnum.FEMALE:
        return "FEMALE";
        break;
    }
  }

  String get titleSK {
    switch (this) {
      case GenderEnum.MALE:
        return "Muž";
        break;
      case GenderEnum.FEMALE:
        return "Žena";
        break;
    }
  }
}