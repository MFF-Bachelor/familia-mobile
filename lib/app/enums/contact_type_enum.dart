enum ContactTypeEnum {
  PHONE_NUMBER,
  EMAIL
}

extension GenderEnumExtension on ContactTypeEnum {

  String get title {
    switch (this) {
      case ContactTypeEnum.PHONE_NUMBER:
        return "PHONE_NUMBER";
        break;
      case ContactTypeEnum.EMAIL:
        return "EMAIL";
        break;
    }
  }

  String get titleSK {
    switch (this) {
      case ContactTypeEnum.PHONE_NUMBER:
        return "Telefónne číslo";
        break;
      case ContactTypeEnum.EMAIL:
        return "Email";
        break;
    }
  }
}