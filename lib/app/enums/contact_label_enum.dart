enum ContactLabelEnum {
  HOME,
  WORK,
  MAIN,
  FAX
}

extension GenderEnumExtension on ContactLabelEnum {

  String get title {
    switch (this) {
      case ContactLabelEnum.HOME:
        return "HOME";
        break;
      case ContactLabelEnum.WORK:
        return "WORK";
        break;
      case ContactLabelEnum.MAIN:
        return "MAIN";
        break;
      case ContactLabelEnum.FAX:
        return "FAX";
        break;
    }
  }

  String get titleSK {
    switch (this) {
      case ContactLabelEnum.HOME:
        return "Domov";
        break;
      case ContactLabelEnum.WORK:
        return "Práca";
        break;
      case ContactLabelEnum.MAIN:
        return "Hlavné";
        break;
      case ContactLabelEnum.FAX:
        return "Fax";
        break;
    }
  }
}