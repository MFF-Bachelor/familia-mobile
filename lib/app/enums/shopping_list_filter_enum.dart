enum ShoppingListFilterEnum {
  DUE_TODAY,
  DUE_TOMORROW,
  MY,
  SHARED_WITH_ME,
  UNFINISHED
}

extension ShoppingListFilterEnumExtension on ShoppingListFilterEnum {
  static dynamic get definition {
    return {
      ShoppingListFilterEnum.DUE_TODAY: {
        'label': 'Termín dnes',
        'active': false,
      },
      ShoppingListFilterEnum.DUE_TOMORROW: {
        'label': 'Termín zajtra',
        'active': false,
      },
      ShoppingListFilterEnum.MY: {
        'label': 'Moje',
        'active': false,
      },
      ShoppingListFilterEnum.SHARED_WITH_ME: {
        'label': 'Zdieľané so mnou',
        'active': false,
      },
      ShoppingListFilterEnum.UNFINISHED: {
        'label': 'Nedokončené',
        'active': false,
      },
    };
  }
}

enum ShoppingListSortEnum {
  TITLE,
  DUE_DATE,
  CREATED_DATE_ASC,
  CREATED_DATE_DESC
}

extension ShoppingListSortEnumExtension on ShoppingListSortEnum {
  static dynamic get definition {
    return {
      ShoppingListSortEnum.TITLE: {
        'label': 'Názov'
      },
      ShoppingListSortEnum.DUE_DATE: {
        'label': 'Termín'
      },
      ShoppingListSortEnum.CREATED_DATE_ASC: {
        'label': 'Dátum vytvorenia - vzostupne'
      },
      ShoppingListSortEnum.CREATED_DATE_DESC: {
        'label': 'Dátum vytvorenia - zostupne'
      },
    };
  }
}