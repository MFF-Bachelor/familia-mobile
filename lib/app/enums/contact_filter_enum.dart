enum ContactFilterEnum {
  ALL
}

enum ContactSortEnum {
  NAME_ASC,
  NAME_DESC,
  CREATED_DATE_ASC,
  CREATED_DATE_DESC
}

extension ContactSortEnumExtension on ContactSortEnum {
  static dynamic get definition {
    return {
      ContactSortEnum.CREATED_DATE_ASC: {
        'label': 'Dátum vytvorenia - vzostupne'
      },
      ContactSortEnum.CREATED_DATE_DESC: {
        'label': 'Dátum vytvorenia - zostupne'
      },
      ContactSortEnum.NAME_ASC: {
        'label': 'Meno - vzostupne'
      },
      ContactSortEnum.NAME_DESC: {
        'label': 'Meno - zostupne'
      },
    };
  }
}