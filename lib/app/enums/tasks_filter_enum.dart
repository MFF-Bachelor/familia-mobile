enum TasksFilterEnum {
  DUE_TODAY,
  DUE_TOMORROW,
  HIGHEST_PRIORITY,
  LOWEST_PRIORITY,
  ACTIVE_TASKS
}

extension TasksFilterEnumExtension on TasksFilterEnum {
  static dynamic get definition {
    return {
      TasksFilterEnum.DUE_TODAY: {
        'label': 'Termín dnes',
        'active': false,
      },
      TasksFilterEnum.DUE_TOMORROW: {
        'label': 'Termín zajtra',
        'active': false,
      },
      TasksFilterEnum.HIGHEST_PRIORITY: {
        'label': 'Najvyššia priorita',
        'active': false,
      },
      TasksFilterEnum.LOWEST_PRIORITY: {
        'label': 'Najnižšia priorita',
        'active': false,
      },
      TasksFilterEnum.ACTIVE_TASKS: {
        'label': 'Nedokončené úlohy',
        'active': false,
      },
    };
  }
}

enum TasksSortEnum {
  PRIORITY_ASC,
  PRIORITY_DESC,
  NAME,
  DUE_DATE,
  CREATED_DATE_ASC,
  CREATED_DATE_DESC
}

extension TasksSortEnumExtension on TasksSortEnum {
  static dynamic get definition {
    return {
      TasksSortEnum.CREATED_DATE_ASC: {
        'label': 'Dátum vytvorenia - vzostupne'
      },
      TasksSortEnum.CREATED_DATE_DESC: {
        'label': 'Dátum vytvorenia - zostupne'
      },
      TasksSortEnum.PRIORITY_ASC: {
        'label': 'Najnižšia priorita'
      },
      TasksSortEnum.PRIORITY_DESC: {
        'label': 'Najvyššia priorita'
      },
      TasksSortEnum.DUE_DATE: {
        'label': 'Termín'
      },
      TasksSortEnum.NAME: {
        'label': 'Názov'
      },
    };
  }
}
