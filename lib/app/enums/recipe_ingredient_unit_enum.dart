import '../pages/common/text_constants.dart';
import 'package:flutter/material.dart';

enum RecipeIngredientUnitEnum {
  L,
  ML,
  G,
  KG,
  KS,
  PINCH,
  TABLE_SPOON,
  TEA_SPOON,
  DCL,
  DKG
}

extension RecipeIngredientUnitEnumExtension on RecipeIngredientUnitEnum {


  String get titleSK {
    switch (this) {
      case RecipeIngredientUnitEnum.L:
        return 'l';
        break;
      case RecipeIngredientUnitEnum.ML:
        return 'ml';
        break;
      case RecipeIngredientUnitEnum.G:
        return 'g';
        break;
      case RecipeIngredientUnitEnum.KG:
        return 'kg';
        break;
      case RecipeIngredientUnitEnum.KS:
        return 'ks';
        break;
      case RecipeIngredientUnitEnum.PINCH:
        return 'štipka';
        break;
      case RecipeIngredientUnitEnum.TABLE_SPOON:
        return 'p.l.';
        break;
      case RecipeIngredientUnitEnum.TEA_SPOON:
        return 'č.l.';
        break;
      case RecipeIngredientUnitEnum.DCL:
        return 'dcl';
        break;
      case RecipeIngredientUnitEnum.DKG:
        return 'dkg';
        break;
    }
  }
}