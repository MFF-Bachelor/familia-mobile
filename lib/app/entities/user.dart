import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/abstract_user.dart';
import 'package:familia_mobile/app/entities/contact.dart';
import 'package:familia_mobile/app/entities/notification.dart';
import 'package:familia_mobile/app/entities/recipe.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/entities/shopping_list.dart';
import 'package:familia_mobile/app/entities/task.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:familia_mobile/app/enums/relationship_state_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///User entity represents registered user in app
class User extends AbstractUser {
  int _id;
  String _uid;
  String _firstName;
  String _lastName;
  TZDateTime _birthDate;
  List<String> _emails;
  List<String> _phoneNumbers;
  GenderEnum _gender;
  String _street;
  String _descriptiveNumber;
  String _routingNumber;
  String _postcode;
  String _city;
  String _country;
  List<Relationship> _relationships;
  List<Notification> _notifications;
  List<Task> _myTasks;
  List<Task> _otherCreatedTasks;
  List<Recipe> _recipes;
  List<ShoppingList> _shoppingLists;
  List<ShoppingList> _assigneeShoppingLists;
  List<Contact> _contacts;

  User.fromApiObject(Map<String, dynamic> apiObject) {
    _id = apiObject['id'];
    _uid = apiObject['uid'];
    _firstName = apiObject['firstName'];
    _lastName = apiObject['lastName'];
    _birthDate = apiObject['birthDate'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['birthDate']);
    _emails = [];
    apiObject['emails']?.forEach((emailObject) =>
        _emails.add(emailObject['email'])
    );
    _phoneNumbers = [];
    apiObject['phoneNumbers']?.forEach((phoneNumberObject) =>
        _phoneNumbers.add(phoneNumberObject['phoneNumber'])
    );
    _gender = apiObject['gender'] == null ? null : EnumToString.fromString(GenderEnum.values, apiObject['gender']);
    _street = apiObject['street'];
    _descriptiveNumber = apiObject['descriptiveNumber'];
    _routingNumber = apiObject['routingNumber'];
    _postcode = apiObject['postcode'];
    _city = apiObject['city'];
    _country = apiObject['country'];
    _relationships = [];
    apiObject['relationshipsFrom']?.forEach((relationshipObject) =>
      _relationships.add(Relationship.fromApiObject(relationshipObject))
    );
    _notifications = [];
    apiObject['notifications']?.forEach((notificationObject) =>
        _notifications.add(Notification.fromApiObject(notificationObject))
    );
    _myTasks = [];
    apiObject['assignedTasks']?.forEach((taskObject) =>
        _myTasks.add(Task.fromApiObject(taskObject))
    );
    _otherCreatedTasks = [];
    apiObject['otherCreatedTasks']?.forEach((taskObject) =>
        _otherCreatedTasks.add(Task.fromApiObject(taskObject))
    );
    _recipes = [];
    apiObject['recipes']?.forEach((recipeObject) =>
        _recipes.add(Recipe.fromApiObject(recipeObject))
    );
    _shoppingLists = [];
    apiObject['createdShoppingLists']?.forEach((shoppingList) =>
        _shoppingLists.add(ShoppingList.fromApiObject(shoppingList))
    );
    _assigneeShoppingLists = [];
    apiObject['assigneeShoppingLists']?.forEach((shoppingList) =>
        _shoppingLists.add(ShoppingList.fromApiObject(shoppingList))
    );
    _contacts = [];
    apiObject['contacts']?.forEach((contact) =>
        _contacts.add(Contact.fromApiObject(contact))
    );
  }

  @override
  int get id => _id;
  String get uid => _uid;

  @override
  String get firstName => _firstName;

  @override
  String get lastName => _lastName;

  @override
  TZDateTime get birthDate => _birthDate;

  @override
  GenderEnum get gender => _gender;

  List<String> get emails => _emails;
  List<String> get phoneNumbers => _phoneNumbers;
  String get street => _street;
  String get descriptiveNumber => _descriptiveNumber;
  String get routingNumber => _routingNumber;
  String get postcode => _postcode;
  String get city => _city;
  String get country => _country;
  List<Relationship> get relationships => _relationships;
  List<Relationship> get confirmedRelationships {
    return _relationships.where((relationship) => relationship.state == RelationshipStateEnum.ACTIVE).toList();
  }
  List<Notification> get notifications => _notifications;
  List<Task> get myTasks => _myTasks;
  List<Task> get otherCreatedTasks => _otherCreatedTasks;
  List<Contact> get contacts => _contacts;

  set relationships(List<Relationship> relationships) {
    _relationships = relationships;
  }

  set myTasks(List<Task> tasks) {
    _myTasks = tasks;
  }

}