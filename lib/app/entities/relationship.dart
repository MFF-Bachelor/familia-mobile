import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/abstract_user.dart';
import 'package:familia_mobile/app/entities/inactive_user.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:familia_mobile/app/enums/relationship_role_enum.dart';
import 'package:familia_mobile/app/enums/relationship_side_enum.dart';
import 'package:familia_mobile/app/enums/relationship_state_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///Relationsip enity represents relationship between two users
class Relationship {
  int _id;
  TZDateTime _created;
  User _userFrom;
  User _userTo;
  RelationshipRoleEnum _role;
  RelationshipSideEnum _side;
  RelationshipStateEnum _state;
  InactiveUser _inactiveUserTo;
  GenderEnum _userToGender;
  String _bio;

  Relationship();

  Relationship.fromApiObject(Map<String, dynamic> apiObject) {
    _id = apiObject['id'];
    _created = apiObject['created'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['created']);
    _userFrom = apiObject['userFrom'] == null ? null: User.fromApiObject(apiObject['userFrom']);
    if (apiObject['userTo'] != null)
      _userTo = User.fromApiObject(apiObject['userTo']);
    _role = EnumToString.fromString(RelationshipRoleEnum.values, apiObject['role']);
    apiObject['side'] == null ? null :  _side = EnumToString.fromString(RelationshipSideEnum.values, apiObject['side']);
    apiObject['state'] == null ? null : _state = EnumToString.fromString(RelationshipStateEnum.values, apiObject['state']);
    apiObject['inactiveUserTo'] == null ? null : _inactiveUserTo = InactiveUser.fromApiObject(apiObject['inactiveUserTo']);
    _userToGender = apiObject['userToGender'] == null ? null : EnumToString.fromString(GenderEnum.values, apiObject['userToGender']);
    _bio = apiObject['bio'];
  }

  int get id => _id;
  TZDateTime get created => _created;
  User get userFrom => _userFrom;
  User get userTo => _userTo;
  RelationshipRoleEnum get role => _role;
  RelationshipSideEnum get side => _side;
  RelationshipStateEnum get state => _state;
  InactiveUser get inactiveUserTo => _inactiveUserTo;
  GenderEnum get userToGender => _userToGender;
  String get bio => _bio;
  AbstractUser get anyUserTo => _userTo != null ? _userTo : _inactiveUserTo;
}