import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/abstract_user.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///Inactive user used in oneWayRelationships
class InactiveUser extends AbstractUser {
  int _id;
  String _firstName;
  String _lastName;
  TZDateTime _birthDate;
  GenderEnum _gender;

  InactiveUser.fromApiObject(Map<String, dynamic> apiObject) {
    _id = apiObject['id'];
    _firstName = apiObject['firstName'];
    _lastName = apiObject['lastName'];
    _birthDate = apiObject['birthDate'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['birthDate']);
    _gender = apiObject['gender'] == null ? null : EnumToString.fromString(GenderEnum.values, apiObject['gender']);
  }

  @override
  int get id => id;

  @override
  String get firstName => _firstName;

  @override
  String get lastName => _lastName;

  @override
  TZDateTime get birthDate => _birthDate;

  @override
  GenderEnum get gender => _gender;
}