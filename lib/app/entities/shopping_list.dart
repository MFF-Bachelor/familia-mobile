import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/shopping_list_item.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/entity_state_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///Shopping list used in ShoppingList module
class ShoppingList {
  int shoppingListId;
  String title;
  String description;
  List<ShoppingListItem> shoppingListItems;
  TZDateTime dueTime;
  User creator;
  User assignee;
  EntityStateEnum state;
  TZDateTime created;
  TZDateTime modified;

  ShoppingList();

  ShoppingList.fromApiObject(Map<String, dynamic> apiObject) {
    shoppingListId = apiObject['id'];
    title = apiObject['title'];
    description = apiObject['description'];
    dueTime = apiObject['dueTime'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['dueTime']);
    created = apiObject['created'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['created']);
    modified = apiObject['modified'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['modified']);
    creator = apiObject['creator'] == null ? null :  User.fromApiObject(apiObject['creator']);
    assignee = apiObject['assignee'] == null ? null : User.fromApiObject(apiObject['assignee']);
    state = apiObject['state'] == null ? null : EnumToString.fromString(EntityStateEnum.values, apiObject['state']);
    shoppingListItems = [];
    apiObject['shoppingListItems']?.forEach((shoppingListItem) =>
        shoppingListItems.add(ShoppingListItem.fromApiObject(shoppingListItem))
    );
  }

  bool get isDone {
    if (shoppingListItems.isEmpty)
      return false;
    return shoppingListItems.every((item) => item.checked)
        ? true
        : false;
  }

  String get progressText {
    if (shoppingListItems.isEmpty)
      return '';
    int done = 0;
    int total = 0;
    for (var value in shoppingListItems) {
      if (value.checked) {
        ++done;
      }
      ++total;
    }

    return '$done/$total';
  }
}