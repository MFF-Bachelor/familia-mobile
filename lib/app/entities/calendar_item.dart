import 'dart:ui';
import 'package:timezone/timezone.dart';

///CalendarItem represents item in the calendar module
class CalendarItem<T> {
  CalendarItemType eventType;
  T entity;
  String eventName;
  bool isAllDay;
  TZDateTime from;
  TZDateTime to;
  Color background;

  CalendarItem(
    CalendarItemType eventType,
    T entity,
    String eventName,
    bool isAllDay,
    DateTime from,
    DateTime to,
    Color background
      ) {
    this.eventType = eventType;
    this.entity = entity;
    this.eventName = eventName;
    this.isAllDay = isAllDay;
    this.from = from;
    this.to = to;
    this.background = background;
  }
}

enum CalendarItemType {
  SHOPPING_LIST,
  TASK
}