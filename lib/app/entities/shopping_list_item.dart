
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///Item of shopping list used in ShoppingList module
class ShoppingListItem {
  int shoppingListItemId;
  String title;
  TZDateTime created;
  TZDateTime modified;
  bool checked;

  ShoppingListItem();

  ShoppingListItem.fromApiObject(Map<String, dynamic> apiObject) {
    shoppingListItemId = apiObject['id'];
    title = apiObject['title'];
    created = TZDateTime.parse(currentTimezone, apiObject['created']);
    modified = TZDateTime.parse(currentTimezone, apiObject['modified']);
    checked = apiObject['checked'];
  }

  Map<String, dynamic> toJson() {
    return {
      "title": title,
      "checked": checked
    };
  }
}