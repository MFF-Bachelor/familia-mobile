///Recipe instruction used in Recipe module
class RecipeInstruction {
  int id;
  String description;
  int ordering;

  RecipeInstruction();

  RecipeInstruction.fromApiObject(Map<String, dynamic> apiObject) {
    id = apiObject['id'];
    description = apiObject['description'];
    ordering = apiObject['ordering'];
  }

  Map<String, dynamic> toJson() {
    return {
      "description": description,
      "ordering": ordering
    };
  }
}