
///Recipe category used in Recipe module
class RecipeCategory {
  int id;
  String name;
  List<RecipeCategory> childrenCategories;

  RecipeCategory();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is RecipeCategory &&
              runtimeType == other.runtimeType &&
              name == other.name;

  @override
  int get hashCode => name.hashCode;

  RecipeCategory.fromApiObject(Map<String, dynamic> apiObject) {
    id = apiObject['id'];
    name = apiObject['name'];
    childrenCategories = [];
    apiObject['recipeSubCategories']?.forEach((recipeCategory) =>
        childrenCategories.add(RecipeCategory.fromApiObject(recipeCategory))
    );
  }

}