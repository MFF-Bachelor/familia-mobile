import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/notification_type_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

/// Notification entity used in Notification module
class Notification {
  int _id;
  String _title;
  String _text;
  User _userFrom;
  TZDateTime _created;
  TZDateTime _modified;
  NotificationTypeEnum _type;

  Notification.fromApiObject(Map<String, dynamic> apiObject) {
    _id = apiObject['id'];
    _title = apiObject['title'];
    _text = apiObject['text'];
    if (apiObject['userFrom'] != null)
      _userFrom = User.fromApiObject(apiObject['userFrom']);
    if (apiObject['type'] != null)
      _type = EnumToString.fromString(NotificationTypeEnum.values, apiObject['type']);
    _created = apiObject['created'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['created']);
    _modified = apiObject['modified'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['modified']);
  }

  int get id => _id;
  String get title => _title;
  String get text => _text;
  User get userFrom => _userFrom;
  NotificationTypeEnum get type => _type;
  TZDateTime get created => _created;
  TZDateTime get modified => _modified;

}