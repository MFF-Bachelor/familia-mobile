import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:timezone/timezone.dart';

/// Abstract user defining general fields for different user types
abstract class AbstractUser {
  int get id;
  String get firstName;
  String get lastName;
  TZDateTime get birthDate;
  GenderEnum get gender;
}