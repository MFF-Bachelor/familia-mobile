import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/enums/recipe_ingredient_unit_enum.dart';

///Recipe ingredient used in Recipe module
class RecipeIngredient {
  int id;
  int amount;
  RecipeIngredientUnitEnum unit;
  String name;

  RecipeIngredient();

  RecipeIngredient.fromApiObject(Map<String, dynamic> apiObject) {
    id = apiObject['id'];
    amount = apiObject['amount'];
    unit = apiObject['unit'] == null ? null : EnumToString.fromString(RecipeIngredientUnitEnum.values, apiObject['unit']);
    name = apiObject['name'];
  }

  Map<String, dynamic> toJson() {
    return {
      "amount": amount,
      "unit": unit == null ? null : EnumToString.convertToString(unit),
      "name": name
    };
  }
}