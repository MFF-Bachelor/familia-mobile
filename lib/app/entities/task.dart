import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/task_state_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///Task entity used in Task module
class Task {
  int id;
  User creator;
  User assignee;
  String title;
  String text;
  int priority;
  TZDateTime dueTime;
  TZDateTime created;
  TaskStateEnum state;
  bool canAssigneeEdit;

  Task();// : super('', null, null, null, false);

  Task.fromApiObject(Map<String, dynamic> apiObject) //: super(apiObject['title'], null, null, null, false)
  {
    id = apiObject['id'];
    creator = apiObject['creator'] == null ? null : User.fromApiObject(apiObject['creator']);
    assignee = apiObject['assignee'] == null ? null : User.fromApiObject(apiObject['assignee']);
    title = apiObject['title'];
    text = apiObject['text'];
    priority = apiObject['priority'];
    dueTime = apiObject['dueTime'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['dueTime']);
    created = apiObject['created'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['created']);
    //modified = apiObject['modified'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['modified']);
    state = apiObject['state'] == null ? null : EnumToString.fromString(TaskStateEnum.values, apiObject['state']);
    canAssigneeEdit = apiObject['canAssigneeEdit'] == null ? false : apiObject['canAssigneeEdit'];
  }
}