import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/recip_ingredient.dart';
import 'package:familia_mobile/app/entities/recipe_category.dart';
import 'package:familia_mobile/app/entities/recipe_instruction.dart';
import 'package:familia_mobile/app/enums/recipe_preparation_time_unit_enum.dart';
import 'package:familia_mobile/app/enums/user_recipe_type_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

///Recipe entity used in Recipe module
class Recipe {
  int userRecipeId;
  int recipeId;
  String title;
  int servings;
  int caloriesPerServing;
  String source;
  int preparationTime;
  RecipePreparationTimeUnitEnum recipePreparationTimeUnit;
  int difficulty;
  String description;
  List<RecipeIngredient> recipeIngredients;
  List<RecipeInstruction> recipeInstructions;
  List<RecipeCategory> recipeCategories;
  bool isFavourite;
  int rating;
  String comment;
  UserRecipeTypeEnum userTypeEnum;
  bool willCook;
  TZDateTime created;


  Recipe();

  Recipe.fromApiObject(Map<String, dynamic> apiObject) {
    userRecipeId = apiObject['id'];
    recipeId = apiObject['recipe'] == null ? null : apiObject['recipe']['id'];
    title = apiObject['recipe'] == null ? null : apiObject['recipe']['title'];
    servings = apiObject['recipe'] == null ? null : apiObject['recipe']['servings'];
    caloriesPerServing = apiObject['recipe'] == null ? null : apiObject['recipe']['caloriesPerServing'];
    source = apiObject['recipe'] == null ? null : apiObject['recipe']['source'];
    preparationTime = apiObject['recipe'] == null ? null : apiObject['recipe']['preparationTime'];
    recipePreparationTimeUnit = apiObject['recipe'] == null  || apiObject['recipe']['preparationTimeUnit'] == null ? null : EnumToString.fromString(RecipePreparationTimeUnitEnum.values, apiObject['recipe']['preparationTimeUnit']);
    difficulty = apiObject['recipe'] == null || apiObject['recipe']['difficulty'] == null ? 0 : apiObject['recipe']['difficulty'];
    description = apiObject['recipe'] == null ? null : apiObject['recipe']['description'];
    recipeIngredients = [];
    if (apiObject['recipe'] != null ) apiObject['recipe']['recipeIngredients']?.forEach((recipeIngredient) =>
        recipeIngredients.add(RecipeIngredient.fromApiObject(recipeIngredient))
    );
    recipeInstructions = [];
    if (apiObject['recipe'] != null ) apiObject['recipe']['recipeInstructions']?.forEach((recipeInstruction) =>
        recipeInstructions.add(RecipeInstruction.fromApiObject(recipeInstruction))
    );
    recipeCategories = [];
    if (apiObject['recipe'] != null ) apiObject['recipe']['recipeCategories']?.forEach((recipeCategory) =>
        recipeCategories.add(RecipeCategory.fromApiObject(recipeCategory))
    );
    isFavourite = apiObject['favourite'];
    rating = apiObject['rating'];
    comment = apiObject['comment'];
    userTypeEnum = apiObject['type'] == null ? null : EnumToString.fromString(UserRecipeTypeEnum.values, apiObject['type']);
    willCook = apiObject['willCook'];
    created = apiObject['recipe'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['recipe']['created']);
  }
}