import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/contact_label_enum.dart';
import 'package:familia_mobile/app/enums/entity_state_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:timezone/timezone.dart';

/// Contact entity for Contact module
class Contact {
  int contactId;
  User user;
  String email;
  String phoneNumber;
  ContactLabelEnum label;
  String name;
  EntityStateEnum state;
  TZDateTime created;
  TZDateTime modified;

  Contact();

  Contact.create(String name, String phoneNumber, String email) {
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }

  Contact.fromApiObject(Map<String, dynamic> apiObject) {
    contactId = apiObject['id'];
    user = apiObject['user'] == null ? null :  User.fromApiObject(apiObject['user']);
    email = apiObject['email'];
    phoneNumber = apiObject['phoneNumber'];
    label = apiObject['label'] == null ? null : EnumToString.fromString(ContactLabelEnum.values, apiObject['label']);
    name = apiObject['name'];
    state = apiObject['state'] == null ? null : EnumToString.fromString(EntityStateEnum.values, apiObject['state']);
    created = apiObject['created'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['created']);
    modified = apiObject['modified'] == null ? null : TZDateTime.parse(currentTimezone, apiObject['modified']);
  }
}