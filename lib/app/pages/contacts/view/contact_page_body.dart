import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/contact.dart';
import 'package:familia_mobile/app/enums/contact_filter_enum.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/contacts/service/contact_service.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../widgets.dart';
import 'contact_detail_page.dart';

class ContactPageBody extends StatefulWidget {
  ContactPageBody({Key key}) : super(key: key);

  @override
  ContactPageBodyState createState() => ContactPageBodyState();
}

class ContactPageBodyState extends State<ContactPageBody> {
  List<Contact> _contacts = [];
  VoidCallback myRefetch;
  ContactSortEnum appliedSort = ContactSortEnum.CREATED_DATE_DESC;
  String searchString = '';

  final ContactService _contactService = ContactService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        return  Query(
          builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
            myRefetch = refetch;
            CustomQueryResult queryResult = _contactService.checkQueryResult(result, refetch: refetch, fetchMore: fetchMore);
            switch (queryResult) {
              case CustomQueryResult.EXCEPTION:
                return BuildWidgets.queryExceptionWidget(context, exceptionMessage: result.exception.toString());
                break;
              case CustomQueryResult.LOADING:
                return BuildWidgets.queryLoadingWidget(context);
                break;
              case CustomQueryResult.FAIL:
                return BuildWidgets.queryFailWidget(context);
                break;
              case CustomQueryResult.SUCCESS:
                _parseQueryResult(result);
                if (_contacts.isEmpty) {
                  return BuildWidgets.queryNoDataWidget(context);
                }
                return _buildContactsList();
                break;
            }
            return BuildWidgets.queryFailWidget(context);
          },
          options: QueryOptions(
            document: gql(Queries.getUserContactsQuery),
          ),
        );
      }),
      floatingActionButton: BuildWidgets.floatingButtonAddItem(
          context,
          nextRoute: MaterialPageRoute(builder: (context) => ContactDetailPage(
              pageType: ContactDetailPageTypeEnum.CREATE_PAGE,
              contact: Contact()
          )),
          onReturn: () {
            myRefetch();
            setState(() {});
          }
      ),
    );
  }

  ///Parse data (assignedTasks and otherCreatedTasks) from query result, apply filters and sort and save
  ///the data into local variables myTasks and otherCreatedTasks
  _parseQueryResult(QueryResult result) {
    _contacts = [];
    result.data[Queries.GET_USER_CONTACTS]['data']['contacts']?.forEach((contact) {
      var contactObject = Contact.fromApiObject(contact);
      _contacts.add(contactObject);
    });

    _contacts = _contactService.filterVisibleContacts(contacts: _contacts, appliedSort: appliedSort, searchString: searchString);
  }

  ListView _buildContactsList() {
    return ListView.builder(
        itemCount: _contacts.length,
        itemBuilder: (context, index) {
          return Widgets.buildMyContactCard(
              context,
              contact: _contacts[index],
              onCardPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ContactDetailPage(
                            contact: _contacts[index],
                            pageType: ContactDetailPageTypeEnum.VIEW_PAGE
                        )
                    )
                ).then((needRefetch) {
                  myRefetch();
                  setState(() {});
                });
              }
          );
        }
    );
  }

  void applyFilters({ContactSortEnum appliedSort, String searchString}) {
    if (appliedSort != null)
      this.appliedSort = appliedSort;
    if (searchString != null)
      this.searchString = searchString;
    setState(() {});
  }
}