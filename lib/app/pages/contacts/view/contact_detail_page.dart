import 'package:enum_to_string/enum_to_string.dart';
import '../../../services/graphql_service/graphql_helper.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/contact.dart';
import 'package:familia_mobile/app/enums/contact_label_enum.dart';
import 'package:familia_mobile/app/enums/entity_state_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/contacts/service/contact_service.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widgets.dart';

class ContactDetailPage extends StatefulWidget {
  final ContactDetailPageTypeEnum pageType;
  Contact contact;

  ContactDetailPage({Key key, @required this.pageType, @required this.contact}) : super(key: key);

  @override
  _ContactDetailPageState createState() => _ContactDetailPageState();
}

class _ContactDetailPageState extends State<ContactDetailPage> {
  Map<String, dynamic> data = {};
  var dateFormat = DateFormat('dd.MM.yyyy');
  var timeFormat = DateFormat('HH:mm');
  var dateTimeFormat = DateFormat('HH:mm dd.MM.yyyy');
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ContactService _contactService = ContactService();
  GraphGLHelper _graphGLHelper = GraphGLHelper();

  //Data controllers
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _phoneNumberController;

  @override
  void initState() {
    _initFormValues();

    super.initState();
  }

  _initFormValues() {
    switch (widget.pageType) {
      case ContactDetailPageTypeEnum.CREATE_PAGE:
        _nameController = widget.contact.name == null ? TextEditingController(text:'') : TextEditingController(text: widget.contact.name);
        _emailController = widget.contact.email == null ? TextEditingController(text:'') : TextEditingController(text: widget.contact.email);
        _phoneNumberController = widget.contact.phoneNumber == null ? TextEditingController(text:'') : TextEditingController(text: widget.contact.phoneNumber);
        break;
      case ContactDetailPageTypeEnum.UPDATE_PAGE:
      case ContactDetailPageTypeEnum.VIEW_PAGE:
        _nameController = TextEditingController(text: widget.contact.name);
        _emailController = TextEditingController(text: widget.contact.email);
        _phoneNumberController = TextEditingController(text: widget.contact.phoneNumber);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery
        .of(context)
        .padding
        .top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Column(
                  children: <Widget>[
                    OneRowHeader(
                      title:
                      widget.pageType == ContactDetailPageTypeEnum.CREATE_PAGE
                          ? TextConstants.contact_detail_page_new_contact_title
                          : TextConstants.contact_detail_page_view_contact_title,
                      rightSidedWidget:
                      (widget.pageType == ContactDetailPageTypeEnum.VIEW_PAGE)
                          ? _buildEditButton()
                          : _buildSubmitButton()
                    ),
                    Expanded(
                        child: _buildContactDetailPageBody()
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }

  _buildContactDetailPageBody() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox( height: 20,),
              //_buildFormBody(),
              (widget.pageType == ContactDetailPageTypeEnum.VIEW_PAGE)
                  ? _buildViewBody()
                  : _buildFormBody(),
              SizedBox( height: 20,)
            ],
          ),
        ),
      ),
    );
  }

  _buildFormBody() {
    return Wrap(
      spacing: 20.0,
      children: [
        Visibility(
          visible: true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.contact_title_text,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _nameController,
                enabled: true,
                validator: (String value) {
                  return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.contact_phone_number_text,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _phoneNumberController,
                enabled: true,
                validator: (String value) {
                  if (value == null || value.isEmpty) {
                    return null;
                  }
                  RegExp regex = new RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$');
                  return regex.hasMatch(value)
                      ? null
                      : TextConstants.wrong_phone_number_format_alert;
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.contact_email_text,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _emailController,
                enabled: true,
                validator: (String value) {
                  if (value == null || value.isEmpty) {
                    return null;
                  }

                  RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$');
                  return regex.hasMatch(value)
                      ? null
                      : TextConstants.wrong_email_format_alert;
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: widget.pageType == ContactDetailPageTypeEnum.UPDATE_PAGE,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildRemoveContactButton(widget.contact)
            ],
          ),
        )
      ],
    );
  }

  _buildViewBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.contact.name,
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox( height: 20,),
        Column(
          children: [
            BuildWidgets.buildBasicHorizontalField(
                context,
                label: TextConstants.contact_phone_number_text,
                child: Row(
                    children: [
                      SizedBox(width: 5.0,),
                      widget.contact.phoneNumber.isEmpty
                          ? Text(
                              "Nezadané",
                              style: Theme.of(context).textTheme.bodyText1,
                            )
                          : Text(
                              widget.contact.phoneNumber,
                              style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ]
                )
            ),
            SizedBox( height: 20,),
            BuildWidgets.buildBasicHorizontalField(
                context,
                label: TextConstants.contact_email_text,
                child: Row(
                    children: [
                      SizedBox(width: 5.0,),
                      widget.contact.email.isEmpty
                          ? Text(
                            "Nezadané",
                            style: Theme.of(context).textTheme.bodyText1,
                          )
                        :
                      Text(
                        widget.contact.email,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ]
                )
            ),
            SizedBox(height: 50,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildContactButton(
                  icon: Icons.call_rounded,
                  url: "tel:" + _phoneNumberController.text.trim()
                ),
                _buildContactButton(
                  icon: Icons.email_outlined,
                  url: "mailto:" + _emailController.text.trim()
                )
              ],
            )
          ],
        )
      ],
    );
  }

  _buildSubmitButton() {
    return Mutation (
        options: _mutationOptionsBasedOnPageType(),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildDoneButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _runMutationBasedOnPageType(runMutation);
                }
              });
        });
  }

  _buildEditButton() {
    return IconButton(
        icon: Icon(
          Icons.edit,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ContactDetailPage(
                    pageType: ContactDetailPageTypeEnum.UPDATE_PAGE,
                    contact: widget.contact
                )
            ),
          ).then((value) => Navigator.pop(context, true));
        }
    );
  }

  _buildRemoveContactButton(Contact contact) {
    return Mutation(
        options: _graphGLHelper.entityMutationOptions(
          context,
          entityService: _contactService,
          mutationDefinition: Mutations.deleteUserContact,
          mutationName: Mutations.DELETE_CONTACT,
          onError: (_) {
            BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
          },
          onSuccess: (result) {
            Navigator.of(context).pop();
            BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
          }
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildBasicOutlinedButton(
              context,
              text: TextConstants.delete_contact_button_text,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _contactService.onDeleteButtonTap(
                      runMutation,
                      id: contact.contactId,
                      onError: () {
                        throw Exception("Custom error: on delete failed");
                      }
                  );
                }
              }
          );
        }
    );
  }

  _mutationOptionsBasedOnPageType() {
    switch (widget.pageType) {
      case ContactDetailPageTypeEnum.CREATE_PAGE:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _contactService,
            mutationDefinition: Mutations.createContactMutation,
            mutationName: Mutations.CREATE_CONTACT,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      case ContactDetailPageTypeEnum.UPDATE_PAGE:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _contactService,
            mutationDefinition: Mutations.setContactMutation,
            mutationName: Mutations.SET_CONTACT,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      default: return null;
    }
  }

  _runMutationBasedOnPageType(RunMutation runMutation) {
   switch (widget.pageType) {
      case ContactDetailPageTypeEnum.CREATE_PAGE:
        _contactService.runCreateContactMutation(
          runMutation,
          name: _nameController,
          user: apiAuthService.currentUser,
          email: _emailController,
          phoneNumber: _phoneNumberController,
          label: EnumToString.convertToString(ContactLabelEnum.MAIN),
          state: EnumToString.convertToString(EntityStateEnum.ACTIVE),
          context: context,
        );
        break;
      case ContactDetailPageTypeEnum.UPDATE_PAGE:
        _contactService.runSetContactMutation(
            runMutation,
            contactId: widget.contact.contactId,
            name: _nameController,
            phoneNumber: _phoneNumberController,
            email: _emailController,
            label: EnumToString.convertToString(ContactLabelEnum.MAIN),
            state: EnumToString.convertToString(EntityStateEnum.ACTIVE),
            context: context
        );
        break;
      default: return null;
    }
  }

  _buildContactButton({@required icon, @required String url}) {
    return InkWell(
      onTap: () {
        launchUrl(Uri.parse(url));
      },
      child: Container(
        width: 80,
        height: 80,
        decoration: BoxDecoration(
            color: Theme.of(context).accentColor,
            borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Center(child: Icon(icon, color: Colors.white, size: 42,)),
      ),
    );
  }
}

enum ContactDetailPageTypeEnum {
  CREATE_PAGE,
  UPDATE_PAGE,
  VIEW_PAGE
}