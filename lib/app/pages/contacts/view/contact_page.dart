import '../../common/text_constants.dart';
import 'package:familia_mobile/app/enums/contact_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/filter_sort_panel.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'contact_page_body.dart';

class ContactPage extends StatefulWidget {
  ContactPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  PanelController _panelController = new PanelController();
  final GlobalKey<FilterSortPanelState> _contactFilterPanelKey = GlobalKey();
  final GlobalKey<ContactPageBodyState> _contactPageBodyKey = GlobalKey();

  var _appliedSort;
  String _searchString;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return BuildWidgets.customSlidingUpPanel(
      controller: _panelController,
      panel: FilterSortPanel<ContactFilterEnum, ContactSortEnum>(
        key: _contactFilterPanelKey,
        activeSort: ContactSortEnum.CREATED_DATE_DESC,
        entityFilterEnumValues: null,
        entitySortEnumValues: ContactSortEnum.values,
        filtersDefinition: null,
        sortsDefinition: ContactSortEnumExtension.definition,
        onFilterSwitch: ({dynamic filter, List<dynamic> appliedFilters, dynamic appliedSort}) {
          _appliedSort = appliedSort;
          _contactPageBodyKey.currentState.applyFilters(appliedSort: appliedSort, searchString: _searchString);
        },
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
            body: Center(
                child: Container(
                  padding: EdgeInsets.only(top: _statusBarHeight),
                  child: Column(
                    children: <Widget>[
                      TwoRowsHeader(
                        title: TextConstants.contact_page_header_text,
                        rightSidedWidget: IconButton(
                          icon: Icon(Icons.sort),
                          onPressed: () {
                            _panelController.open();
                          },
                          color: Colors.white,
                        ),
                        secondRowWidget: BuildWidgets.buildSearchBar(context,
                            onChanged: (String newValue) {
                              _searchString = newValue;
                              _contactPageBodyKey.currentState.applyFilters(appliedSort: _appliedSort, searchString: newValue);
                            }),
                      ),
                      Expanded(
                          child: ContactPageBody(key: _contactPageBodyKey,)
                      ),
                    ],
                  ),
                )
            )
        ),
      ),
    );
  }
}