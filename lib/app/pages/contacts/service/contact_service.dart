import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/contact.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/contact_filter_enum.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/material.dart';


class ContactService {
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return CustomQueryResult.EXCEPTION;
    }

    if (result.isLoading) {
      return CustomQueryResult.LOADING;
    }

    var success = result.data[Queries.GET_USER_CONTACTS][Queries.SUCCESS];
    if (success) {
      sessionService.saveCookieFromApiResponse(result);
      return CustomQueryResult.SUCCESS;
    } else {
      return CustomQueryResult.FAIL;
    }
  }

  Future<void>  runCreateContactMutation(
      RunMutation runCreateContactMutation,
      {
        @required context,
        @required User user,
        @required phoneNumber,
        @required email,
        @required label,
        @required name,
        @required state,
      }) async {
    runCreateContactMutation({
      "inputData": {
        "userUid": user.uid,
        "phoneNumber": phoneNumber.text.trim(),
        "email": email.text.trim(),
        "label": label.trim(),
        "name": name.text.trim(),
        "state": state,
      }
    });
  }

  Future<void>  runSetContactMutation(
      RunMutation runSetContactMutation,
      {
        @required context,
        @required contactId,
        @required email,
        @required phoneNumber,
        @required label,
        @required name,
        @required state,
      }) async {
    runSetContactMutation({
      "inputData": {
        "userContactId": contactId,
        "email": email.text.trim(),
        "phoneNumber": phoneNumber.text.trim(),
        "label":  label,
        "name": name.text.trim(),
        "state": state,
      }
    });
  }

  Future<void> onDeleteButtonTap(
      RunMutation runDeleteContactMutation,
      {
        @required id,
        @required onError,
      }) async {
    runDeleteContactMutation({
      "userContactId": id
    });
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success) {
        return CustomMutationResult.SUCCESS;
      } else {
        return CustomMutationResult.FAIL;
      }
    }
  }

  ///Filters and sorts contacts based on given params
  List<Contact> filterVisibleContacts({ @required List<Contact> contacts, @required appliedSort, @required String searchString}) {
    contacts.removeWhere((contact) {

      bool toRemoveWithSearch = false;
      if (searchString != null && searchString != '') {
        String lowerCaseSearchString = searchString.toLowerCase();
        toRemoveWithSearch = true;
        if (contact.name.toLowerCase().contains(lowerCaseSearchString)) {
          toRemoveWithSearch = false;
        }
      }

      return toRemoveWithSearch;
    });


    contacts.sort((Contact contact1, Contact contact2) {
      switch(appliedSort) {
        case ContactSortEnum.CREATED_DATE_DESC:
          return contact2.created.compareTo(contact1.created);
          break;
        case ContactSortEnum.CREATED_DATE_ASC:
          return contact1.created.compareTo(contact2.created);
          break;
        case ContactSortEnum.NAME_DESC:
          return contact2.name.toLowerCase().compareTo(contact1.name.toLowerCase());
          break;
        case ContactSortEnum.NAME_ASC:
          return contact1.name.toLowerCase().compareTo(contact2.name.toLowerCase());
          break;
        default:
          return 0;
      }
    });

    return contacts;
  }
}