import 'package:familia_mobile/app/entities/contact.dart';
import '../common/build_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Widgets {
  static Widget buildMyContactCard(
    BuildContext context, {
      @required Contact contact,
      @required Function onCardPressed
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
      child: InkWell(
        child: BuildWidgets.buildBasicTitledCard(
            context,
            cardTitle: contact.name,
            height: 115,
            bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
            leftSidedVerticalWidget:SizedBox(width: 0),
            rightTopWidget: SizedBox(),
            firstRowWidget: contact.phoneNumber.isEmpty ? SizedBox() : BuildWidgets.buildLabelItem(context,
              icon: Icons.phone,
              text: contact.phoneNumber,
            ),
          secondRowWidget: contact.email.isEmpty ? SizedBox() : BuildWidgets.buildLabelItem(context,
            icon: Icons.email_outlined,
            text: contact.email,
          ),
        ),
        onTap: () => onCardPressed(),
      ),
    );
  }
}