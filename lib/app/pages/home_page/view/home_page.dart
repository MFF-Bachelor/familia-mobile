
import '../../common/text_constants.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/home_page/service/home_page_service.dart';
import 'package:familia_mobile/app/pages/login/view/login_view.dart';
import 'package:familia_mobile/app/pages/settings/view/setting_page.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'home_page_header.dart';
import 'home_page_menu.dart';

class HomePage extends StatefulWidget {

  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomePageService _homePageService;

  @override
  void initState() {
    _homePageService = HomePageService();


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    sessionService.setCookieToApiRequest();
    if (apiAuthService.currentUser == null) {
      Fimber.d("The user is null, need to call api and get user");
      _homePageService.getUser(
          context: context,
          onError: () {
            BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.login_failed_alert);
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginPage()),
            );
          });
    }
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: Center(
          child: Container(
            padding: EdgeInsets.only(top: _statusBarHeight),

            child: Column(
              children: <Widget>[
                HomePageHeader(
                  onSettingPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SettingPage()),
                    );
                  },
                  onLogoutPressed: () {
                    _homePageService.logoutUser(
                        context: context,
                      onError: () {
                        BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.logout_failed_alert);
                      }
                    );
                    sessionService.deleteCookieFromSharedPreferences();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                ),
                Expanded(child: const HomePageMenu()),
              ],
            ),
          ),
        ),
      ),
    );
  }
}