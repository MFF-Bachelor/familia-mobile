
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../common/text_constants.dart';

class HomePageHeader extends StatefulWidget {
  //constructor
  const HomePageHeader({@required this.onSettingPressed, @required this.onLogoutPressed, Key key}) : super(key: key);

  final onSettingPressed;
  final onLogoutPressed;

  @override
  _HomePageHeaderState createState() => _HomePageHeaderState();
}

class _HomePageHeaderState extends State<HomePageHeader> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF2F4B61),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              TextConstants.logo_text,
              style: Theme.of(context).textTheme.headline2,
            ),
          ),
          Row(
            children: [
              IconButton(
                  icon: Icon(
                    Icons.settings,
                    size: 25,
                    color: Colors.white,
                  ),
                  onPressed: widget.onSettingPressed
              ),
              IconButton(
                  icon: Icon(
                    Icons.logout,
                    size: 25,
                    color: Colors.white,
                  ),
                  onPressed: widget.onLogoutPressed
              ),
            ],
          ),
        ],
      ),
    );
  }
}