import 'package:community_material_icon/community_material_icon.dart';
import 'package:familia_mobile/app/pages/calendar/view/calendar_page.dart';
import 'package:familia_mobile/app/pages/contacts/view/contact_page.dart';
import 'package:familia_mobile/app/pages/family_tree/view/family_tree_page.dart';
import 'package:familia_mobile/app/pages/recipes/view/recipes_page.dart';
import 'package:familia_mobile/app/pages/shopping_list/view/shopping_list_page.dart';
import 'package:familia_mobile/app/pages/tasks/view/tasks_page.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePageMainSection extends StatefulWidget {
  const HomePageMainSection({Key key}) : super(key: key);

  @override
  _HomePageMainSectionState createState() => _HomePageMainSectionState();
}

class _HomePageMainSectionState extends State<HomePageMainSection> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Column(
        children: [
          Row(
              children: [
                Expanded(
                  child: _LargeTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CalendarPage()),
                      );
                    },
                    title: 'Kalendár',
                    iconData: CommunityMaterialIcons.calendar_month
                  ),
                ),
              ]
          ),
          SizedBox(height: 10),
          Row(
              children: [
                Expanded(
                  flex: 5,
                  child: _SmallTile(
                    title: 'Úlohy',
                    iconData: Icons.task_outlined,
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TasksPage()),
                    )),
                ),
                Expanded(
                  flex: 5,
                  child: _SmallTile(
                      title: 'Recepty',
                      iconData: CommunityMaterialIcons.chef_hat,
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RecipesPage()),
                      )),
                ),
              ]
          ),
          Row(
              children: [
                Expanded(
                  flex: 5,
                  child: _SmallTile(
                      title: 'Nákupné zoznamy',
                      iconData: CommunityMaterialIcons.format_list_checks,
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ShoppingListPage()),
                      )),
                ),
                Expanded(
                  flex: 5,
                  child: _SmallTile(
                      iconData: CommunityMaterialIcons.graph,
                      title: 'Rodokmeň',
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => FamilyTreePage(userUid: apiAuthService.currentUser.uid,)),
                      )),
                ),
              ]
          ),
          Row(
              children: [
                Expanded(
                  flex: 5,
                  child: _SmallTile(
                      iconData: CommunityMaterialIcons.contacts,
                      title: 'Kontakty',
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ContactPage()),
                      )),
                ),
                Expanded(flex: 5, child: SizedBox()),
              ]
          ),
        ],
      ),
    );
  }
}

class _LargeTile extends StatelessWidget {
  final String title;
  final Function onTap;
  final IconData iconData;

  const _LargeTile({
    @required this.title,
    @required this.onTap,
    @required this.iconData,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: Theme.of(context).accentColor,
        elevation: 3,
        shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(
            color: Colors.grey.withOpacity(0.2),
            width: 1,
          ),
        ),
        shadowColor: Colors.black38,
        child: SizedBox(
          height: 200,
          width: 100,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: Theme.of(context).textTheme.headline3.copyWith(color: Colors.white)),
                SizedBox(height: 30,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      iconData,
                      size: 70,
                      color: Colors.white,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      onTap: onTap,
    );
  }
}

class _SmallTile extends StatelessWidget {
  final String title;
  final Function onTap;
  final IconData iconData;

  const _SmallTile({
    Key key,
    @required this.title,
    @required this.onTap,
    @required this.iconData
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        color: Theme.of(context).primaryColorLight,//Color(0xFFF8596A6),
        elevation: 3,
        shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(
            color: Colors.grey.withOpacity(0.2),
            width: 1,
          ),
        ),
        // shadowColor: Colors.black38,
        child: SizedBox(
          height: 100,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: Theme.of(context).textTheme.headline5.copyWith(color: Colors.white),),
                SizedBox(height: 30,),
                Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    iconData,
                    size: 30,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      onTap: onTap,
    );
  }
}