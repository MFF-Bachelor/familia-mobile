import 'package:familia_mobile/app/pages/home_page/view/home_page_main_section.dart';
import 'package:familia_mobile/app/pages/my_family/view/my_family_main_section.dart';
import 'package:familia_mobile/app/pages/notifications/view/notifications_main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePageMenu extends StatefulWidget {
  const HomePageMenu({Key key}) : super(key: key);

  @override
  _HomePageMenuState createState() => _HomePageMenuState();
}

class _HomePageMenuState extends State<HomePageMenu> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  double _iconSize = 30;
  TabController _tabController;
  static const _iconPaddingHorizontal = 20.0;

  @override
  Widget build(BuildContext context) {
    var _tabs = [
      Tab(
        icon: Icon(
        Icons.home,
        size: _iconSize,
        color: Colors.white,
      ),),
      Tab(icon: Icon(
        Icons.family_restroom_outlined,
        size: _iconSize,
        color: Colors.white,
      ),),
      Tab(icon: Icon(
        Icons.notifications,
        size: _iconSize,
        color: Colors.white,
      ),),
    ];

    return DefaultTabController(
        length: _tabs.length,
        child: Builder(builder: (BuildContext context) {
          if (_tabController == null) {
            _tabController = DefaultTabController.of(context);
          }
          return Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(26),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                child: Container(
                  color: Color(0xFFF2F4B61),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TabBar(
                        controller: _tabController,
                        isScrollable: true,
                        indicatorColor: Colors.transparent,
                        tabs: _tabs
                      ),
                      Container(
                          width: double.maxFinite,
                          height: 5,
                          color: Colors.white,
                      )
                    ],
                  ),
                ),
              ),
            ),
            body: TabBarView(
              controller: _tabController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                HomePageMainSection(),
                MyFamilyMainSection(),
                NotificationsMainSection()
              ],
            ),
          );
        })
    );
  }
}