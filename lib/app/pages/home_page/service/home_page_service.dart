import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class HomePageService {
  Future<void> getUser({@required BuildContext context, onError}) async {
    QueryResult result = await GraphQLProvider
        .of(context)
        .value
        .query(
        QueryOptions(document: gql(Queries.getUserQuery)));

    if (result.hasException)
      onError();
    else {
      var success = result.data[Queries.GET_USER][Queries.SUCCESS];
      if (success) {
        sessionService.saveCookieFromApiResponse(result);
        apiAuthService.setCurrentUser(User.fromApiObject(result.data[Queries.GET_USER]['data']));
      } else
        onError();
    }
  }

  Future<void> logoutUser({@required BuildContext context, onError}) async {
    GraphQLProvider.of(context).value.mutate(
        MutationOptions(
            document: gql(Mutations.logoutUserMutation),
            onCompleted: (dynamic resultData) {
              var success = resultData[Mutations.LOGOUT_USER][Mutations.SUCCESS];
              if (success) {

              } else {
                onError();
              }
            }
        )
    );
  }
}