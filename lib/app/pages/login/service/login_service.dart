import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:familia_mobile/app/services/firebase_service.dart';
import 'package:firebase_auth/firebase_auth.dart' as Auth;

class LoginService {
  Future<void> onSubmitButtonTap(RunMutation runLoginMutation, _email, _pass, {@required onError, @required context}) async {

    Auth.User user = await FirebaseService.signInWithEmailAndPass(_email.text.trim(), _pass.text);
    if (user != null) {
      var idToken = await user.getIdToken();
      runLoginMutation({
        "inputData": {
          "token": idToken
        }
      });
    } else
      onError();
  }

  Future<void> resetPassword({@required String email, @required onSuccess, @required onError}) async {
    bool result = await FirebaseService.sendPasswordResetEmail(email: email);
    if (result == true) {
      onSuccess();
    } else {
      onError();
    }
  }
}