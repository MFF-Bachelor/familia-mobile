
import 'package:familia_mobile/app/pages/registration/view/registration_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'login_page_footer.dart';
import 'login_page_form.dart';
import 'login_page_header.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {

    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: Center(
          child: Container(
            padding: EdgeInsets.only(top: _statusBarHeight, bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const LoginPageHeader(),
                const LoginPageForm(),
                LoginPageFooter(
                  onRegistrationPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegistrationPage()),
                    );
                  }
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}