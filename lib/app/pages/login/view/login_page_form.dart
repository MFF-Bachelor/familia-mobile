import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/pages/common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/text_constants.dart';
import 'package:familia_mobile/app/pages/home_page/view/home_page.dart';
import 'package:familia_mobile/app/pages/login/service/login_service.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';



class LoginPageForm extends StatefulWidget {
  const LoginPageForm({Key key}) : super(key: key);

  @override
  _LoginPageFormState createState() => _LoginPageFormState();
}

class _LoginPageFormState extends State<LoginPageForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _resetPasswordFormKey = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController();
  TextEditingController _resetPasswordEmail = TextEditingController();
  TextEditingController _pass = TextEditingController();
  final LoginService _loginService = LoginService();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Wrap(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 15.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      TextConstants.login_page_form_login_text,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  BuildWidgets.buildInputField(
                    context,
                    isRequired: true,
                    label: TextConstants.login_page_form_login_text,
                    inputField: BuildWidgets.buildBasicTextFormField(
                        context,
                        controller: _email,
                        validator: (String value) {
                          if (value == null || value.isEmpty) {
                            return TextConstants.non_empty_input_alert;
                          }
                          return null;
                        }
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      TextConstants.login_page_form_pass_text,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  BuildWidgets.buildInputField(
                    context,
                    isRequired: true,
                    label: TextConstants.login_page_form_pass_text,
                    inputField: BuildWidgets.buildBasicTextFormField(
                      context,
                      controller: _pass,
                      enableSuggestions: false,
                      autocorrect: false,
                      obscureText: true,
                      validator: (String value) {
                        if (value == null || value.isEmpty) {
                          return TextConstants.non_empty_input_alert;
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        _buildResetPasswordButton(),
                        SizedBox(width: 20,),
                        _buildSubmitButton(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSubmitButton() {
    return Mutation(
      options: MutationOptions(
        document: gql(Mutations.loginMutation),
        onCompleted: (dynamic resultData) {
          if (resultData == null) {
            BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.login_failed_alert);
            return;
          }
          var success = resultData[Mutations.LOGIN_USER][Mutations.SUCCESS];
          if (success) {
            apiAuthService.setCurrentUser(User.fromApiObject(resultData[Mutations.LOGIN_USER]['data']));
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          } else {
            BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.login_failed_alert);
          }
        },
      ),
      builder: (
        RunMutation runMutation,
        QueryResult result
      ) {
        if (result.data != null)
          sessionService.saveCookieFromApiResponse(result);

        return BuildWidgets.buildBasicButton(
            context,
            text: TextConstants.login_page_login_button,
            onPressed: () async {
              // Validate will return true if the form is valid, or false if
              // the form is invalid.
              if (_formKey.currentState.validate()) {
                _loginService.onSubmitButtonTap(runMutation, _email, _pass,
                  onError: () {
                    BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.login_failed_alert);
                  }, context: context,);
              }
            });
      },
    );
  }

  Widget _buildResetPasswordButton() {
    return InkWell(
      child: Text(
        TextConstants.login_page_reset_password_button,
        style: Theme.of(context).textTheme.headline5.copyWith(color: Theme.of(context).accentColor),
      ),
      onTap: () {
        _onShowResetPasswordDialog();
      },
    );
  }

  void _onShowResetPasswordDialog() {
    showDialog(
      context: context,
      builder: (BuildContext newContext) => AlertDialog(
        title: Text(TextConstants.login_page_reset_password_title, style: Theme.of(context).textTheme.headline3,),
        content: Container(
          child: Form(
            key: _resetPasswordFormKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(TextConstants.login_page_reset_password_text, style: Theme.of(context).textTheme.bodyText1,),
                Padding(padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0)),
                BuildWidgets.buildInputField(
                  context,
                  isRequired: true,
                  label: TextConstants.login_page_form_login_text,
                  inputField: BuildWidgets.buildBasicTextFormField(
                      context,
                      controller: _resetPasswordEmail,
                      validator: (String value) {
                        if (value == null || value.isEmpty) {
                          return TextConstants.non_empty_input_alert;
                        }
                        return null;
                      }
                  ),
                )
              ],
            ),
          ),
        ),
        actions: [
          TextButton(
            child: Text(TextConstants.login_page_reset_password_title, style: Theme.of(context).textTheme.headline5.copyWith(color: Theme.of(context).accentColor),),
            onPressed: () {
              final form = _resetPasswordFormKey.currentState;
              if (form.validate()) {
                form.save();
                Navigator.of(context).pop();
                _loginService.resetPassword(
                    email: _resetPasswordEmail.text.trim(),
                    onError: () {
                      BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.login_page_reset_password_error);
                    },
                    onSuccess: () {
                      BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.login_page_reset_password_success);
                    });
              }
            },
          ),
          TextButton(
            child: Text(TextConstants.login_page_reset_password_dialog_back, style: Theme.of(context).textTheme.headline5.copyWith(color: Theme.of(context).accentColor),),
            onPressed: () {
              Navigator.of(newContext).pop();
            },
          )
        ],
      ),
    );
  }

}

