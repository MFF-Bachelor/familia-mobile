import 'package:familia_mobile/app/pages/common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/text_constants.dart';
import 'package:flutter/material.dart';

class LoginPageFooter extends StatefulWidget {
  final onRegistrationPressed;

  //constructor
  const LoginPageFooter({@required this.onRegistrationPressed, Key key}) : super(key: key);

  _LoginPageFooterState createState() => _LoginPageFooterState();
}

class _LoginPageFooterState extends State<LoginPageFooter> {
  @override
  Widget build(BuildContext context) {
    return BuildWidgets.buildBasicOutlinedButton(context,
      text: TextConstants.registration_form_page_submit,
      onPressed: widget.onRegistrationPressed);
  }
}
