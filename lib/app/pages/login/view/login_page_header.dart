import '../../common/text_constants.dart';
import 'package:familia_mobile/app/pages/about_us/view/about_us.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPageHeader extends StatefulWidget {
  const LoginPageHeader({Key key}) : super(key: key);

  @override
  _LoginPageHeaderState createState() => _LoginPageHeaderState();
}

class _LoginPageHeaderState extends State<LoginPageHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 5),
      color: Theme.of(context).primaryColor,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 45,
                child: IconButton(
                    icon: Icon(
                      Icons.family_restroom,
                      color: Colors.white,
                    ),
                  onPressed: () {  },
                ),
              ),
              Text(
                TextConstants.logo_text,
                style: Theme.of(context).textTheme.headline2,
              ),
              IconButton(
                icon: Icon(
                  Icons.info,
                  size: 25,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => AboutUsPage()),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
