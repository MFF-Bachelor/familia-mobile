import 'package:community_material_icon/community_material_icon.dart';
import '../common/text_constants.dart';
import 'package:familia_mobile/app/entities/recip_ingredient.dart';
import 'package:familia_mobile/app/entities/recipe.dart';
import 'package:familia_mobile/app/entities/recipe_category.dart';
import 'package:familia_mobile/app/entities/recipe_instruction.dart';
import 'package:familia_mobile/app/enums/recipe_ingredient_unit_enum.dart';
import 'package:familia_mobile/app/enums/recipe_preparation_time_unit_enum.dart';
import '../common/build_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tags/flutter_tags.dart';

class Widgets {
  static Widget buildMyRecipeCard(BuildContext context, {
    @required Recipe recipe,
    @required Function onCardPressed
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
      child: InkWell(
        child: BuildWidgets.buildBasicTitledCard(
          context,
          cardTitle: recipe.title,
          widgetBeforeTitle: recipe.willCook ? Icon(CommunityMaterialIcons.pot_steam, color: Theme.of(context).primaryColor,) : SizedBox(),
          height: 115,
          bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
          leftSidedVerticalWidget:SizedBox(width: 0),
          rightTopWidget: recipe.isFavourite ? Icon(Icons.favorite, color: Color(0xFFFC5451),) : SizedBox(),
          firstRowWidget: recipe.servings == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
              icon: Icons.room_service,
              text: recipe.servings.toString() + " porc."
          ),
          secondRowWidget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              recipe.preparationTime == null || recipe.recipePreparationTimeUnit == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
                  icon: Icons.timer,
                  text: recipe.preparationTime.toString() + " " + RecipePreparationTimeUnitEnum.values.byName(recipe.recipePreparationTimeUnit.name).titleSKAbbr
              ),
              recipe.difficulty == 0 ? SizedBox() : Row(
                children: [
                  Icon(
                    CommunityMaterialIcons.chef_hat,
                    color: recipe.difficulty >= 1 ? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
                  ),
                  Icon(
                    CommunityMaterialIcons.chef_hat,
                    color: recipe.difficulty >= 2 ? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
                  ),
                  Icon(
                    CommunityMaterialIcons.chef_hat,
                    color: recipe.difficulty >= 3 ? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
                  ),
                ],
              )
            ],
          ),
        ),
        onTap: () => onCardPressed(),
      ),
    );
  }

  static final priorities = {
    1: Color(0xFFFC5451),
    2: Color(0xFFFCD638),
    3: Color(0xFF69B04A)
  };

  static buildDoneButton({@required Function onPressed}) {
    return IconButton(
        icon: Icon(
          Icons.check,
          color: Colors.white,
        ),
        onPressed: onPressed
    );
  }

  static buildDifficultyField(BuildContext context, {@required difficulty, @required onDifficultyChange}) {
    difficulty ??= 0;

    return Column(
      children: [
        Text(
          TextConstants.recipe_difficulty,
          style: Theme.of(context).textTheme.headline5,
        ),
        Container(
          height: 60,
          child: Row(
            children: [
              buildDifficultyItem(difficulty, 1, context, onDifficultyChange),
              buildDifficultyItem(difficulty, 2, context, onDifficultyChange),
              buildDifficultyItem(difficulty, 3, context, onDifficultyChange),
            ],
          ),
        ),
      ],
    );
  }

  static buildFavouriteField(
      BuildContext context,
      {
        @required isFavourite,
        @required onFavouriteChange,
        @required title,
        @required icon
      }) {
    isFavourite ??= false;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.headline5,
        ),
        Container(
          height: 60,
          child: InkWell(
            child: Icon(
              icon,
              size: 35,
              color: isFavourite ? Color(0xFFFC5451) : Theme.of(context).primaryColorLight,
            ),
            onTap: () {
              onFavouriteChange();
            },
          ),
        ),
      ],
    );
  }

  static buildWillCookField(
      BuildContext context,
      {
        @required willCook,
        @required title,
        @required onChange
      }) {
    willCook ??= false;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.headline5,
        ),
        Container(
          height: 60,
          width: 34,
          child: Transform.scale(
            scale: 1.5,
            child: Checkbox(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              checkColor: Colors.white,
              activeColor: Theme.of(context).primaryColor,
              value: willCook,
              onChanged: onChange
            ),
          ),
        )
      ]
    );
  }

  static buildRatingField(BuildContext context, {@required rating, @required onRatingChange}) {
    rating ??= 0;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          TextConstants.recipe_rating,
          style: Theme.of(context).textTheme.headline5,
        ),
        Container(
          height: 60,
          child: Row(
            children: [
              buildRatingItem(rating, 1, context, onRatingChange),
              buildRatingItem(rating, 2, context, onRatingChange),
              buildRatingItem(rating, 3, context, onRatingChange),
              buildRatingItem(rating, 4, context, onRatingChange),
              buildRatingItem(rating, 5, context, onRatingChange),
            ],
          ),
        ),
      ],
    );
  }

  static InkWell buildDifficultyItem(difficulty, representedValue, BuildContext context, onDifficultyChange) {
    return InkWell(
      child: Icon(
        CommunityMaterialIcons.chef_hat,
        size: 35,
        color: difficulty >= representedValue ? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
      ),
      onTap: () {
        onDifficultyChange(representedValue);
      },
    );
  }

  static InkWell buildRatingItem(rating, representedValue, BuildContext context, onRatingChange) {
    return InkWell(
      child: Icon(
        CommunityMaterialIcons.star,
        size: 35,
        color: rating >= representedValue ? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
      ),
      onTap: () {
        onRatingChange(representedValue);
      },
    );
  }

  static buildRecipeIngredients(
      BuildContext context,
      {
        @required recipeIngredients,
        @required recipeIngredientUnits,
        @required Function onRemoveTap,
        @required Function onAddTap,
        @required recipeIngredientAmountControllers,
        @required recipeIngredientNameControllers,
      }) {
    recipeIngredients ??= [];
    List<Widget> result = [];

    var index = 0;
    recipeIngredients.forEach((recipeIngredient) {
      result.add(
          Row(
            children: [
              Expanded(
                flex: 20,
                child: BuildWidgets.buildInputField(
                  context,
                  inputField: BuildWidgets.buildBasicTextFormField(
                    context,
                    keyboardType: TextInputType.number,
                    controller: recipeIngredientAmountControllers[index],
                  ),
                ),
              ),
              SizedBox(width: 5,),
              Expanded(
                flex: 25,
                child: BuildWidgets.buildInputField(
                  context,
                  inputField: BuildWidgets.buildBasicDropdownFormField(
                      context,
                      items: recipeIngredientUnits,
                      value: recipeIngredient.unit,
                      enabled: true,
                      onChanged: (value) {
                        recipeIngredient.unit = value;
                      }
                  ),
                ),
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 60,
                child: BuildWidgets.buildInputField(
                  context,
                  inputField: BuildWidgets.buildBasicTextFormField(
                    context,
                    enabled: true,
                    controller: recipeIngredientNameControllers[index],
                  ),
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                  child: Icon(
                    Icons.remove_circle_outline,
                    size: 25,
                    color: Theme.of(context).primaryColor,
                  ),
                  onTap: () => onRemoveTap(recipeIngredient)
              ),
            ],
          )
      );
      index++;
    });

    result.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
                child: Icon(
                  Icons.add_circle_outline,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
                onTap: () => onAddTap()
            ),
          ],
        )
    );

    return Column(
      children: result,
    );
  }

  static buildRecipeInstructions(
      BuildContext context,
      {
        @required recipeInstructions,
        @required recipeInstructionDescriptionControllers,
        @required Function onRemoveTap,
        @required Function onAddTap,
      }) {
    recipeInstructions ??= [];
    recipeInstructionDescriptionControllers ??= [];
    List<Widget> result = [];

    var index = 0;
    recipeInstructions.forEach((recipeInstruction) {
      result.add(
          Row(
            children: [
              Expanded(
                flex: 0,
                child: Text(recipeInstruction.ordering.toString())
              ),
              SizedBox(width: 5),
              Expanded(
                flex: 60,
                child: BuildWidgets.buildInputField(
                  context,
                  inputField: BuildWidgets.buildBasicTextFormField(
                    context,
                    maxLines: 5,
                    enabled: true,
                    controller: recipeInstructionDescriptionControllers[index],
                  ),
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                  child: Icon(
                    Icons.remove_circle_outline,
                    size: 25,
                    color: Theme.of(context).primaryColor,
                  ),
                  onTap: () => onRemoveTap(recipeInstruction)
              ),
            ],
          )
      );
      index++;
    });

    result.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
                child: Icon(
                  Icons.add_circle_outline,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
                onTap: () => onAddTap()
            ),
          ],
        )
    );

    return Column(
      children: result,
    );
  }

  static buildRecipeCategories(BuildContext context, {
      @required List<RecipeCategory> recipeCategories,
      @required activeCategories,
      @required Function(RecipeCategory category) onCategoryTap
    }) {
    List<Widget> childrenCategories = [];

    recipeCategories.forEach((recipeCategory) {
      childrenCategories.add(
          Padding(
            padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
            child: Column(
              children: [
                SizedBox(height: 5,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      recipeCategory.name,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                Tags(
                  itemCount: recipeCategory.childrenCategories.length,
                  itemBuilder: (int index) {
                    return ItemTags(
                      index: index,
                      textColor: Theme.of(context).primaryColor,
                      activeColor: Theme.of(context).primaryColor,
                      title: recipeCategory.childrenCategories[index].name,
                      active: activeCategories.any((activeCategory) {
                        return activeCategory.id == recipeCategory.childrenCategories[index].id;
                      }),
                      customData: recipeCategory.childrenCategories[index],
                      textStyle: Theme.of(context).textTheme.bodyText1,
                      onPressed: (Item item) {
                        onCategoryTap(item.customData);
                      },
                    );
                  },
                )
              ],
            ),
          )
      );
    });

    return Column(
      children: childrenCategories,
    );
  }

  static buildOnlySelectedRecipeCategories(
      BuildContext context, {
    @required activeCategories,
    @required Function(RecipeCategory category) onCategoryTap
  }) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Kategórie",
              style: Theme.of(context).textTheme.headline3,
            ),
          ],
        ),
        SizedBox( height: 20,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
              Tags(
                itemCount: activeCategories.length,
                itemBuilder: (int index) {
                  return ItemTags(
                    pressEnabled: false,
                    index: index,
                    textColor: Theme.of(context).primaryColor,
                    activeColor: Theme.of(context).primaryColor,
                    title: activeCategories[index].name,
                    customData: activeCategories[index],
                    textStyle: Theme.of(context).textTheme.bodyText1
                  );
                },
              )
          ],
        ),
      ],
    );
  }

  static buildViewRecipeIngredients(
    BuildContext context, {
    @required List<RecipeIngredient> recipeIngredients
  }){
    List<Widget> recipeIngredientsWidgets = [];
    recipeIngredientsWidgets.add(
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "Suroviny",
            style: Theme.of(context).textTheme.headline3,
          ),
        ],
      ),
    );

    recipeIngredientsWidgets.add(
        SizedBox( height: 20,)
    );

    recipeIngredients.forEach((recipeIngredient) {
      recipeIngredientsWidgets.add(
        Row(
          children: [
            Text(
              recipeIngredient.amount != null && recipeIngredient.unit != null ?
              recipeIngredient.amount.toString() + " " + RecipeIngredientUnitEnum.values.byName(recipeIngredient.unit.name).titleSK : "",
              style: Theme.of(context).textTheme.bodyText1),
            SizedBox(width: 5,),
            Text(
              recipeIngredient.name != null ? recipeIngredient.name : "",
              style: Theme.of(context).textTheme.bodyText1,)
          ],
        )
      );

      recipeIngredientsWidgets.add(
          SizedBox( height: 10,)
      );
    });

    return Column(
      children: recipeIngredientsWidgets
    );
  }

  static buildViewRecipeInstructions(
      BuildContext context, {
        @required List<RecipeInstruction> recipeInstructions
  }) {
    List<Widget> recipeInstructionWidgets = [];
    recipeInstructionWidgets.add(
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "Postup",
            style: Theme.of(context).textTheme.headline3,
          ),
        ],
      ),
    );

    recipeInstructionWidgets.add(
        SizedBox( height: 20,)
    );

    recipeInstructions.forEach((recipeInstruction) {
      recipeInstructionWidgets.add(
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Color(0x962F4B61)),
                  shape: BoxShape.circle,
                  color: Colors.white
                ),
                child: Center(
                  child: Text(
                  recipeInstruction.ordering.toString(),
                  style: Theme.of(context).textTheme.bodyText1),
                ),
              ),
              SizedBox(width: 10,),
              Flexible(
                child: Text(
                  recipeInstruction.description != null ? recipeInstruction.description : "",
                  style: Theme.of(context).textTheme.bodyText1,),
              )
            ],
          )
      );

      recipeInstructionWidgets.add(
          SizedBox( height: 10,)
      );
    });

    return Column(
        children: recipeInstructionWidgets
    );
  }

  static buildViewInfoField(
    BuildContext context, {
      IconData icon,
      Widget alternativeIcon,
      @required text,
      Function onPressed
    }) {
      return InkWell(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              icon == null ? alternativeIcon
                  : Icon(icon, color: Theme.of(context).accentColor),
              SizedBox(height: 5,),
              Text(text.toString(), style: Theme.of(context).textTheme.headline4.copyWith(color: Theme.of(context).accentColor),)
            ],
          ),
        ),
        onTap: onPressed,
      );
  }
}





