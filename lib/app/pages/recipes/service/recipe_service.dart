import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/recipe.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/recipe_preparation_time_unit_enum.dart';
import 'package:familia_mobile/app/enums/recipes_filter_enum.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class RecipeService {
  ///Returns type of query result
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return CustomQueryResult.EXCEPTION;
    }

    if (result.isLoading) {
      return CustomQueryResult.LOADING;
    }

    var success = result.data[Queries.GET_USER_RECIPES][Queries.SUCCESS];
    if (success) {
      sessionService.saveCookieFromApiResponse(result);
      return CustomQueryResult.SUCCESS;
    } else {
      return CustomQueryResult.FAIL;
    }
  }

  Future<void> runCreateRecipeMutation(
      RunMutation runCreateRecipeMutation,
      {
        @required context,
        @required String title,
        servings,
        caloriesPerServing,
        source,
        preparationTime,
        preparationTimeUnit,
        description,
        @required difficulty,
        @required recipeIngredients,
        @required recipeInstructions,
        @required recipeCategories,
        @required creator,
        @required willCook,
      }
  ) async {
    runCreateRecipeMutation({
      "inputData": {
        "title": title,
        "servings": servings,
        "caloriesPerServing": caloriesPerServing,
        "source": source,
        "preparationTime": preparationTime,
        "preparationTimeUnit": preparationTimeUnit,
        "difficulty": difficulty,
        "description": description,
        "recipeIngredients": recipeIngredients,
        "recipeInstructions": recipeInstructions,
        "willCook": willCook,
        "recipeCategories": recipeCategories,
        "creator": creator
      }
    });
  }

  Future<void>  runSetRecipeMutation(
      RunMutation runSetRecipeMutation,
      {
        @required context,
        @required recipeId,
        @required String title,
        servings,
        caloriesPerServing,
        source,
        preparationTime,
        preparationTimeUnit,
        description,
        @required difficulty,
        @required recipeIngredients,
        @required recipeInstructions,
        @required recipeCategories,
        @required willCook,
        comment,
        rating,
        favourite
      }) async {
    runSetRecipeMutation({
      "inputData": {
        "recipeId" : recipeId,
        "title": title,
        "servings": servings,
        "caloriesPerServing": caloriesPerServing,
        "source": source,
        "preparationTime": preparationTime,
        "preparationTimeUnit": preparationTimeUnit,
        "difficulty": difficulty,
        "description": description,
        "recipeIngredients": recipeIngredients,
        "recipeInstructions": recipeInstructions,
        "willCook": willCook,
        "recipeCategories": recipeCategories,
        "comment": comment,
        "rating": rating,
        "favourite": favourite
      }
    });
  }

  Future<void> onDeleteButtonTap(
      RunMutation runDeleteRecipeMutation,
      {
        @required id,
        @required onError,
      }) async {
    runDeleteRecipeMutation({
      "userRecipeId": id
    });
  }

  Future<void> onShareButtonTap(
      RunMutation runShareRecipeMutation,
      {
        @required id,
        @required userUid
      }) async {
    runShareRecipeMutation({
      "userRecipeId": id,
      "uid": userUid
    });
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success) {
        return CustomMutationResult.SUCCESS;
      } else {
        return CustomMutationResult.FAIL;
      }
    }
  }

  ///Returns filtered and sorted recipes based on given params
  List<Recipe> filterVisibleRecipes({ @required List<Recipe> recipes, @required appliedFilters, @required appliedSort, @required String searchString}) {
    recipes.removeWhere((recipe) {
      bool toRemoveWithFilter = false;
      if ((appliedFilters != null && appliedFilters.length != 0)) {
        toRemoveWithFilter = true;
        appliedFilters?.forEach((filter) {
          switch (filter) {
            case RecipesFilterEnum.EASY:
              if (recipe.difficulty == 1) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case RecipesFilterEnum.MEDIUM:
              if (recipe.difficulty == 2) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case RecipesFilterEnum.HARD:
              if (recipe.difficulty == 3) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case RecipesFilterEnum.FAVOURITE:
              if (recipe.isFavourite) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case RecipesFilterEnum.QUICK_PREPARATION:
              if ((recipe.recipePreparationTimeUnit == RecipePreparationTimeUnitEnum.MINUTE
                  || recipe.recipePreparationTimeUnit == RecipePreparationTimeUnitEnum.SECOND)
                  && recipe.preparationTime <= 30) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case RecipesFilterEnum.HIGH_RATED:
              if (recipe.rating != null && recipe.rating > 3) {
                toRemoveWithFilter = false;
                return;
              }
              break;
          }
        });
      }

      bool toRemoveWithSearch = false;
      if (searchString != null && searchString != '') {
        String lowerCaseSearchString = searchString.toLowerCase();
        toRemoveWithSearch = true;
        if (recipe.title.toLowerCase().contains(lowerCaseSearchString)) {
          toRemoveWithSearch = false;
        }
      }

      return toRemoveWithFilter || toRemoveWithSearch;
    });

    recipes.sort((Recipe recipe1, Recipe recipe2) {
      switch(appliedSort) {
        case RecipesSortEnum.NAME:
          return recipe1.title.toLowerCase().compareTo(recipe2.title.toLowerCase());
          break;
        case RecipesSortEnum.CREATED_DATE_DESC:
          return recipe1.created == null || recipe2.created == null ? 0 : recipe2.created.compareTo(recipe1.created);
          break;
        case RecipesSortEnum.CREATED_DATE_ASC:
          return recipe1.created == null || recipe2.created == null ? 0 : recipe1.created.compareTo(recipe2.created);
          break;
        case RecipesSortEnum.RATING:
          return recipe1.rating == null || recipe2.rating == null ? 0 : recipe2.rating.compareTo(recipe1.rating);
          break;
        case RecipesSortEnum.CALORIES:
          return recipe1.caloriesPerServing == null || recipe2.caloriesPerServing == null ? 0 : recipe1.caloriesPerServing.compareTo(recipe2.caloriesPerServing);
          break;
        case RecipesSortEnum.DIFFICULTY:
          return recipe1.difficulty == null || recipe2.difficulty == null ? 0 : recipe1.difficulty.compareTo(recipe2.difficulty);
          break;
        case RecipesSortEnum.PREPARATION_TIME:
          return recipe1.preparationTime == null || recipe2.preparationTime == null ? 0 : recipe1.preparationTime.compareTo(recipe2.preparationTime);
          break;
        default:
          return 0;
      }
    });

    return recipes;
  }
}