import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/recipe.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/recipes_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/recipes/service/recipe_service.dart';
import 'package:familia_mobile/app/pages/recipes/view/recipes_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../common/text_constants.dart';
import '../widgets.dart';

class RecipesPageBody extends StatefulWidget {
  RecipesPageBody({Key key}) : super(key: key);

  @override
  RecipesPageBodyState createState() => RecipesPageBodyState();
}

class RecipesPageBodyState extends State<RecipesPageBody> {
  List<Recipe> recipes = [];
  List<Recipe> willCookRecipes = [];
  List<Recipe> myRecipes = [];
  List<Widget> tabs = [];
  VoidCallback myRefetch;
  List<dynamic> appliedFilters;
  RecipesSortEnum appliedSort = RecipesSortEnum.CREATED_DATE_DESC;
  String searchString = '';

  final RecipeService _recipeService = RecipeService();

  @override
  Widget build(BuildContext context) {
    tabs = [
      Tab(
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(TextConstants.my_recipes)
          )
      ),
      Tab(
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(TextConstants.will_cook_recipes)
          )
      ),
    ];

    return Scaffold(
      body: DefaultTabController(
        length: tabs.length,
        child: Builder(builder: (BuildContext context) {
          return Scaffold(
              appBar: _buildTabs(),
              body: Query(
                builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
                  myRefetch = refetch;
                  CustomQueryResult queryResult = _recipeService.checkQueryResult(result, refetch: refetch, fetchMore: fetchMore);
                  switch (queryResult) {
                    case CustomQueryResult.EXCEPTION:
                      return BuildWidgets.queryExceptionWidget(context, exceptionMessage: result.exception.toString());
                      break;
                    case CustomQueryResult.LOADING:
                      return BuildWidgets.queryLoadingWidget(context);
                      break;
                    case CustomQueryResult.FAIL:
                      return BuildWidgets.queryFailWidget(context);
                      break;
                    case CustomQueryResult.SUCCESS:
                      _parseQueryResult(result);
                      return _buildBody();
                      break;
                  }
                  return BuildWidgets.queryFailWidget(context);
                },
                options: QueryOptions(
                  document: gql(Queries.getUserRecipesQuery),
                ),
              )
          );
        }),
      ),
      floatingActionButton: BuildWidgets.floatingButtonAddItem(
          context,
          nextRoute: MaterialPageRoute(builder: (context) => RecipesDetailPage(
              pageType: RecipesDetailPageTypeEnum.CREATE_PAGE,
              recipe: Recipe()
          )),
          onReturn: () {
            myRefetch();
            setState(() {});
          }
      ),
    );
  }

  _buildTabs() {
    return PreferredSize(
      preferredSize: Size.fromHeight(80.0),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
        child: TabBar(
            isScrollable: true,
            indicatorColor: Colors.transparent,
            labelColor: Theme.of(context).primaryColor,
            labelStyle: Theme.of(context).textTheme.headline3,
            unselectedLabelStyle: Theme.of(context).textTheme.headline3.copyWith(color: Colors.deepPurple),
            tabs: tabs
        ),
      ),
    );
  }

  ///Parse data (assignedTasks and otherCreatedTasks) from query result, apply filters and sort and save
  ///the data into local variables myTasks and otherCreatedTasks
  _parseQueryResult(QueryResult result) {
    myRecipes = [];
    willCookRecipes = [];
    result.data[Queries.GET_USER_RECIPES]['data']['recipes']?.forEach((recipe) {
      var recipeObject = Recipe.fromApiObject(recipe);

      myRecipes.add(recipeObject);

      if (recipeObject.willCook) {
        willCookRecipes.add(recipeObject);
      }
    });

     myRecipes = _recipeService.filterVisibleRecipes(recipes: myRecipes, appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: searchString);
     willCookRecipes = _recipeService.filterVisibleRecipes(recipes: willCookRecipes, appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: searchString);
  }

  _buildBody() {
    return TabBarView(
        children: [
          myRecipes.isEmpty ? BuildWidgets.queryNoDataWidget(context) : _buildRecipesList(myRecipes),
          willCookRecipes.isEmpty ? BuildWidgets.queryNoDataWidget(context) : _buildRecipesList(willCookRecipes),
        ]
    );
  }

  ListView _buildRecipesList(List<Recipe> recipes) {
    return ListView.builder(
        itemCount: recipes.length,
        itemBuilder: (context, index) {
          return Widgets.buildMyRecipeCard(
              context,
              recipe: recipes[index],
              onCardPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RecipesDetailPage(
                        pageType: RecipesDetailPageTypeEnum.VIEW_PAGE,
                        recipe: recipes[index]
                    ))
                ).then((needRefetch) {
                  myRefetch();
                  setState(() {});
                });
              }
          );
        }
    );
  }

  void applyFilters({List<dynamic> appliedFilters, dynamic appliedSort, String searchString}) {
    if (appliedFilters != null)
      this.appliedFilters = appliedFilters;
    if (appliedSort != null)
      this.appliedSort = appliedSort;
    if (searchString != null)
      this.searchString = searchString;
    setState(() {});
  }
}