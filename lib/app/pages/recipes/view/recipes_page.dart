import '../../common/text_constants.dart';
import 'package:familia_mobile/app/enums/recipes_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/filter_sort_panel.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/recipes/view/recipes_page_body.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class RecipesPage extends StatefulWidget {
  RecipesPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RecipesPageState createState() => _RecipesPageState();
}

class _RecipesPageState extends State<RecipesPage> {
  PanelController _panelController = new PanelController();
  final GlobalKey<FilterSortPanelState> _recipesFilterPanelKey = GlobalKey();
  final GlobalKey<RecipesPageBodyState> _recipesPageBodyKey = GlobalKey();

  List<dynamic> _appliedFilters = [];
  var _appliedSort;
  String _searchString;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return BuildWidgets.customSlidingUpPanel(
      controller: _panelController,
      panel: FilterSortPanel<RecipesFilterEnum, RecipesSortEnum>(
        key: _recipesFilterPanelKey,
        activeSort: RecipesSortEnum.CREATED_DATE_DESC,
        entityFilterEnumValues: RecipesFilterEnum.values,
        entitySortEnumValues: RecipesSortEnum.values,
        filtersDefinition: RecipesFilterEnumExtension.definition,
        sortsDefinition: RecipesSortEnumExtension.definition,
        onFilterSwitch: ({dynamic filter, List<dynamic> appliedFilters, dynamic appliedSort}) {
          if (filter != null) {
            RecipesFilterEnumExtension.definition[filter]['active'] = !RecipesFilterEnumExtension.definition[filter]['active'];
          }
          _appliedFilters = appliedFilters;
          _appliedSort = appliedSort;
          _recipesPageBodyKey.currentState.applyFilters(appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: _searchString);
        },
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
            body: Center(
                child: Container(
                  padding: EdgeInsets.only(top: _statusBarHeight),
                  child: Column(
                    children: <Widget>[
                      TwoRowsHeader(
                        title: TextConstants.recipes_page_header_text,
                        rightSidedWidget: IconButton(
                          icon: Icon(Icons.sort),
                          onPressed: () {
                            _panelController.open();
                          },
                          color: Colors.white,
                        ),
                        secondRowWidget: BuildWidgets.buildSearchBar(context,
                            onChanged: (String newValue) {
                              _searchString = newValue;
                              _recipesPageBodyKey.currentState.applyFilters(appliedFilters: _appliedFilters, appliedSort: _appliedSort, searchString: newValue);
                            }),
                      ),
                      Expanded(
                          child: RecipesPageBody(key: _recipesPageBodyKey,)
                      ),
                    ],
                  ),
                )
            )
        ),
      ),
    );
  }
}