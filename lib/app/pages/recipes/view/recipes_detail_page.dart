import 'package:community_material_icon/community_material_icon.dart';
import 'package:enum_to_string/enum_to_string.dart';
import '../../../services/graphql_service/graphql_helper.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/recip_ingredient.dart';
import 'package:familia_mobile/app/entities/recipe.dart';
import 'package:familia_mobile/app/entities/recipe_category.dart';
import 'package:familia_mobile/app/entities/recipe_instruction.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/recipe_ingredient_unit_enum.dart';
import 'package:familia_mobile/app/enums/recipe_preparation_time_unit_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/recipes/service/recipe_service.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:intl/intl.dart';

import '../widgets.dart';

class RecipesDetailPage extends StatefulWidget {
  final RecipesDetailPageTypeEnum pageType;
  Recipe recipe;

  RecipesDetailPage({Key key, @required this.pageType, @required this.recipe}) : super(key: key);

  @override
  _RecipesDetailPageState createState() => _RecipesDetailPageState();
}

class _RecipesDetailPageState extends State<RecipesDetailPage> {
  Map<String, dynamic> data = {};
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _servings = TextEditingController();
  TextEditingController _caloriesPerServing = TextEditingController();
  TextEditingController _sourceController = TextEditingController();
  TextEditingController _recipePreparationTime = TextEditingController();
  TextEditingController _commentController = TextEditingController();
  RecipePreparationTimeUnitEnum _recipePreparationTimeUnit;
  List<RecipeIngredient> _recipeIngredients = [];
  List<TextEditingController> _recipeIngredientAmountControllers = [];
  List<TextEditingController> _recipeIngredientNameControllers = [];
  List<RecipeInstruction> _recipeInstructions = [];
  List<TextEditingController> _recipeInstructionDescriptionControllers = [];
  List<RecipeCategory> _selectedCategories = [];
  int _difficulty;
  int _rating;
  bool _isFavourite;
  bool _willCook;

  var dateFormat = DateFormat('dd.MM.yyyy');
  var timeFormat = DateFormat('HH:mm');
  var dateTimeFormat = DateFormat('HH:mm dd.MM.yyyy');
  List<DropdownMenuItem> preparationTimeUnits;
  List<DropdownMenuItem> recipeIngredientUnits;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _dialogFormKey = GlobalKey<FormState>();
  ScrollController scrollController = ScrollController();
  RecipeService _recipeService = RecipeService();
  GraphGLHelper _graphGLHelper = GraphGLHelper();
  Widget recipeCategoriesWidgets;
  bool updating = false;
  User _assignee;
  List<DropdownMenuItem> assigneeDropdownItems = [];

  List<RecipeCategory> _recipeCategories = [];

  Recipe _recipe;

  @override
  void initState() {
    _recipe = widget.recipe;
    preparationTimeUnits = RecipePreparationTimeUnitEnum.values.map<DropdownMenuItem>((preparationTimeUnit) {
      return DropdownMenuItem(
          value: preparationTimeUnit,
          child: Text(preparationTimeUnit.titleSK)
      );
    }).toList();

    recipeIngredientUnits = RecipeIngredientUnitEnum.values.map<DropdownMenuItem>((recipeIngredientUnit) {
      return DropdownMenuItem(
          value: recipeIngredientUnit,
          child: Text(recipeIngredientUnit.titleSK)
      );
    }).toList();

    _titleController = TextEditingController(text: _recipe.title == null ? '' : _recipe.title);
    _descriptionController = TextEditingController(text: _recipe.description == null ? '' : _recipe.description);
    _servings =  TextEditingController(text: _recipe.servings == null ? '' : _recipe.servings.toString());
    _caloriesPerServing = TextEditingController(text: _recipe.caloriesPerServing == null ? null : _recipe.caloriesPerServing.toString());
    _difficulty = _recipe.difficulty == null ? null : _recipe.difficulty;
    _sourceController = TextEditingController(text: _recipe.source == null ? '' : _recipe.source);
    _recipePreparationTime = TextEditingController(text: _recipe.preparationTime == null ? null : _recipe.preparationTime.toString());
    _recipePreparationTimeUnit = _recipe.recipePreparationTimeUnit == null ? null : _recipe.recipePreparationTimeUnit;
    _recipeIngredients = _recipe.recipeIngredients == null ? [] : _recipe.recipeIngredients;
    _recipeIngredientAmountControllers = [];
    _recipeIngredientNameControllers = [];
    _recipeInstructions = _recipe.recipeInstructions == null ? [] : _recipe.recipeInstructions;
    _recipeInstructionDescriptionControllers = [];
    _selectedCategories = _recipe.recipeCategories == null ? [] : _recipe.recipeCategories;
    _commentController = TextEditingController(text: _recipe.comment == null ? '' : _recipe.comment);
    _rating = _recipe.rating == null ? null : _recipe.rating;
    _isFavourite = _recipe.isFavourite == null ? false : _recipe.isFavourite;
    _willCook = _recipe.willCook == null ? false : _recipe.willCook;


    _recipe.recipeIngredients?.forEach((recipeIngredient) {
      _recipeIngredientAmountControllers.add(TextEditingController(text: recipeIngredient.amount == null ? '' : recipeIngredient.amount.toString()));
      _recipeIngredientNameControllers.add(TextEditingController(text: recipeIngredient.name == null ? '' : recipeIngredient.name.toString()));
    });

    _recipe.recipeInstructions?.forEach((recipeInstruction) {
      _recipeInstructionDescriptionControllers.add(TextEditingController(text: recipeInstruction.description == null ? '' : recipeInstruction.description));
    });

    // relationships where userTo is not null (removes all relationships with inactive users)
    var relationshipsWithUserTo = List.from(apiAuthService.currentUser.confirmedRelationships);
    relationshipsWithUserTo.removeWhere((element) => element.userTo == null);
    assigneeDropdownItems = relationshipsWithUserTo.map<DropdownMenuItem>((relationship) {
      return DropdownMenuItem(
          value: relationship.userTo,
          child: Text(relationship.userTo.firstName + " " + relationship.userTo.lastName)
      );
    }).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        OneRowHeader(
                            title: widget.pageType == RecipesDetailPageTypeEnum.VIEW_PAGE || widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE
                                ? TextConstants.recipe_detail_page_view_shopping_list_title
                                : TextConstants.recipe_detail_page_new_shopping_list_title,
                            rightSidedWidget: (widget.pageType == RecipesDetailPageTypeEnum.VIEW_PAGE)
                                ? _buildEditButton()
                                : _buildSubmitButton(),
                        ),
                        Expanded(
                          child: Query( //getRecipeCategories
                            builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {

                              if (result.hasException) {
                                return Text(result.exception.toString());
                              }

                              if (result.isLoading) {
                                return BuildWidgets.queryLoadingWidget(context);
                              }

                              var success = result.data[Queries.GET_RECIPE_CATEGORIES][Queries.SUCCESS];
                              if (success) {
                                _parseQueryResult(result);
                                recipeCategoriesWidgets = Widgets.buildRecipeCategories(context,
                                  recipeCategories: _recipeCategories,
                                  onCategoryTap: (RecipeCategory category) {
                                    setState(() {
                                      if (_selectedCategories.contains(category)) {
                                        _selectedCategories.remove(category);
                                      } else {
                                        _selectedCategories.add(category);
                                      }
                                    });
                                  },
                                  activeCategories: _selectedCategories
                                );
                              } else {
                                BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
                              }

                              return _buildRecipeDetailPageBody();
                            },
                            options: QueryOptions(
                              document: gql(Queries.getRecipeCategories),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Visibility(
                        visible: updating,
                        child: Container(
                            color: Colors.black12,
                            child: BuildWidgets.queryLoadingWidget(context)
                        )
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }

  _buildRecipeDetailPageBody() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox( height: 20,),
              (widget.pageType == RecipesDetailPageTypeEnum.VIEW_PAGE)
                  ? _buildViewBody()
                  : _buildFormBody(),
              SizedBox( height: 20,)
            ],
          ),
        ),
      ),
    );
  }

  _buildFormBody() {
    return Wrap(
      spacing: 20.0,
      children: [
        BuildWidgets.buildInputField(
          context,
          label: TextConstants.recipe_name,
          inputField: BuildWidgets.buildBasicTextFormField(
              context,
              controller: _titleController,
              enabled: widget.pageType != RecipesDetailPageTypeEnum.VIEW_PAGE,
              validator: (String value) {
                return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
              }
          ),
        ),
        SizedBox( height: 20),
        BuildWidgets.buildInputField(
          context,
          label: TextConstants.recipe_description,
          inputField: BuildWidgets.buildBasicTextFormField(
              context,
              maxLines: 7,
              controller: _descriptionController,
              enabled: widget.pageType != RecipesDetailPageTypeEnum.VIEW_PAGE
          ),
        ),
        SizedBox( height: 20),
        Row(
          children: [
            Expanded(
              child: BuildWidgets.buildInputField(
                context,
                label: TextConstants.recipe_servings,
                inputField: BuildWidgets.buildBasicTextFormField(
                    context,
                    suffixIcon: Icon(Icons.room_service, color: Theme.of(context).primaryColor,),
                    controller: _servings,
                    enabled: widget.pageType != RecipesDetailPageTypeEnum.VIEW_PAGE,
                    keyboardType: TextInputType.number,
                ),
              ),
            ),
            SizedBox(width: 25,),
            Expanded(
              child: BuildWidgets.buildInputField(
                context,
                label: TextConstants.recipe_calories_per_serving,
                inputField: BuildWidgets.buildBasicTextFormField(
                    context,
                    suffixIcon: Icon(Icons.local_fire_department, color: Theme.of(context).primaryColor,),
                    controller: _caloriesPerServing,
                    enabled: widget.pageType != RecipesDetailPageTypeEnum.VIEW_PAGE,
                    keyboardType: TextInputType.number
                ),
              ),
            ),
            SizedBox(width: 25,),
            Widgets.buildDifficultyField(
              context,
              difficulty: _recipe.difficulty,
              onDifficultyChange: (newDifficulty) {
                setState(() {
                  _difficulty = newDifficulty;
                  _recipe.difficulty = newDifficulty;
                });
              })
          ],
        ),
        SizedBox( height: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              TextConstants.recipe_preparation_time,
              style: Theme.of(context).textTheme.headline5,
            ),
            Row(
              children: [
                Expanded(
                  flex: 40,
                  child: BuildWidgets.buildInputField(
                    context,
                    inputField: BuildWidgets.buildBasicTextFormField(
                        context,
                        controller: _recipePreparationTime,
                        enabled: true,
                        keyboardType: TextInputType.number
                    ),
                  ),
                ),
                SizedBox(width: 30,),
                Expanded(
                  flex: 60,
                  child: BuildWidgets.buildInputField(
                    context,
                    inputField: BuildWidgets.buildBasicDropdownFormField(
                        context,
                        items: preparationTimeUnits,
                        value: _recipePreparationTimeUnit,
                        enabled: true,
                        onChanged: (value) {
                          setState(() {
                            _recipePreparationTimeUnit = value;
                          });
                        }
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        SizedBox( height: 20),
        BuildWidgets.buildInputField(
          context,
          label: TextConstants.recipe_source,
          inputField: BuildWidgets.buildBasicTextFormField(
              context,
              controller: _sourceController,
              enabled: widget.pageType != RecipesDetailPageTypeEnum.VIEW_PAGE
          ),
        ),
        SizedBox( height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Ingrediencie",
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        ),
        SizedBox( height: 20),
        Widgets.buildRecipeIngredients(
          context,
          recipeIngredients: _recipeIngredients,
          recipeIngredientUnits: recipeIngredientUnits,
          recipeIngredientAmountControllers: _recipeIngredientAmountControllers,
          recipeIngredientNameControllers: _recipeIngredientNameControllers,
          onAddTap: () {
            setState(() {
              _recipeIngredients.add(RecipeIngredient());
              _recipeIngredientAmountControllers.add(TextEditingController(text: ''));
              _recipeIngredientNameControllers.add(TextEditingController(text: ''));
            });
            //scrollController.jumpTo(scrollController.position.maxScrollExtent + 70);
          },
          onRemoveTap: (RecipeIngredient ingredient) {
            setState(() {
              var index = _recipeIngredients.indexOf(ingredient);
              _recipeIngredients.removeAt(index);
              _recipeIngredientAmountControllers.removeAt(index);
              //_recipeIngredientAmountControllers.removeAt(index);
              _recipeIngredientNameControllers.removeAt(index);
            });
          },
        ),
        SizedBox( height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Postup",
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        ),
        SizedBox( height: 20),
        Widgets.buildRecipeInstructions(
          context,
          recipeInstructions: _recipeInstructions,
          recipeInstructionDescriptionControllers: _recipeInstructionDescriptionControllers,
          onAddTap: () {
            setState(() {
              _recipeInstructions.add(RecipeInstruction()..ordering = _recipeInstructions.length + 1);
              _recipeInstructionDescriptionControllers.add(TextEditingController(text: ''));
            });
            //scrollController.jumpTo(scrollController.position.maxScrollExtent + 140);
          },
          onRemoveTap: (RecipeInstruction instruction) {
            setState(() {
              var index = _recipeInstructions.indexOf(instruction);
              _recipeInstructions.removeAt(index);
              _recipeInstructionDescriptionControllers.removeAt(index);
              _setRecipeInstructionsOrdering();
            });
          },
        ),
        SizedBox( height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Kategórie",
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        ),
        SizedBox( height: 20),
        recipeCategoriesWidgets,
        Visibility(visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE, child: SizedBox( height: 20)),
        Visibility(
            visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE,
            child: BuildWidgets.buildInputField(
              context,
              label: TextConstants.recipe_comment_text,
              inputField: BuildWidgets.buildBasicTextFormField(
                  context,
                  maxLines: 4,
                  controller: _commentController,
                  enabled: widget.pageType != RecipesDetailPageTypeEnum.VIEW_PAGE
              ),
            ),
        ),
        Visibility(visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE, child: SizedBox( height: 20)),
        Visibility(
            visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE,
            child: Widgets.buildRatingField(
                context,
                rating: _rating,
                onRatingChange: (newRating) {
                  setState(() {
                    _rating = newRating;
                    _recipe.rating = newRating;
                  });
                }),
        ),
        Visibility(visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE, child: SizedBox( height: 10)),
        Visibility(
            visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE,
            child: Widgets.buildFavouriteField(
                context,
                isFavourite: _isFavourite,
                icon: Icons.favorite,
                title: TextConstants.recipe_favourite_text,
                onFavouriteChange: () {
                  setState(() {
                    bool newValue = false;
                    if (!_isFavourite)
                      newValue = true;
                    _isFavourite = newValue;
                    _recipe.isFavourite = newValue;
                  });
                }
            )
        ),
        Visibility(
            visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE,
            child: Widgets.buildWillCookField(
                context,
                willCook: _willCook,
                title: TextConstants.recipe_will_cook_text,
                onChange: (bool value) {
                  setState(() {
                    _willCook = value;
                    _recipe.willCook = value;
                  });
                }
            )
        ),
        Visibility(
            visible: widget.pageType == RecipesDetailPageTypeEnum.UPDATE_PAGE,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _buildRemoveRecipeButton(_recipe)
              ],
            )
        )
      ],
    );
  }

  _buildViewBody() {
    List<Widget> _difficultyIcons = [];
    for (int i = 0; i < _difficulty; i++) {
      _difficultyIcons.add(Icon(CommunityMaterialIcons.chef_hat, color: Theme.of(context).accentColor));
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          _recipe.title,
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _servings.text.isEmpty ? SizedBox() : Widgets.buildViewInfoField(
                context,
                icon: Icons.person,
                text: "${int.parse(_servings.text)} porc."),
            _recipePreparationTime.text.isEmpty || _recipePreparationTimeUnit == null ? SizedBox() : Widgets.buildViewInfoField(
                context,
                icon: Icons.access_time,
                text: "${int.parse(_recipePreparationTime.text)} ${(_recipePreparationTimeUnit).titleSKAbbr}"),
            _caloriesPerServing.text.isEmpty ? SizedBox() : Widgets.buildViewInfoField(
                context,
                icon: Icons.room_service,
                text: "${int.parse(_caloriesPerServing.text)} Kcal/porc.")
          ],
        ),
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _difficulty == 0 ? SizedBox() : Widgets.buildViewInfoField(context,
                alternativeIcon: Row(
                  children: _difficultyIcons,
                ),
                text: _difficulty),
            Widgets.buildViewInfoField(context, icon: Icons.share, text: "Zdieľať", onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: Text ("Zdieľať recept", style: Theme.of(context).textTheme.headline4,),
                    content: Container(
                      width: 1500,
                      height: 200,
                      child: Form(
                        key: _dialogFormKey,
                        child: Column(
                          children: [
                            BuildWidgets.buildInputField(
                                context,
                                label: TextConstants.shopping_list_assignees_label,
                                inputField: BuildWidgets.buildBasicDropdownFormField(
                                    context,
                                    items: assigneeDropdownItems,
                                    value: _assignee,
                                    enabled: true,
                                    validator: (value) {
                                      return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                                    },
                                    onChanged: (value) {
                                      setState(() {
                                        _assignee = value;
                                      });
                                    }
                                ),
                                rightSideWidget: SizedBox()
                            ),
                            SizedBox(height: 20,),
                            _buildShareButton()
                          ],
                        ),
                      ),
                    ),

              ));
            }),
        ],),
        SizedBox(height: 20,),
        Widgets.buildOnlySelectedRecipeCategories(context,
          activeCategories: _selectedCategories,
          onCategoryTap: (RecipeCategory category) {},
        ),
        SizedBox(height: 20,),
        Text(
          "Zdroj",
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 20,),
        Text(
          _recipe.source,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        SizedBox(height: 20,),
        Text(
          "Popis",
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox(height: 20,),
        Text(
          _recipe.description,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        SizedBox(height: 20,),
        Widgets.buildViewRecipeIngredients(context, recipeIngredients: _recipe.recipeIngredients),
        SizedBox(height: 20,),
        Widgets.buildViewRecipeInstructions(context, recipeInstructions: _recipe.recipeInstructions)
      ],
    );
  }

  _buildSubmitButton() {
    return Mutation (
        options: _mutationOptionsBasedOnPageType(),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildDoneButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    updating = true;
                  });
                  _runMutationBasedOnPageType(runMutation);
                }
              });
        });
  }

  _buildShareButton() {
    return Mutation (
        options: _graphGLHelper.entityMutationOptions(
            context,
            entityService: _recipeService,
            mutationDefinition: Mutations.shareRecipeMutation,
            mutationName: Mutations.SHARE_RECIPE,
            onSuccess: (_) {
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.recipe_share_success);
              Navigator.of(context).pop();
            },
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            }
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildBasicButton(
              context,
              text: TextConstants.share_recipe_button_text,
              onPressed: () {
                if (_dialogFormKey.currentState.validate()) {
                  _recipeService.onShareButtonTap(
                      runMutation,
                      id: _recipe.userRecipeId,
                      userUid: _assignee.uid
                  );
                }
              }
          );
        });
  }

  _buildEditButton() {
    return IconButton(
        icon: Icon(
          Icons.edit,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RecipesDetailPage(
                    pageType: RecipesDetailPageTypeEnum.UPDATE_PAGE,
                    recipe: _recipe
                )
            ),
          ).then((value) => Navigator.pop(context, true));
        }
    );
  }

  _buildRemoveRecipeButton(Recipe recipe) {
    return Mutation(
        options: _graphGLHelper.entityMutationOptions(
            context,
            entityService: _recipeService,
            mutationDefinition: Mutations.deleteRecipeMutation,
            mutationName: Mutations.DELETE_RECIPE,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildBasicOutlinedButton(
              context,
              text: TextConstants.delete_recipe_button_text,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    updating = true;
                  });
                  _recipeService.onDeleteButtonTap(
                      runMutation,
                      id: recipe.userRecipeId,
                      onError: () {
                        throw Exception("Custom error: on delete failed");
                      }
                  );
                }
              }
          );
        }
    );
  }

  _runMutationBasedOnPageType(RunMutation runMutation) {
    _summariseRecipeInstructions();
    _summariseRecipeIngredients();
    switch (widget.pageType) {
      case RecipesDetailPageTypeEnum.CREATE_PAGE:
        _recipeService.runCreateRecipeMutation(
            runMutation,
            context: context,
            title: _titleController.text.trim(),
            description: _descriptionController.text.trim(),
            servings: _servings.text.isEmpty ? null : int.parse(_servings.text),
            caloriesPerServing: _caloriesPerServing.text.isEmpty ? null : int.parse(_caloriesPerServing.text),
            difficulty: _difficulty,
            source: _sourceController.text.trim(),
            preparationTime: _recipePreparationTime.text.isEmpty ? null : int.parse(_recipePreparationTime.text),
            preparationTimeUnit: _recipePreparationTimeUnit == null ? null : _recipePreparationTimeUnit.title,
            recipeIngredients: _convertObjectsToJsons(_recipeIngredients),
            recipeInstructions:  _convertObjectsToJsons(_recipeInstructions),
            recipeCategories: _getIdsFromArray(_selectedCategories),
            creator: apiAuthService.currentUser.uid,
            willCook: false
        );
        break;
      case RecipesDetailPageTypeEnum.UPDATE_PAGE:
        _recipeService.runSetRecipeMutation(
            runMutation,
            recipeId: _recipe.userRecipeId,
            context: context,
            title: _titleController.text.trim(),
            description: _descriptionController.text.trim(),
            servings: _servings.text.isEmpty ? null : int.parse(_servings.text),
            caloriesPerServing: _caloriesPerServing.text.isEmpty ? null : int.parse(_caloriesPerServing.text),
            difficulty: _difficulty,
            source: _sourceController.text.trim(),
            preparationTime: _recipePreparationTime.text.isEmpty ? null : int.parse(_recipePreparationTime.text),
            preparationTimeUnit: _recipePreparationTimeUnit == null ? null : _recipePreparationTimeUnit.title,
            recipeIngredients: _convertObjectsToJsons(_recipeIngredients),
            recipeInstructions:  _convertObjectsToJsons(_recipeInstructions),
            recipeCategories: _getIdsFromArray(_selectedCategories),
            willCook: _willCook,
            comment: _commentController.text.trim(),
            rating: _rating,
            favourite: _isFavourite
        );
        break;
      default: return null;
    }
  }

  _mutationOptionsBasedOnPageType() {
    switch (widget.pageType) {
      case RecipesDetailPageTypeEnum.CREATE_PAGE:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _recipeService,
            mutationDefinition: Mutations.createRecipeMutation,
            mutationName: Mutations.CREATE_RECIPE,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      case RecipesDetailPageTypeEnum.UPDATE_PAGE:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _recipeService,
            mutationDefinition: Mutations.setRecipeMutation,
            mutationName: Mutations.SET_RECIPE,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      default: return null;
    }
  }

  void _setRecipeInstructionsOrdering() {
    for(var i = 0 ; i < data["recipeInstructions"].length; i++ ) {
      data["recipeInstructions"][i].ordering = i + 1;
    }
  }

  _parseQueryResult(QueryResult result) {
    result.data[Queries.GET_RECIPE_CATEGORIES]['data']?.forEach((recipeCategory) {
      var recipeCategoryObject = RecipeCategory.fromApiObject(recipeCategory);
      if (!_recipeCategories.any((currentRecipeCategory) => currentRecipeCategory.id == recipeCategoryObject.id))
        _recipeCategories.add(recipeCategoryObject);
    });

  }

  _getIdsFromArray(recipeCategories) {
    var recipeCategoriesIds = [];
    recipeCategories?.forEach((recipeCategory) {
      recipeCategoriesIds.add(recipeCategory.id);
    });
    return recipeCategoriesIds;
  }

  _convertObjectsToJsons(arrayOfObjects) {
    var result = [];
    arrayOfObjects?.forEach((object) {
      result.add(object.toJson());
    });
    return result;
  }

  _summariseRecipeIngredients() {
    for (int i = 0; i < _recipeIngredientAmountControllers.length; i++) {
      if (_recipeIngredientAmountControllers[i].text.isNotEmpty && _recipeIngredientNameControllers[i].text.isNotEmpty) {
        _recipeIngredients[i].amount = int.parse(_recipeIngredientAmountControllers[i].text.trim());
        _recipeIngredients[i].name = _recipeIngredientNameControllers[i].text.trim();
      }
    }
  }

  _summariseRecipeInstructions() {
    for (int i = 0; i < _recipeInstructionDescriptionControllers.length; i++) {
      _recipeInstructions[i].description = _recipeInstructionDescriptionControllers[i].text.trim();
    }
  }
}

enum RecipesDetailPageTypeEnum {
  CREATE_PAGE,
  UPDATE_PAGE,
  VIEW_PAGE
}