import 'text_constants.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:timezone/timezone.dart';

class BuildWidgets {

  ///Creates basic input field for form
  static Widget buildBasicInput({
    @required BuildContext context,
    @required String label,
    @required TextEditingController controller,
    @required bool isRequired,
    @required GlobalKey key,
    bool obscureText = false,
    Function(String) validator,
    String hintText = "",
    double textLeftPadding = 30.0,
    double textTopPadding = 10.0,
    double textBottomPadding = 3.0,
    double inputLeftPadding = 0.0,
    double inputRightPadding = 0.0,
    double inputBorderRadius = 0.0,
    isEnabled = true,
    fillColor = Colors.white
  }) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:[
          Padding(
            padding: EdgeInsets.only(left: textLeftPadding, top: textTopPadding, bottom: textTopPadding),
            child: Row(
              children: [
                Text(
                  label,
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
                ),
                Visibility(
                    visible: isRequired ??= false,
                    child: Icon(Icons.star, color: Colors.red, size: 10,)
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: inputLeftPadding, right: inputRightPadding),
            child: TextFormField(
              enabled: isEnabled,
              obscureText: obscureText,
              key: key,
              controller: controller,
              decoration: InputDecoration(
                border: basicInputBorderStyle(context),
                filled: true,
                errorStyle: TextStyle(
                  color: Colors.red,
                ),
                fillColor: fillColor,
                hintText: hintText,
              ),
              validator: validator,
            ),
          ),
        ]);
  }

  //Creates basic select box input field for form
  static Widget buildSelectBox({
    @required String label,
    @required TextEditingController controller,
    @required bool isRequired,
    @required GlobalKey key,
    @required onChanged,
    @required initDropdownValue,
    @required List<String> items,
    bool obscureText = false,
    Function(String) validator,
    String hintText = "",
    double textLeftPadding = 0.0,
    double textTopPadding = 0.0,
    double textBottomPadding = 0.0,
    double inputLeftPadding = 0.0,
    double inputRightPadding = 0.0,
    double inputBorderRadius = 0.0,
    Color textColor,
  }) {
    return Row(children:[
      Padding(
        padding: EdgeInsets.only(left: textLeftPadding, top: textTopPadding, bottom: textBottomPadding),
        child: Text(
          label,
          style: TextStyle(color: textColor),
        ),
      ),
      Expanded(
        child: Padding(
          padding: EdgeInsets.only(left: 30.0, right: inputRightPadding),
          child: DropdownButtonFormField<String>(
            decoration: InputDecoration(
                enabledBorder: InputBorder.none
            ),
            value: initDropdownValue,
            icon: const Icon(Icons.expand_more, color: Colors.black,),
            iconSize: 24,
            elevation: 16,
            style: const TextStyle(color: Colors.black),
            onChanged: onChanged,
            validator: validator,
            key: key,
            items: items.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    ]);
  }

  ///Creates Date picker
  static Future<void> selectDate({
    @required  BuildContext context,
    @required onPickedDate,
    @required birthDateValue
  }) async {

    var a = await showDatePicker(
        context: context,
        initialDate: birthDateValue,
        firstDate: TZDateTime(currentTimezone, 1000, 1),
        lastDate: TZDateTime(currentTimezone, 2101)
    );
    if (a == null)
      return null;
    final TZDateTime picked = TZDateTime.parse(currentTimezone, a.toString());
    if (picked != null && picked != birthDateValue)
      onPickedDate(picked);
  }

  ///Creates basic belete button and call give mutation to API
  static Widget buildDeleteButton({
    @required int elementId,
    @required service,
    @required mutationType,
    @required void Function() refreshPage,
    @required Widget button
  }) {
    return Mutation(
        options: MutationOptions(
          document: gql(mutationType),
          onCompleted: (resultData) {
            service.onDeleteCompleted(
                resultData,
                onSuccess: () { //if the element is deleted on page, is necessary to refresh page to show changes to user
                  refreshPage();
                },
                onError: (context) {
                  BuildWidgets.showCustomSnackBar(context, error: true, message: "Odstránenie bolo neúspešné");
                });
          },
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return InkWell(
              onTap: () {
                service.onDeleteButtonTap(
                    runMutation,
                    id: elementId,
                    onError: (context) {
                      BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
                    });
              },
              child: Ink(
                  child: button
              )
          );
        }
    );
  }

  ///Creates general exception widget with general error text
  static Widget queryExceptionWidget(BuildContext context, {@required String exceptionMessage}) {
    return Center(
        child: Text(
          // exceptionMessage,    //only for testing purposes
          TextConstants.general_error_text,
          style: Theme.of(context).textTheme.bodyText1,
        )
    );

  }

  ///Creates general loading widget
  static Widget queryLoadingWidget(BuildContext context) {
    return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
        )
    );
  }

  ///Creates general fail widget witch general error text
  static Widget queryFailWidget(BuildContext context) {
    showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
    return Center(
      child: Text(
        TextConstants.general_error_text,
        style: Theme.of(context).textTheme.bodyText1
      )
    );
  }

  ///Creates general no data widget
  static Widget queryNoDataWidget(BuildContext context) {
    return Center(
      child: Text(
        TextConstants.general_no_data_text,
        style: Theme.of(context).textTheme.bodyText1
      )
    );
  }

  ///Creates general snack bar
  static showCustomSnackBar(context, {@required bool error, @required String message}) {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              action: SnackBarAction(
                label: 'OK',
                onPressed: () {  },
                textColor: Colors.white,
              ),
              backgroundColor: error ? Color(0xFFFC5451) : Color(0xFF69B04A),
              content: Text(message,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.white))
          ));
    });
  }

  ///Creates general floating button for adding new record in modules
  static Widget floatingButtonAddItem(BuildContext context, {@required onReturn, @required MaterialPageRoute nextRoute}) {
    return FloatingActionButton(
      backgroundColor: Theme.of(context).accentColor,
      onPressed: () {
        Navigator.push(
          context,
          nextRoute
        ).then((value) {
          onReturn();
        });
      },
      child: Icon(Icons.add, color: Colors.white,),
    );
  }

  ///Creates custom sliding up panel
  static Widget customSlidingUpPanel({@required Widget body, @required Widget panel, @required controller}) {
    return WillPopScope(
      onWillPop: () async {
        if (controller.isPanelOpen) {
          controller.close();
          return false;
        }
        return true;
      },
      child: SlidingUpPanel(
        controller: controller,
        backdropEnabled: true,
        renderPanelSheet: false,
        minHeight: 0,
        maxHeight: 500,
        panel: panel,
        body: body,
      ),
    );
  }

  /// Returns: single [Row] widget which contains [label] (text) aligned to the left and [child] Widget aligned to the right
  /// Usage: e.g. in create, view or edit pages in modules for additional information in detail view
  static Widget buildBasicHorizontalField(BuildContext context, {@required String label, Widget child}) {
    child ??= SizedBox.shrink();
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: Theme.of(context).textTheme.headline5,
        ),
        Spacer(),
        child
      ],
    );
  }

  /// Returns: single [Row] widget which contains [icon] and the following [text]
  /// Usage: e.g. in cards in modules like Tasks or Recipes for additional information like name or date
  static Widget buildLabelItem(BuildContext context, {IconData icon, String text, int maxLines, bool expand}) {
    maxLines ??= 1;
    expand ??= false;

    Widget resultText = Text(text,
      style: Theme.of(context).textTheme.bodyText1,
      overflow: TextOverflow.ellipsis,
      maxLines: maxLines,
    );

    if (expand) {
      resultText = Expanded(child: resultText);
    }

    Widget resultRow = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(icon,
          size: 16,
          color: Theme.of(context).primaryColorLight,
        ),
        SizedBox(
          width: 5,
        ),
        resultText,],
    );

    if (expand)
      resultRow = Expanded(child: resultRow,);

    return resultRow;
  }

  /// Returns: [Expanded] widget which represents search bar.
  /// Params: [onChanged] is function what is to happen with input after change
  /// Usage: e.g. in modules like Tasks or Recipes for searching in all task or recipe cards
  static Widget buildSearchBar(BuildContext context, {@required onChanged}) {
    return Expanded(
      child: Container(
        height: 36,
        child: TextField(
          onChanged: onChanged,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(8),
            suffixIcon: Icon(Icons.search, size: 20, color: Theme.of(context).primaryColor,),
            hintText: TextConstants.search_hint_text,
            hintStyle: Theme.of(context).textTheme.bodyText1,
            fillColor: Colors.white,
            filled: true,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.white)
            ),
          ),
        ),
      ),
    );
  }

  /// Returns: [ElevatedButton] widget represents basic button in app
  /// Params: [onPressed] is function what is to happen with input after press
  ///         [text] is label of the button
  /// Usage:  This is basic button used e.g. as save button in modules
  static Widget buildBasicButton(BuildContext context, {@required Function onPressed, @required String text}) {
    return ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
            primary: Theme.of(context).accentColor,
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))
            )
        ),
        child: Wrap(
          direction: Axis.vertical,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
              child: Text(
                text,
                style: Theme.of(context).textTheme.headline5.copyWith(color: Colors.white),
              ),
            ),
          ],
        )
    );
  }

  /// Returns: [OutlinedButton] widget represents outlined button in app
  /// Params: [onPressed] is function what is to happen with input after press
  ///         [text] is label of the button
  /// Usage:  e.g. for delete or remove buttons as contrast to BasicButton
  static Widget buildBasicOutlinedButton(BuildContext context, {@required Function onPressed, @required String text}) {
    return OutlinedButton(
        onPressed: onPressed,
        style: OutlinedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            side: BorderSide(
                color: Theme.of(context).primaryColor,
                width: 1
            ),
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))
            )
        ),
        child:  Wrap(
          direction: Axis.vertical,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
              child: Text(
                text,
                style: Theme.of(context).textTheme.headline5.copyWith(color: Theme.of(context).primaryColor),
              ),
            ),
          ],
        )
    );
  }

  ///Returns: [InkWell] which contains TextFormField represents basic date picker
  ///Usage: like date picker e.g. in the Tasks create or update page
  static Widget buildBasicDatePickerFormField(BuildContext context, {@required TextEditingController controller, @required onDatePicked, bool enabled, @required dateFormat}) {
    enabled ??= true;

    return InkWell(
      child: AbsorbPointer(
        child: TextFormField(
            enabled: enabled,
            controller: controller,
            decoration: InputDecoration(
                border: basicInputBorderStyle(context),
                filled: true,
                errorStyle: TextStyle(
                  color: Colors.red,
                ),
                fillColor: Colors.white,
                suffixIcon: Icon(
                  Icons.date_range,
                  color: Theme.of(context).primaryColor,
                )
            )
        ),
      ),
      onTap: () => !enabled ? null : showDatePicker(
          context: context,
          initialDate: controller == null ? TZDateTime.now(currentTimezone) : TZDateTime.from(dateFormat.parse(controller.text), currentTimezone),
          firstDate: TZDateTime(currentTimezone, 1000, 1),
          lastDate: TZDateTime(currentTimezone, 2101)
      ).then((picked) => picked == null ? null
            : onDatePicked(TZDateTime.from(picked, currentTimezone))),
    );
  }

  ///Returns: [InkWell] which contains TextFormField represents basic time picker
  ///Usage: like time picker e.g. in the Tasks create or update page
  static Widget buildBasicTimePickerFormField(BuildContext context, {@required controller, @required onTimePicked, bool enabled, @required timeFormat}) {
    enabled ??= true;

    return InkWell(
      child: AbsorbPointer(
        child: TextFormField(
            enabled: enabled,
            controller: controller,
            decoration: InputDecoration(
                border: basicInputBorderStyle(context),
                filled: true,
                errorStyle: TextStyle(
                  color: Colors.red,
                ),
                fillColor: Colors.white,
                suffixIcon: Icon(
                  Icons.timer,
                  color: Theme.of(context).primaryColor,
                )
            )
        ),
      ),
      onTap: () => !enabled ? null :  {
        showTimePicker(
            context: context,
            initialTime: controller == null ? TimeOfDay.now() : TimeOfDay.fromDateTime(timeFormat.parse(controller.text))
        ).then((picked) => picked == null ? null : onTimePicked(picked))
      },
    );
  }

  /// Returns:  Simple [Row] represents input row in forms.
  /// Params:   [label] is label of the input
  ///           [inputField] is input widget itself (common used is buildBasicTextFormField or
  ///             buildBasicDropdownFormField widget in this param)
  ///           [rightSideWidget] is addition widget connected with the input. Can be used e.g. for checkbox.
  /// Usage:    e.g. in simple forms like basic input field. RightSideWidget is used e.g. for checkbox,weather or
  ///             not is input enabled.
  static Widget buildInputField(BuildContext context, {String label, @required Widget inputField, Widget rightSideWidget, bool isRequired}) {
    rightSideWidget ??= SizedBox(width: 0.0,);
    isRequired ??= false;

    return Column(
      children: [
        Row(
          children: [
            Visibility(
              visible: label != null,
              child: Text(
                label != null ? label : "",
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
            Visibility(
                visible: isRequired ??= false,
                child: Icon(Icons.star, color: Colors.red, size: 10,)
            ),
            Spacer(),
            rightSideWidget
          ],
        ),
        SizedBox(
          height: 5,
        ),
        inputField
      ],
    );
  }

  /// Returns:  [DropdownButtonFormField] represents basic dropDown form field
  /// Usage:    e.g. in forms like basic dropdown input. For example in tasks create page for assignee field.
  static Widget buildBasicDropdownFormField(BuildContext context, {String hintText, @required List<DropdownMenuItem> items, GlobalKey key, Function validator, @required value, @required onChanged, bool enabled}) {
    enabled ??= true;
    hintText ??= "";

    return DropdownButtonFormField(
      value: value,
      onChanged: !enabled ? null : (dynamic newValue) {
        onChanged(newValue);
      },
      validator: validator,
      isExpanded: true,
      key: key,
      decoration: InputDecoration(
        border: basicInputBorderStyle(context),
        filled: true,
        errorStyle: TextStyle(
          color: Colors.red,
        ),
        fillColor: Colors.white,
        hintText: hintText,
      ),
      items: items,
    );
  }

  /// Returns:  [TextFormField] widget represents basic input field in forms
  /// Usage:    like simple input field in forms e.g. in Tasks create page for title or description
  static Widget buildBasicTextFormField(BuildContext context,
      {
        String hintText,
        int maxLines,
        @required TextEditingController controller,
        List<TextInputFormatter> inputFormatters,
        TextInputType keyboardType,
        bool enabled,
        Function validator,
        suffixIcon,
        GlobalKey key,
        bool enableSuggestions,
        bool autocorrect,
        bool obscureText
      })
  {
    enabled ??= true;
    enableSuggestions ??= true;
    autocorrect ??= true;
    obscureText ??= false;
    hintText ??= "";
    maxLines ??= 1;

    return TextFormField(
        enabled: enabled,
        maxLines: maxLines,
        controller: controller,
        validator: validator,
        inputFormatters: inputFormatters,
        keyboardType: keyboardType,
        key: key,
        enableSuggestions: enableSuggestions,
        autocorrect: autocorrect,
        obscureText: obscureText,
        decoration: InputDecoration(
          suffixIcon: suffixIcon,
          border: basicInputBorderStyle(context),
          filled: true,
          errorStyle: TextStyle(
            color: Colors.red,
          ),
          fillColor: Colors.white,
          hintText: hintText,
        )
    );
  }

  ///Creates basic card with title
  static Widget buildBasicTitledCard(BuildContext context,
    {
      @required String cardTitle,
      Widget widgetBeforeTitle,
      Widget widgetAfterTitle,
      Widget leftSidedVerticalWidget,
      rightTopWidget,
      firstRowWidget,
      secondRowWidget,
      double height,
      EdgeInsets bodyPadding

    })
  {
    height ??= 117;
    bodyPadding ??= EdgeInsets.fromLTRB(8, 17, 8, 17);
    widgetBeforeTitle ??= SizedBox();
    widgetAfterTitle ??= SizedBox();
    leftSidedVerticalWidget ??= SizedBox();
    rightTopWidget ??= SizedBox();
    firstRowWidget ??= SizedBox();
    secondRowWidget ??= SizedBox();
    //
    // if (cardTitle.length > 20)
    //   cardTitle = cardTitle.substring(0, 20) + "...";

    return Card(
      elevation: 0.5,
      shape:  RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        height: height,
        padding: bodyPadding,
        child: Row(
          children: [
            leftSidedVerticalWidget,
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              widgetBeforeTitle,
                              widgetBeforeTitle != SizedBox() ? SizedBox(width: 3,) : SizedBox(),
                              Expanded(
                                child: Text(
                                  cardTitle,
                                  maxLines: 2,
                                  style: Theme.of(context).textTheme.headline4,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              widgetAfterTitle == SizedBox() ? SizedBox(width: 3,) : SizedBox(),
                              widgetAfterTitle
                            ],
                          ),
                        ),
                        rightTopWidget
                      ],
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5,),
                          firstRowWidget,
                          SizedBox(height: 2,),
                          secondRowWidget
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///Creats titled field with one row
  static Widget buildSingleTitledField(BuildContext context, {
    @required title,
    @required body
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Theme.of(context).textTheme.headline3,
        ),
        SizedBox( height: 20,),
        Text(
          body,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }

  ///Creates general done button
  static buildDoneButton({@required Function onPressed}) {
    return IconButton(
        icon: Icon(
          Icons.check,
          color: Colors.white,
        ),
        onPressed: onPressed
    );
  }

  static basicInputBorderStyle(BuildContext context) => OutlineInputBorder(
    borderSide: BorderSide(
      color: Theme.of(context).primaryColor,
    ),
    borderRadius: new BorderRadius.circular(15.0),
  );
}