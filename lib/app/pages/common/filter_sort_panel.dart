import 'text_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';

///General filterSort panel
class FilterSortPanel<F, S> extends StatefulWidget {
  final dynamic Function({dynamic filter, List<dynamic> appliedFilters, dynamic appliedSort}) onFilterSwitch;
  var filtersDefinition;
  var sortsDefinition;
  S activeSort;
  var entityFilterEnumValues;
  var entitySortEnumValues;

  FilterSortPanel({@required Key key, @required this.onFilterSwitch, this.filtersDefinition, this.sortsDefinition, this.activeSort, this.entityFilterEnumValues, this.entitySortEnumValues}) : super(key: key);

  @override
  FilterSortPanelState createState() => FilterSortPanelState(this.filtersDefinition);
}

class FilterSortPanelState extends State<FilterSortPanel> {
  var _filtersDefinition;
  
  FilterSortPanelState(this._filtersDefinition);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(24.0)),
          boxShadow: [
            BoxShadow(
              blurRadius: 20.0,
              color: Colors.grey,
            ),
          ]
      ),
      margin: const EdgeInsets.all(24.0),
      child: Container(
        padding: EdgeInsets.fromLTRB(0, 10.0, 0, 10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
                visible: _filtersDefinition != null,
                child: Text(TextConstants.quick_filters_text, style: Theme.of(context).textTheme.headline3)
            ),
            Visibility(
              visible: _filtersDefinition != null,
              child: SizedBox(height: 10,)
            ),
            Visibility(
              visible: _filtersDefinition != null,
              child: Tags(
                itemCount: widget.entityFilterEnumValues == null ? 0 : widget.entityFilterEnumValues.length,
                itemBuilder: (int index) {
                  var entityItemsFilter = widget.entityFilterEnumValues == null ? null : widget.entityFilterEnumValues[index];
                  return ItemTags(
                    index: index,
                    title: entityItemsFilter == null ? "" : _filtersDefinition[entityItemsFilter]['label'],
                    active: _filtersDefinition == null ? false : _filtersDefinition[entityItemsFilter]['active'],
                    customData: entityItemsFilter,
                    textStyle: Theme.of(context).textTheme.bodyText1,
                    onPressed: (Item item) {
                      if (_filtersDefinition != null) {
                        _filtersDefinition[item.customData]['active'] = _filtersDefinition[item.customData]['active']
                            ? false
                            : true;
                        var appliedFilters = [];
                        _filtersDefinition.forEach((key, value) {
                          if (value['active'])
                            appliedFilters.add(key);
                        });

                        widget.onFilterSwitch(filter: item.customData, appliedFilters: appliedFilters);
                      }
                    },
                  );
                },
              ),
            ),
            Visibility(
                visible: _filtersDefinition != null,
                child: SizedBox(height: 10,)
            ),
            Text(TextConstants.sort_by_text, style: Theme.of(context).textTheme.headline3),
            Expanded(
              child: ListView.builder(
                  itemCount: widget.sortsDefinition.length,
                  itemBuilder: (context, index) {
                    var sortEnum = widget.entitySortEnumValues[index];
                    return _buildSortRow(active: widget.activeSort == sortEnum, sortEnum: sortEnum);
                  }
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildSortRow({@required active, @required sortEnum}) {
    return GestureDetector(
      child: Container(
          color: active ? Theme.of(context).accentColor : Colors.transparent,
          child: Padding(
            padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
            child: Text(
              widget.sortsDefinition[sortEnum]['label'],
              style: Theme.of(context).textTheme.bodyText1,
            ),
          )
      ),
      onTap: () {
        if (!active) {
          widget.activeSort = sortEnum;
          widget.onFilterSwitch(appliedSort: widget.activeSort);
          setState(() {});
        }
      },
    );
  }
}