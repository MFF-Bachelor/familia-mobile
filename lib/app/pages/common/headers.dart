
import 'text_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Home page header is designed to be used on home page. It contains two rows. In the first row there is one placeholder
/// aligned to the left and two placeholders aligned to the right. In the second line there is one TabBar.
class HomePageHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    throw UnimplementedError();
  }

}

/// Header template for pages with three placeholders in one row.
/// One placeholder is aligned to the left, the next one is aligned to the center and the last one is aligned to the right side.
class OneRowHeader extends StatelessWidget {
  String title;
  Widget rightSidedWidget;

  static final _iconWidth = 44.0;

  OneRowHeader({Key key, @required this.title, this.rightSidedWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 5),
      color: Theme.of(context).primaryColor,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: _iconWidth,
                child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios_outlined,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }
                ),
              ),
              Text(
                title,
                style: Theme.of(context).textTheme.headline2,
              ),
              SizedBox(
                width: _iconWidth,
                child: rightSidedWidget == null ? SizedBox() : rightSidedWidget,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

/// Header template for pages with three placeholders in the first row and one placeholder in the second row.
/// In the first line, one placeholder is aligned to the left, the next one is aligned to the center and
/// the last one is aligned to the right.
/// In the second line, there is only one placeholder aligned to the center (used e.g. for search)
class TwoRowsHeader extends StatelessWidget {
  String title;
  Widget rightSidedWidget; //first row right sided widget
  Widget secondRowWidget;

  static final _iconWidth = 44.0;

  TwoRowsHeader({Key key, @required this.title, this.rightSidedWidget, @required this.secondRowWidget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      padding: EdgeInsets.symmetric(horizontal: 5),
      color: Theme.of(context).primaryColor,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: _iconWidth,
                child: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios_outlined,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }
                ),
              ),
              Text(
                title,
                style: Theme.of(context).textTheme.headline2,
              ),
              SizedBox(
                width: _iconWidth,
                child: rightSidedWidget == null ? SizedBox() : rightSidedWidget,
              )
            ],
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: _iconWidth),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                secondRowWidget
              ],
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}