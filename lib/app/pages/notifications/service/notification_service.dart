import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class NotificationService {
  Future<void> onDeleteButtonTap(RunMutation runDeleteNotificationMutation, {@required id, @required onError,}) async {
    runDeleteNotificationMutation({
      "notificationId": id
    });
  }

  void onDeleteCompleted(dynamic resultData, {@required void Function() onSuccess, @required onError}) {
    var success = resultData[Mutations.DELETE_NOTIFICATION][Mutations.SUCCESS];
    if (success) {
      //apiAuthService.setCurrentUser(User.fromApiObject(resultData[Mutations.UPDATE_USER]['data']));
      onSuccess();
    } else {
      onError();
    }
  }
}