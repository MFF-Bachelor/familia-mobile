import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:timezone/timezone.dart';
import '../../common/text_constants.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/notifications/service/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:familia_mobile/app/entities/notification.dart' as UserNotif;
import 'package:graphql_flutter/graphql_flutter.dart';

class NotificationsMainSection extends StatefulWidget {

  NotificationsMainSection({Key key}) : super(key: key);

  @override
  _NotificationsMainSectionState createState() => _NotificationsMainSectionState();
}

class _NotificationsMainSectionState extends State<NotificationsMainSection> {
  List<UserNotif.Notification> notifications = [];
  VoidCallback myRefetch;

  NotificationService _notificationService = new NotificationService();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: Query(
          builder: (QueryResult result,
              { VoidCallback refetch, FetchMore fetchMore }) {
            myRefetch = refetch;
            if (result.hasException) {
              return BuildWidgets.queryFailWidget(context);
            }

            if (result.isLoading) {
              return BuildWidgets.queryLoadingWidget(context);
            }

            var success = result.data[Queries.GET_USER_NOTIFICATIONS][Queries.SUCCESS];
            if (success) {
              notifications = [];
              result.data[Queries.GET_USER_NOTIFICATIONS]['data']['notifications']?.forEach((notification) =>
              {
                notifications.add(UserNotif.Notification.fromApiObject(notification))
              });
              if (notifications.isEmpty) {
                return BuildWidgets.queryNoDataWidget(context);
              }
            } else {
              return BuildWidgets.queryFailWidget(context);
            }

            return SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(TextConstants.notifications_page_notifications_text,
                      style: Theme.of(context).textTheme.headline3.copyWith(color: Theme.of(context).primaryColor),),
                  ),
                  Column(
                    children: buildUserNotifications(notifications),
                  )
                ],
              ),
            );
          },
          options: QueryOptions(
            document: gql(Queries.getUserNotificationsQuery),
          ),
        ),
      ),
    );
  }

  Widget buildNotificationCard({String title, String text, TZDateTime dateTime, int notificationId}) {
    return BuildWidgets.buildBasicTitledCard(
      context,
      cardTitle: title,
      height: 155,
      bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
      rightTopWidget: BuildWidgets.buildDeleteButton(
          elementId: notificationId,
          service: _notificationService,
          mutationType: Mutations.deleteNotificationMutation,
          refreshPage: () {
            myRefetch();
            setState(() {});
          },
          button: Icon(Icons.highlight_remove, color: Theme.of(context).primaryColor,)
      ),
      firstRowWidget: BuildWidgets.buildLabelItem(context,
          icon: Icons.access_time,
          text: '${dateTime.day}.${dateTime.month}.${dateTime.year} ${dateTime.hour}:${dateTime.minute}',
          maxLines: 1,
          expand: false
      ),
      secondRowWidget: BuildWidgets.buildLabelItem(context,
          icon: Icons.notifications,
          text: text,
          maxLines: 2,
          expand: true
      )
    );
  }

  List<Widget> buildUserNotifications(List<UserNotif.Notification> notifications) {
    List<Widget> myNotifications = [];

    notifications?.sort((UserNotif.Notification n1, UserNotif.Notification n2) {
      return n2.created.compareTo(n1.created);
    });
    notifications?.forEach((notification) {
      var widget = Padding(
          padding:EdgeInsets.all(5.0),
          child: buildNotificationCard(
              title: notification.title.trim(),
              text: notification.text.trim(),
              dateTime: notification.created,
              notificationId: notification.id)
      );

      myNotifications.add(widget);
    });

    return myNotifications;
  }
}