import 'package:enum_to_string/enum_to_string.dart';
import '../../../services/graphql_service/graphql_helper.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/task.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/tasks/service/task_service.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/enums/task_state_enum.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart';

import '../widgets.dart';

class TasksDetailPage extends StatefulWidget {
  final TasksDetailPageTypeEnum pageType;
  Task task;

  TasksDetailPage({Key key, @required this.pageType, @required this.task}) : super(key: key);

  @override
  _TasksDetailPageState createState() => _TasksDetailPageState();
}

class _TasksDetailPageState extends State<TasksDetailPage> {
  var dateFormat = DateFormat('dd.MM.yyyy');
  var timeFormat = DateFormat('HH:mm');
  var dateTimeFormat = DateFormat('HH:mm dd.MM.yyyy');
  List<DropdownMenuItem> assignees;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TaskService _taskService = TaskService();
  GraphGLHelper _graphGLHelper = GraphGLHelper();
  bool updating = false;
  Task _task;

  TextEditingController _titleController;
  var _assignee;
  TextEditingController _textController;
  TextEditingController _dueDateController;
  TextEditingController _dueTimeController;
  int _priority;
  bool _canAssigneeEdit;

  @override
  void initState() {
    _task = widget.task;
    // relationships where userTo is not null (removes all relationships with inactive users)
    var relationshipsWithUserTo = List.from(apiAuthService.currentUser.confirmedRelationships);
    relationshipsWithUserTo.removeWhere((element) => element.userTo == null);

    assignees = relationshipsWithUserTo.map<DropdownMenuItem>((relationship) {
        return DropdownMenuItem(
            value: relationship.userTo,
            child: Text(relationship.userTo.firstName + " " + relationship.userTo.lastName)
        );
    }).toList();

    if (_task.assignee != null)
      assignees.insert(0, DropdownMenuItem(
          value: _task.assignee,
          child: Text(_task.assignee.firstName + " " + _task.assignee.lastName)));
    else
      assignees.insert(0, DropdownMenuItem(
          value: apiAuthService.currentUser,
          child: Text(apiAuthService.currentUser.firstName + " " + apiAuthService.currentUser.lastName)));

    _titleController = TextEditingController(text: _task.title == null ? '' : _task.title);
    _assignee = _task.assignee == null ? apiAuthService.currentUser : _task.assignee;
    _textController = TextEditingController(text: _task.text == null ? '' : _task.text);
    _dueDateController = _task.dueTime == null ? null : TextEditingController(text: dateFormat.format(_task.dueTime));
    _dueTimeController = _task.dueTime == null ? null : TextEditingController(text: timeFormat.format(_task.dueTime));
    _priority = _task.priority == null ? 1 : _task.priority;
    _canAssigneeEdit = _task.canAssigneeEdit == null ? true : _task.canAssigneeEdit;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery
        .of(context)
        .padding
        .top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Stack(
                  children: [
                    Column(
                      children: <Widget>[
                        OneRowHeader(
                          title: widget.pageType == TasksDetailPageTypeEnum.CREATE_PAGE
                              ? TextConstants.tasks_page_header_text
                              : TextConstants.tasks_page_header_text,
                          rightSidedWidget: (widget.pageType == TasksDetailPageTypeEnum.VIEW_PAGE)
                              ? (_task.creator.uid == apiAuthService.currentUser.uid ||
                                  _task.canAssigneeEdit ? _buildEditButton() : SizedBox())
                              : _buildSubmitButton()
                        ),
                        Expanded(
                            child: _buildTaskDetailPageBody()
                        ),
                      ],
                    ),
                    Visibility(
                        visible: updating,
                        child: Container(
                            color: Colors.black12,
                            child: BuildWidgets.queryLoadingWidget(context)
                        )
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }

  _buildTaskDetailPageBody() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox( height: 20,),
              //_buildFormBody(),
              (widget.pageType == TasksDetailPageTypeEnum.VIEW_PAGE)
                  ? _buildViewBody()
                  : _buildFormBody(),
              SizedBox( height: 20,)
            ],
          ),
        ),
      ),
    );
  }

  _buildViewBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BuildWidgets.buildSingleTitledField(
            context,
            title: "Názov",
            body: _task.title
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildSingleTitledField(
            context,
            title: "Popis",
            body: _task.text
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildBasicHorizontalField(
            context,
            label: TextConstants.task_creator,
            child: Text(
              _task.creator.firstName + " " + _task.creator.lastName,
              style: Theme.of(context).textTheme.bodyText1,
            )
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildBasicHorizontalField(
            context,
            label: TextConstants.task_state,
            child: Row(
                children: [
                  _task.state.icon,
                  SizedBox(width: 5.0,),
                  Text(_task.state.title, style: Theme.of(context).textTheme.bodyText1,)
                ]
            )
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildBasicHorizontalField(
            context,
            label: TextConstants.task_created,
            child: Row(
                children: [
                  SizedBox(width: 5.0,),
                  Text(
                    dateFormat.format(_task.created),
                    style: Theme.of(context).textTheme.bodyText1,
                  )
                ]
            )
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildBasicHorizontalField(
            context,
            label: TextConstants.task_assignee,
            child: Row(
                children: [
                  SizedBox(width: 5.0,),
                  Text(
                    _task.assignee.firstName + " " + _task.assignee.lastName,
                    style: Theme.of(context).textTheme.bodyText1,)
                ]
            )
        ),
        Visibility(
            visible: _task.creator.uid != apiAuthService.currentUser.uid || _assignee.uid != apiAuthService.currentUser.uid,
            child: SizedBox( height: 20,)
        ),
        Visibility(
          visible: _task.creator.uid != apiAuthService.currentUser.uid || _assignee.uid != apiAuthService.currentUser.uid,
          child: BuildWidgets.buildBasicHorizontalField(
              context,
              label: TextConstants.task_can_edit,
              child: Row(
                  children: [
                    SizedBox(width: 5.0,),
                    Text(
                      _task.canAssigneeEdit ? "áno" : "nie",
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ]
              )
          ),
        ),
        SizedBox( height: 20,),
        _task.dueTime == null ? SizedBox() : Column(
          children: [
            BuildWidgets.buildBasicHorizontalField(
                context,
                label: TextConstants.task_deadline_text,
                child: Row(
                    children: [
                      SizedBox(width: 5.0,),
                      Text(
                        dateTimeFormat.format(_task.dueTime),
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ]
                )
            ),
            SizedBox( height: 20,),
          ],
        ),
        BuildWidgets.buildInputField(
            context,
            label: TextConstants.task_priority,
            inputField: _buildPriorityField(value: _priority, enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE)
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: _task.state != TaskStateEnum.DONE,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildDoneTaskButton()
            ],
          ),
        )
      ],
    );
  }

  _buildFormBody() {
    return Wrap(
      spacing: 20.0,
      children: [
        SizedBox( height: 20,),
        Visibility(
          visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE,
          child:
          BuildWidgets.buildBasicHorizontalField(
              context,
              label: TextConstants.task_creator,
              child: Text(
                _task.creator != null ? _task.creator.firstName + " " + _task.creator.lastName : "",
                style: Theme.of(context).textTheme.bodyText1,
              )
          ),
        ),
        Visibility(visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE, child: SizedBox( height: 20,)),
        Visibility(
          visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE,
          child: BuildWidgets.buildBasicHorizontalField(
              context,
              label: TextConstants.task_state,
              child: Row(
                  children: [
                    _task.state != null ? _task.state.icon : SizedBox(),
                    SizedBox(width: 5.0,),
                    Text(_task.state != null ? _task.state.title : "", style: Theme.of(context).textTheme.bodyText1,)
                  ]
              )
          ),
        ),
        Visibility(visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE, child: SizedBox( height: 20,)),
        Visibility(
          visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE,
          child: BuildWidgets.buildBasicHorizontalField(
              context,
              label: TextConstants.task_created,
              child: Row(
                  children: [
                    SizedBox(width: 5.0,),
                    Text( _task.created != null ? _task.created.day.toString() + ". " +
                        _task.created.month.toString() + ". " +
                        _task.created.year.toString() : "",
                      style: Theme.of(context).textTheme.bodyText1,)
                  ]
              )
          ),
        ),
        Visibility(visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE, child: SizedBox( height: 20,)),
        BuildWidgets.buildInputField(
          context,
          label: TextConstants.task_title,
          inputField: BuildWidgets.buildBasicTextFormField(
              context,
              controller: _titleController,
              enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE,
              validator: (String value) {
                return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
              }
          ),
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildInputField(
            context,
            label: TextConstants.task_assignee,
            inputField: BuildWidgets.buildBasicDropdownFormField(
                context,
                items: assignees,
                value: _assignee,
                enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE,
                onChanged: (value) {
                  setState(() {
                    _assignee = value;
                    if (value == apiAuthService.currentUser)
                      _canAssigneeEdit = true;
                  });
                }
            ),
            rightSideWidget: Visibility(
              visible: _assignee.uid != apiAuthService.currentUser.uid,
              child: Row(
                children: [
                  Text(TextConstants.task_can_edit, style: Theme.of(context).textTheme.headline5),
                  Container(
                    width: 15,
                    height: 15,
                    margin: EdgeInsets.fromLTRB(8.0, 3.0, 3.0, 3.0),
                    child: Checkbox(
                        value: _canAssigneeEdit,
                        activeColor: Theme.of(context).primaryColor,
                        onChanged: (bool value) {
                          setState(() {
                            _canAssigneeEdit = value;
                          });
                        }
                    ),
                  ),
                ],
              ),
            )
        ),
        SizedBox( height: 20),
        BuildWidgets.buildInputField(
          context,
          label: TextConstants.task_text,
          inputField: BuildWidgets.buildBasicTextFormField(context, maxLines: 4, controller: _textController, enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE),
        ),
        SizedBox( height: 20,),
        Row( // Date and Time field
          children: [
            Expanded(
                child: BuildWidgets.buildInputField(
                  context,
                  label: TextConstants.task_due_date,
                  inputField: BuildWidgets.buildBasicDatePickerFormField(
                      context,
                      dateFormat: dateFormat,
                      enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE,
                      controller: _dueDateController,
                      onDatePicked: (TZDateTime picked) {

                        if (picked != null) {
                          _dueTimeController ??= TextEditingController(text: '23:59');
                          setState(() {
                            _dueDateController ??= TextEditingController(text: '');
                            _dueDateController.text = dateFormat.format(picked);
                          });
                        }
                      }
                  ),
                )),
            SizedBox(width: 45,),
            Expanded(
                child: BuildWidgets.buildInputField(
                    context,
                    label: TextConstants.task_due_time,
                    inputField: BuildWidgets.buildBasicTimePickerFormField(
                        context,
                        timeFormat: timeFormat,
                        enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE,
                        controller: _dueTimeController,
                        onTimePicked: (TimeOfDay picked) {
                          _dueDateController ??= TextEditingController(text: dateFormat.format(TZDateTime.now(currentTimezone)));
                          TZDateTime date = TZDateTime.from(dateFormat.parse(_dueDateController.text), currentTimezone);
                          setState(() {
                            _dueTimeController ??= TextEditingController(text: '');
                            _dueTimeController.text = timeFormat.format(new TZDateTime(currentTimezone, date.year, date.month, date.day, picked.hour, picked.minute));
                          });
                        })
                )),
          ],
        ),
        SizedBox( height: 20,),
        BuildWidgets.buildInputField(
            context,
            label: TextConstants.task_priority,
            inputField: _buildPriorityField(value: _priority, enabled: widget.pageType != TasksDetailPageTypeEnum.VIEW_PAGE)
        ),
        SizedBox( height: 50,),
        Row( //Delete and Save field
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: widget.pageType != TasksDetailPageTypeEnum.CREATE_PAGE,
              child: _buildRemoveButton()
            ),
            Visibility(
                visible: widget.pageType == TasksDetailPageTypeEnum.UPDATE_PAGE,
                child: SizedBox( width: 25,)
            ),
          ],
        ),
      ],
    );
  }

  _buildSubmitButton() {
    return Mutation (
        options: _mutationOptionsBasedOnPageType(),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildDoneButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    updating = true;
                  });
                  _runMutationBasedOnPageType(runMutation);
                }
              });
        });
  }

  _buildRemoveButton() {
    return Mutation(
        options: _graphGLHelper.entityMutationOptions(context,
            entityService: _taskService,
            mutationDefinition: Mutations.deleteTaskMutation,
            mutationName: Mutations.DELETE_TASK,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildBasicOutlinedButton(
              context,
              text: TextConstants.delete_text,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    updating = true;
                  });
                  _taskService.runDeleteTaskMutation(
                      runMutation,
                      id: _task.id
                  );
                }
              }
          );
        }
    );
  }

  MutationOptions _mutationOptionsBasedOnPageType() {
    switch (widget.pageType) {
      case TasksDetailPageTypeEnum.CREATE_PAGE:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _taskService,
            mutationDefinition: Mutations.createTaskMutation,
            mutationName: Mutations.CREATE_TASK,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      case TasksDetailPageTypeEnum.UPDATE_PAGE:
      case  TasksDetailPageTypeEnum.VIEW_PAGE: //In the view page, there is only _buildDoneTaskButton which calls setTaskMutation with Done state for the task.
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _taskService,
            mutationDefinition: Mutations.setTaskMutation,
            mutationName: Mutations.SET_TASK,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      default: return null;
    }
  }

  _runMutationBasedOnPageType(RunMutation runMutation) {
    var dueTime = null;
    if (_dueTimeController != null && _dueDateController != null)
      dueTime = TZDateTime.from(dateTimeFormat.parse(_dueTimeController.text + " " + _dueDateController.text), currentTimezone);

    switch (widget.pageType) {
      case TasksDetailPageTypeEnum.VIEW_PAGE:
        _taskService.runSetTaskMutation(
            runMutation,
            context: context,
            taskId: _task.id,
            priority: _priority,
            title: _titleController.text.trim(),
            canAssigneeEdit: _canAssigneeEdit,
            assignee: _assignee,
            text: _textController.text.trim(),
            dueTime: dueTime,
            state:  EnumToString.convertToString(TaskStateEnum.DONE)
        );
        break;
      case TasksDetailPageTypeEnum.CREATE_PAGE:
        _taskService.runCreateTaskMutation(
            runMutation,
            context: context,
            priority: _priority,
            title: _titleController.text.trim(),
            canAssigneeEdit: _canAssigneeEdit,
            assignee: _assignee,
            text: _textController.text.trim(),
            state:  EnumToString.convertToString(TaskStateEnum.ACTIVE),
            dueTime: dueTime
        );
        break;
      case TasksDetailPageTypeEnum.UPDATE_PAGE:
        _taskService.runSetTaskMutation(
            runMutation,
            context: context,
            taskId: _task.id,
            priority: _priority,
            title: _titleController.text.trim(),
            canAssigneeEdit: _canAssigneeEdit,
            assignee: _assignee,
            text: _textController.text.trim(),
            dueTime: dueTime,
            state:  EnumToString.convertToString(_task.state)
        );
        break;
      default: return null;
    }
  }

  _buildEditButton() {
    return IconButton(
        icon: Icon(
          Icons.edit,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TasksDetailPage(
                    pageType: TasksDetailPageTypeEnum.UPDATE_PAGE,
                    task: _task
                )
            ),
          ).then((value) => Navigator.pop(context, true));
        }
    );
  }

  _buildDoneTaskButton() {
    return Mutation (
        options: _mutationOptionsBasedOnPageType(),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildBasicOutlinedButton(
            context,
            text: TextConstants.task_complete_task,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                setState(() {
                  updating = true;
                });
                _runMutationBasedOnPageType(runMutation);
              }
            });

        });
  }

  _buildPriorityField({@required int value, bool enabled}) {
    enabled ??= true;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Visibility(
          visible: value == 1 || enabled,
          child: _buildPriorityWidget(activeColor: Color(0xFFFC5451), inactiveColor: Color(0x66FC5451), priority: 1)
        ),
        Visibility(
          visible: value == 2 || enabled,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: _buildPriorityWidget(activeColor: Color(0xFFFCD638), inactiveColor: Color(0x66FCD638), priority: 2),
          ),
        ),
        Visibility(
          visible: value == 3 || enabled,
          child: _buildPriorityWidget(activeColor: Color(0xFF69B04A), inactiveColor: Color(0x6669B04A), priority: 3)
        ),
      ],
    );
  }

  _buildPriorityWidget({@required int priority, @required Color activeColor, @required Color inactiveColor}) {
    return InkWell(
      onTap: () {
        _priority = priority;
        setState(() {

        });
      },
      child: Container(
        width: 70,
        height: 70,
        decoration: BoxDecoration(
            color: _priority == priority ? activeColor : inactiveColor,
            borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Center(child: Text(priority.toString(), style: Theme.of(context).textTheme.headline3.copyWith(color: Colors.white, fontSize: 36),)),
      ),
    );
  }
}

enum TasksDetailPageTypeEnum {
  CREATE_PAGE,
  UPDATE_PAGE,
  VIEW_PAGE
}