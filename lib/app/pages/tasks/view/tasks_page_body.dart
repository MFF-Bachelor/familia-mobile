import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/task.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/task_state_enum.dart';
import 'package:familia_mobile/app/enums/tasks_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/tasks/service/task_service.dart';
import 'package:familia_mobile/app/pages/tasks/view/tasks_detail_page.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../common/text_constants.dart';
import '../widgets.dart';

class TasksPageBody extends StatefulWidget {
  TasksPageBody({Key key}) : super(key: key);

  @override
  TasksPageBodyState createState() => TasksPageBodyState();
}

class TasksPageBodyState extends State<TasksPageBody> {
  List<Task> tasks = [];
  List<Task> otherCreatedTasks = [];
  List<Task> myTasks = [];
  List<Widget> tabs = [];
  VoidCallback myRefetch;
  List<dynamic> appliedFilters;
  TasksSortEnum appliedSort = TasksSortEnum.CREATED_DATE_DESC;
  String searchString = '';

  final TaskService _taskService = TaskService();

  @override
  Widget build(BuildContext context) {
    tabs = [
      Tab(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Text(TextConstants.my_tasks)
        )
      ),
      Tab(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Text(TextConstants.other_created_tasks)
        )
      ),
    ];

    return Scaffold(
      body: DefaultTabController(
        length: tabs.length,
        child: Builder(builder: (BuildContext context) {
          return Scaffold(
            appBar: _buildTabs(),
            body: Query(
              builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
                myRefetch = refetch;
                CustomQueryResult queryResult = _taskService.checkQueryResult(result, refetch: refetch, fetchMore: fetchMore);
                switch (queryResult) {
                  case CustomQueryResult.EXCEPTION:
                    return BuildWidgets.queryExceptionWidget(context, exceptionMessage: result.exception.toString());
                    break;
                  case CustomQueryResult.LOADING:
                    return BuildWidgets.queryLoadingWidget(context);
                    break;
                  case CustomQueryResult.FAIL:
                    return BuildWidgets.queryFailWidget(context);
                    break;
                  case CustomQueryResult.SUCCESS:
                    _parseQueryResult(result);
                    return _buildBody();
                    break;
                }
                return BuildWidgets.queryFailWidget(context);
              },
              options: QueryOptions(
                document: gql(Queries.getUserTasksQuery),
              ),
            )
          );
        }),
      ),
      floatingActionButton: BuildWidgets.floatingButtonAddItem(
          context,
          nextRoute: MaterialPageRoute(builder: (context) => TasksDetailPage(
              pageType: TasksDetailPageTypeEnum.CREATE_PAGE,
              task: Task()
          )),
          onReturn: () {
            myRefetch();
            setState(() {});
          }
      ),
    );
  }

  _buildTabs() {
    return PreferredSize(
      preferredSize: Size.fromHeight(80.0),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
        child: TabBar(
            isScrollable: true,
            indicatorColor: Colors.transparent,
            labelColor: Theme.of(context).primaryColor,
            labelStyle: Theme.of(context).textTheme.headline3,
            unselectedLabelStyle: Theme.of(context).textTheme.headline3.copyWith(color: Colors.deepPurple),
            tabs: tabs
        ),
      ),
    );
  }

  ///Parse data (assignedTasks and otherCreatedTasks) from query result, apply filters and sort and save
  ///the data into local variables myTasks and otherCreatedTasks
  _parseQueryResult(QueryResult result) {
    myTasks = [];
    result.data[Queries.GET_USER_TASKS]['data']['assignedTasks']?.forEach((taskObject) {
    Task task = Task.fromApiObject(taskObject);
      if (task.creator.uid == task.assignee.uid || task.state != TaskStateEnum.DONE || task.canAssigneeEdit)
        myTasks.add(task);
    });

    otherCreatedTasks = [];
    result.data[Queries.GET_USER_TASKS]['data']['otherCreatedTasks']?.forEach((task) => {
      otherCreatedTasks.add(Task.fromApiObject(task))
    });

    apiAuthService.currentUser.myTasks = myTasks;

    myTasks = _taskService.filterVisibleTasks(tasks: myTasks, appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: searchString);
    otherCreatedTasks = _taskService.filterVisibleTasks(tasks: otherCreatedTasks, appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: searchString);
  }

  _buildBody() {
    return TabBarView(
        children: [
          myTasks.isEmpty ? BuildWidgets.queryNoDataWidget(context) :_buildTasksList(myTasks),
          otherCreatedTasks.isEmpty ? BuildWidgets.queryNoDataWidget(context) :_buildTasksList(otherCreatedTasks),
        ]
    );
  }

  ListView _buildTasksList(List<Task> tasks) {
    return ListView.builder(
      itemCount: tasks.length,
      itemBuilder: (context, index) {
        return Widgets.buildMyTaskCard(
          context,
          task: tasks[index],
          onCardPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => TasksDetailPage(
                  pageType: TasksDetailPageTypeEnum.VIEW_PAGE,
                  task: tasks[index]
              ))
            ).then((needRefetch) {
              myRefetch();
              setState(() {});
            });
            ;
            }
          );
      }
    );
  }

  void applyFilters({List<dynamic> appliedFilters, dynamic appliedSort, String searchString}) {
    if (appliedFilters != null)
      this.appliedFilters = appliedFilters;
    if (appliedSort != null)
      this.appliedSort = appliedSort;
    if (searchString != null)
      this.searchString = searchString;
    setState(() {});
  }
}