import '../../common/text_constants.dart';
import 'package:familia_mobile/app/enums/tasks_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/filter_sort_panel.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/tasks/widgets.dart';
import 'package:familia_mobile/app/pages/tasks/view/tasks_page_body.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class TasksPage extends StatefulWidget {
  TasksPage({Key key, this.title}) : super(key: key);

  final String title;


  @override
  _TasksPageState createState() => _TasksPageState();
}

class _TasksPageState extends State<TasksPage> {
  PanelController _panelController = new PanelController();
  final GlobalKey<FilterSortPanelState> _tasksFilterPanelKey = GlobalKey();
  final GlobalKey<TasksPageBodyState> _tasksPageBodyKey = GlobalKey();

  List<dynamic> _appliedFilters = [];
  var _appliedSort;
  String _searchString;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;
    return BuildWidgets.customSlidingUpPanel(
      controller: _panelController,
      panel: FilterSortPanel<TasksFilterEnum, TasksSortEnum>(
        key: _tasksFilterPanelKey,
        activeSort: TasksSortEnum.CREATED_DATE_DESC,
        entityFilterEnumValues: TasksFilterEnum.values,
        entitySortEnumValues: TasksSortEnum.values,
        filtersDefinition: TasksFilterEnumExtension.definition,
        sortsDefinition: TasksSortEnumExtension.definition,
        onFilterSwitch: ({dynamic filter, List<dynamic> appliedFilters, dynamic appliedSort}) {
          if (filter != null) {
            TasksFilterEnumExtension.definition[filter]['active'] = !TasksFilterEnumExtension.definition[filter]['active'];
          }
          _appliedFilters = appliedFilters;
          _appliedSort = appliedSort;
          _tasksPageBodyKey.currentState.applyFilters(appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: _searchString);
        },
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
            body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Column(
                  children: <Widget>[
                  TwoRowsHeader(
                    title: TextConstants.tasks_page_header_text,
                    rightSidedWidget: IconButton(
                      icon: Icon(Icons.sort),
                      onPressed: () {
                        _panelController.open();
                      },
                      color: Colors.white,
                    ),
                    secondRowWidget: BuildWidgets.buildSearchBar(context,
                      onChanged: (String newValue) {
                        _searchString = newValue;
                        _tasksPageBodyKey.currentState.applyFilters(appliedFilters: _appliedFilters, appliedSort: _appliedSort, searchString: newValue);
                    }),
                  ),
                  Expanded(
                      child: TasksPageBody(key: _tasksPageBodyKey,)
                  ),
                  ],
                ),
              )
            )
          ),
      ),
    );
  }
}