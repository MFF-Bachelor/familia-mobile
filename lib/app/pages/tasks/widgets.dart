import 'package:familia_mobile/app/entities/task.dart';
import 'package:familia_mobile/app/enums/task_state_enum.dart';
import '../common/build_widgets.dart';
import 'package:flutter/material.dart';

class Widgets {
  static Widget buildMyTaskCard(BuildContext context, {
    @required Task task,
    @required Function onCardPressed
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
      child: InkWell(
        child: BuildWidgets.buildBasicTitledCard(
          context,
          cardTitle: task.title,
          height: 115,
          bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
          leftSidedVerticalWidget: _buildPriorityLine(task.priority),
          rightTopWidget: task.state.icon,
          firstRowWidget: BuildWidgets.buildLabelItem(context,
              icon: Icons.person,
              text: task.creator.firstName + " " + task.creator.lastName
          ),
          secondRowWidget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              task.dueTime == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
                icon: Icons.timer,
                text: task.dueTime.day.toString() + ". " +
                    task.dueTime.month.toString() + ". " +
                    task.dueTime.year.toString(),
              ),
              task.created == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
                icon: Icons.access_time,
                text: task.created.day.toString() + ". " +
                    task.created.month.toString() + ". " +
                    task.created.year.toString(),
              )
            ],
          ),
        ),
        onTap: () => onCardPressed(),
      ),
    );
  }

  static final priorities = {
    1: Color(0xFFFC5451),
    2: Color(0xFFFCD638),
    3: Color(0xFF69B04A)
  };

  static _buildPriorityLine(int priority) {
    return Container(
      width: 10,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: priorities[priority]
      ),
      height: double.maxFinite,
    );
  }

  static buildDoneButton({@required Function onPressed}) {
    return IconButton(
        icon: Icon(
          Icons.check,
          color: Colors.white,
        ),
        onPressed: onPressed
    );
  }
}