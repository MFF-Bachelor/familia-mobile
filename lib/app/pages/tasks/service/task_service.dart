import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/task.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/task_state_enum.dart';
import 'package:familia_mobile/app/enums/tasks_filter_enum.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:timezone/timezone.dart';

class TaskService {

  ///Returns type of query result
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException)
      return CustomQueryResult.EXCEPTION;

    if (result.isLoading)
      return CustomQueryResult.LOADING;

    var success = result.data[Queries.GET_USER_TASKS][Queries.SUCCESS];
    if (success) {
      sessionService.saveCookieFromApiResponse(result);
      return CustomQueryResult.SUCCESS;
    }
    else
      return CustomQueryResult.FAIL;
  }

  Future<void> runCreateTaskMutation(
      RunMutation runCreateTaskMutation,
      {
        @required context,
        @required String title,
        String text,
        TZDateTime dueTime,
        @required int priority,
        @required User assignee,
        @required  String state,
        @required bool canAssigneeEdit,
      }) async {
        text ??= '';
        dueTime ??= null;
        runCreateTaskMutation({
            "inputData": {
              "title": title,
              "text": text,
              "dueTime": dueTime == null ? null : dueTime.toString(),
              "assigneeUids": [assignee.uid],
              "priority": priority,
              "state": state,
              "canAssigneeEdit": canAssigneeEdit
            }
          });
  }

  Future<void> runSetTaskMutation(
      RunMutation runSetTaskMutation,
      {
        @required context,
        @required taskId,
        @required String title,
        String text,
        TZDateTime dueTime,
        @required int priority,
        @required User assignee,
        @required String state,
        @required bool canAssigneeEdit,
      }) async {
    text ??= '';
    dueTime ??= null;
    runSetTaskMutation({
      "inputData": {
        "taskId": taskId,
        "title": title,
        "text": text,
        "dueTime": dueTime == null ? null : dueTime.toString(),
        "assigneeUid": assignee.uid,
        "priority": priority,
        "state": state,
        "canAssigneeEdit": canAssigneeEdit
      }
    });
  }

  Future<void> runDeleteTaskMutation(RunMutation runDeleteTaskMutation, {@required id}) async {
    runDeleteTaskMutation({
      "taskId": id
    });
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    // if (result.hasException) {
    //   return CustomMutationResult.EXCEPTION;
    // }
    //
    // if (result.isLoading) {
    //   return CustomMutationResult.LOADING;
    // }

    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success)
        return CustomMutationResult.SUCCESS;
      else
        return CustomMutationResult.FAIL;
    }
  }

  ///Returns filtered and sorted tasks based on given params
  List<Task> filterVisibleTasks({ @required List<Task> tasks, @required appliedFilters, @required appliedSort, @required String searchString}) {
    tasks.removeWhere((task) {

      bool toRemoveWithFilter = false;
      if ((appliedFilters != null && appliedFilters.length != 0)) {
        toRemoveWithFilter = true;
        appliedFilters?.forEach((filter) {
          switch (filter) {
            case TasksFilterEnum.DUE_TODAY:
              if (_isSameDate(task.dueTime, TZDateTime.now(currentTimezone))) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case TasksFilterEnum.DUE_TOMORROW:
              if (_isSameDate(task.dueTime, TZDateTime.now(currentTimezone).add(Duration(days: 1)))) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case TasksFilterEnum.HIGHEST_PRIORITY:
              if (task.priority == 1) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case TasksFilterEnum.LOWEST_PRIORITY:
              if (task.priority == 3) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case TasksFilterEnum.ACTIVE_TASKS:
              if (task.state == TaskStateEnum.ACTIVE) {
                toRemoveWithFilter = false;
                return;
              }
              break;
          }
        });
      }

      bool toRemoveWithSearch = false;
      if (searchString != null && searchString != '') {
        String lowerCaseSearchString = searchString.toLowerCase();
        toRemoveWithSearch = true;
        if (task.title.toLowerCase().contains(lowerCaseSearchString)) {
          toRemoveWithSearch = false;
        }
      }

      return toRemoveWithFilter || toRemoveWithSearch;
    });

    tasks.sort((Task task1, Task task2) {
      switch(appliedSort) {
        case TasksSortEnum.PRIORITY_ASC:
          return task2.priority.compareTo(task1.priority);
          break;
        case TasksSortEnum.PRIORITY_DESC:
          return task1.priority.compareTo(task2.priority);
          break;
        case TasksSortEnum.NAME:
          return task1.title.toLowerCase().compareTo(task2.title.toLowerCase());
          break;
        case TasksSortEnum.DUE_DATE:
          if (task1.dueTime == null)
            return 1;
          if (task2.dueTime == null)
            return -1;
          return task1.dueTime.compareTo(task2.dueTime);
          break;
        case TasksSortEnum.CREATED_DATE_ASC:
          return task1.created.compareTo(task2.created);
          break;
        case TasksSortEnum.CREATED_DATE_DESC:
          return task2.created.compareTo(task1.created);
          break;
        default:
          return 0;
      }
    });

    return tasks;
  }

  bool _isSameDate(TZDateTime date1, TZDateTime date2) {
    if (date1 == null || date2 == null)
      return false;
    date1 = date1.toLocal();
    date2 = date2.toLocal();
    return date1.year == date2.year && date1.month == date2.month && date1.day == date2.day;
  }
}