import 'package:community_material_icon/community_material_icon.dart';
import 'package:familia_mobile/app/entities/shopping_list.dart';
import 'package:familia_mobile/app/entities/shopping_list_item.dart';
import '../common/build_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Widgets {
  static Widget buildMyShoppingListCard(BuildContext context, {
    @required ShoppingList shoppingList,
    @required Function onCardPressed
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
      child: InkWell(
        child: BuildWidgets.buildBasicTitledCard(
          context,
          cardTitle: shoppingList.title,
          bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
          leftSidedVerticalWidget: SizedBox(width: 0),
          rightTopWidget: shoppingList.isDone
              ? Icon(Icons.check, color: Color(0xFF69B04A),)
              : Text(shoppingList.progressText, style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 18),),
          firstRowWidget: shoppingList.creator == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
              icon: Icons.person,
              text: shoppingList.creator.firstName + " " + shoppingList.creator.lastName
          ),
          secondRowWidget: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              shoppingList.dueTime == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
                icon: Icons.timer,
                text: shoppingList.dueTime.day.toString() + ". " +
                    shoppingList.dueTime.month.toString() + ". " +
                    shoppingList.dueTime.year.toString(),
              ),
              shoppingList.created == null ? SizedBox() : BuildWidgets.buildLabelItem(context,
                icon: Icons.access_time,
                text: shoppingList.created.day.toString() + ". " +
                    shoppingList.created.month.toString() + ". " +
                    shoppingList.created.year.toString(),
              )
            ],
          ),
        ),
        onTap: () => onCardPressed(),
      ),
    );
  }

  static buildShoppingListItemsInput(
      BuildContext context,
      {
        @required ShoppingListItems,
        @required shoppingListTitleControllers,
        @required Function onRemoveTap,
        @required Function onAddTap,
      }) {
    ShoppingListItems ??= [];
    List<Widget> result = [];

    var index = 0;
    ShoppingListItems.forEach((shoppingListItem) {
      result.add(
          Row(
            children: [
              Expanded(
                flex: 60,
                child: BuildWidgets.buildInputField(
                  context,
                  inputField: BuildWidgets.buildBasicTextFormField(
                    context,
                    enabled: true,
                    controller: shoppingListTitleControllers[index],
                  ),
                ),
              ),
              SizedBox(width: 10),
              InkWell(
                  child: Icon(
                    Icons.remove_circle_outline,
                    size: 25,
                    color: Theme.of(context).primaryColor,
                  ),
                  onTap: () => onRemoveTap(shoppingListItem)
              ),
            ],
          )
      );
      index++;
    });

    result.addAll([
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
                child: Icon(
                  Icons.add_circle_outline,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
                onTap: () => onAddTap()
            ),
          ],
        )
    ]);

    return Column(
      children: result,
    );
  }

  static buildShoppingListItemsList(
      BuildContext context,
      {
        @required List<ShoppingListItem> shoppingListItems,
        @required shoppingListTitleControllers,
        @required Function onCheckedChange,
        @required shoppingListItemsCheckedValues
      }) {
    shoppingListItems ??= [];
    List<Widget> result = [];

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Theme.of(context).primaryColor;
      }
      return Theme.of(context).primaryColor;
    }

    shoppingListItems.forEach((shoppingListItem) {
      int index = shoppingListItems.indexOf(shoppingListItem);
      result.add(
        InkWell(
          onTap: () => onCheckedChange(index, !shoppingListItemsCheckedValues[index]),
          child: Row(
            children: [
              Expanded(
                flex: 10,
                  child: Transform.scale(
                    scale: 1.5,
                    child: AbsorbPointer(
                      child: Checkbox(
                        checkColor: Colors.white,
                        activeColor: Theme.of(context).primaryColor,
                        value: shoppingListItemsCheckedValues[index],
                        onChanged: (value) => {}
                      ),
                    ),
                  )
              ),
              Expanded(
                flex: 60,
                child: Text(
                    shoppingListTitleControllers[index].text.trim(),
                    style: Theme.of(context).textTheme.bodyText1,
                )
              ),
            ],
          ),
        )
      );
    });

    return Column(
      children: result,
    );
  }
}