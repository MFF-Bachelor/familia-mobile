import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/shopping_list.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/shopping_list_filter_enum.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';


class ShoppingListService {
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return CustomQueryResult.EXCEPTION;
    }

    if (result.isLoading) {
      return CustomQueryResult.LOADING;
    }

    var success = result.data[Queries.GET_USER_SHOPPING_LISTS][Queries.SUCCESS];
    if (success) {
      return CustomQueryResult.SUCCESS;
    } else {
      return CustomQueryResult.FAIL;
    }
  }

  Future<void>  runCreateShoppingListMutation(
      RunMutation runCreateShoppingListMutation,
      {
        @required context,
        @required title,
        @required description,
        @required dueDate,
        @required assignee,
        @required shoppingListItems
      }) async {
      runCreateShoppingListMutation({
        "inputData": {
          "title": title.text.trim(),
          "description": description.text.trim(),
          "dueTime":  dueDate == null ? null : dueDate.toString(),
          "assigneeUid": assignee == null ? null : assignee.uid,
          "shoppingListItems": shoppingListItems
        }
      });
  }

  Future<void>  runSetShoppingListMutation(
      RunMutation runSetShoppingListMutation,
      {
        @required context,
        @required shoppingListId,
        @required title,
        @required description,
        @required dueDate,
        @required assignee,
        @required shoppingListItems
      }) async {
    runSetShoppingListMutation({
      "inputData": {
        "shoppingListId": shoppingListId,
        "title": title.text,
        "description": description.text,
        "dueTime":  dueDate == null ? null : dueDate.toString(),
        "assigneeUid": assignee == null ? null : assignee.uid,
        "shoppingListItems": shoppingListItems
      }
    });
  }

  Future<void> onDeleteButtonTap(
      RunMutation runDeleteShoppingListMutation,
      {
        @required id,
        @required onError,
      }) async {
    runDeleteShoppingListMutation({
      "shoppingListId": id
    });
  }

  Future<void>  runSetShoppingListItemCheckedValueMutation(
      RunMutation runSetShoppingListItemCheckedValueMutation,
      {
        @required context,
        @required shoppingListItemId,
        @required shoppingListItemCheckedValue
      }) async {
    runSetShoppingListItemCheckedValueMutation({

        "shoppingListItemId": shoppingListItemId,
        "shoppingListItemCheckedValue": shoppingListItemCheckedValue

    });
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success) {
        return CustomMutationResult.SUCCESS;
      } else {
        return CustomMutationResult.FAIL;
      }
    }
  }

  ///Returns filtered and sorted shopping lists based on given params
  List<ShoppingList> filterVisibleShoppingLists({ @required List<ShoppingList> shoppingLists, @required appliedFilters, @required appliedSort, @required String searchString}) {
    shoppingLists.removeWhere((shoppingList) {

      bool toRemoveWithFilter = false;
      if ((appliedFilters != null && appliedFilters.length != 0)) {
        toRemoveWithFilter = true;
        appliedFilters?.forEach((filter) {
          switch (filter) {
            case ShoppingListFilterEnum.DUE_TODAY:
              if (_isSameDate(shoppingList.dueTime, TZDateTime.now(currentTimezone))) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case ShoppingListFilterEnum.DUE_TOMORROW:
              if (_isSameDate(shoppingList.dueTime, TZDateTime.now(currentTimezone).add(Duration(days: 1)))) {
                toRemoveWithFilter = false;
                return;
              }
              break;
            case ShoppingListFilterEnum.MY:
              if (shoppingList.creator.uid == apiAuthService.currentUser.uid)
                toRemoveWithFilter = false;
              break;
            case ShoppingListFilterEnum.SHARED_WITH_ME:
              if (shoppingList.creator.uid != apiAuthService.currentUser.uid)
                toRemoveWithFilter = false;
              break;
            case ShoppingListFilterEnum.UNFINISHED:
              if (!shoppingList.isDone)
                toRemoveWithFilter = false;
              break;
          }
        });
      }

      bool toRemoveWithSearch = false;
      if (searchString != null && searchString != '') {
        String lowerCaseSearchString = searchString.toLowerCase();
        toRemoveWithSearch = true;
        if (shoppingList.title.toLowerCase().contains(lowerCaseSearchString)) {
          toRemoveWithSearch = false;
        }
      }

      return toRemoveWithFilter || toRemoveWithSearch;
    });

    shoppingLists.sort((ShoppingList shoppingList1, ShoppingList shoppingList2) {
      switch(appliedSort) {
        case ShoppingListSortEnum.TITLE:
          return shoppingList1.title.toLowerCase().compareTo(shoppingList2.title.toLowerCase());
          break;
        case ShoppingListSortEnum.DUE_DATE:
          if (shoppingList1.dueTime == null)
            return 1;
          if (shoppingList2.dueTime == null)
            return -1;
          return shoppingList1.dueTime.compareTo(shoppingList2.dueTime);
          break;
        case ShoppingListSortEnum.CREATED_DATE_ASC:
          return shoppingList1.created.compareTo(shoppingList2.created);
          break;
        case ShoppingListSortEnum.CREATED_DATE_DESC:
          return shoppingList2.created.compareTo(shoppingList1.created);
          break;
        default:
          return 0;
      }
    });

    return shoppingLists;
  }

  bool _isSameDate(TZDateTime date1, TZDateTime date2) {
    if (date1 == null || date2 == null)
      return false;
    date1 = date1.toLocal();
    date2 = date2.toLocal();
    return date1.year == date2.year && date1.month == date2.month && date1.day == date2.day;
  }
}