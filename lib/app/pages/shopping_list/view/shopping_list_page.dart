import '../../common/text_constants.dart';
import 'package:familia_mobile/app/enums/shopping_list_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/filter_sort_panel.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/shopping_list/view/shopping_list_page_body.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class ShoppingListPage extends StatefulWidget {
  ShoppingListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ShoppingListPageState createState() => _ShoppingListPageState();
}

class _ShoppingListPageState extends State<ShoppingListPage> {
  PanelController _panelController = new PanelController();
  final GlobalKey<FilterSortPanelState> _filterSortPanelKey = GlobalKey();
  final GlobalKey<ShoppingListPageBodyState> _shoppingListPageBodyKey = GlobalKey();

  List<dynamic> _appliedFilters = [];
  var _appliedSort;
  String _searchString;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return BuildWidgets.customSlidingUpPanel(
      controller: _panelController,
      panel: FilterSortPanel<ShoppingListFilterEnum, ShoppingListSortEnum>(
        key: _filterSortPanelKey,
        activeSort: ShoppingListSortEnum.CREATED_DATE_DESC,
        entityFilterEnumValues: ShoppingListFilterEnum.values,
        entitySortEnumValues: ShoppingListSortEnum.values,
        filtersDefinition: ShoppingListFilterEnumExtension.definition,
        sortsDefinition: ShoppingListSortEnumExtension.definition,
        onFilterSwitch: ({dynamic filter, List<dynamic> appliedFilters, dynamic appliedSort}) {
          if (filter != null) {
            ShoppingListFilterEnumExtension.definition[filter]['active'] = !ShoppingListFilterEnumExtension.definition[filter]['active'];
          }
          _appliedFilters = appliedFilters;
          _appliedSort = appliedSort;
          _shoppingListPageBodyKey.currentState.applyFilters(appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: _searchString);
        },
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
            body: Center(
                child: Container(
                  padding: EdgeInsets.only(top: _statusBarHeight),
                  child: Column(
                    children: <Widget>[
                      TwoRowsHeader(
                        title: TextConstants.shopping_list_page_header_text,
                        rightSidedWidget: IconButton(
                          icon: Icon(Icons.sort),
                          onPressed: () {
                            _panelController.open();
                          },
                          color: Colors.white,
                        ),
                        secondRowWidget: BuildWidgets.buildSearchBar(context,
                            onChanged: (String newValue) {
                              _searchString = newValue;
                              _shoppingListPageBodyKey.currentState.applyFilters(appliedFilters: _appliedFilters, appliedSort: _appliedSort, searchString: newValue);
                            }),
                      ),
                      Expanded(
                          child: ShoppingListPageBody(key: _shoppingListPageBodyKey,)
                      ),
                    ],
                  ),
                )
            )
        ),
      ),
    );
  }
}