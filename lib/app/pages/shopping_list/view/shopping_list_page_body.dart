import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/shopping_list.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/shopping_list_filter_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/shopping_list/service/shopping_list_service.dart';
import 'package:familia_mobile/app/pages/shopping_list/view/shopping_list_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../widgets.dart';

class ShoppingListPageBody extends StatefulWidget {
  ShoppingListPageBody({Key key}) : super(key: key);

  @override
  ShoppingListPageBodyState createState() => ShoppingListPageBodyState();
}

class ShoppingListPageBodyState extends State<ShoppingListPageBody> {
  List<ShoppingList> myShoppingLists = [];
  VoidCallback myRefetch;
  List<dynamic> appliedFilters;
  ShoppingListSortEnum appliedSort = ShoppingListSortEnum.CREATED_DATE_DESC;
  String searchString = '';

  final ShoppingListService _shoppingListService = ShoppingListService();

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        return  Query(
              builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
                myRefetch = refetch;
                CustomQueryResult queryResult = _shoppingListService.checkQueryResult(result, refetch: refetch, fetchMore: fetchMore);
                switch (queryResult) {
                  case CustomQueryResult.EXCEPTION:
                    return BuildWidgets.queryExceptionWidget(context, exceptionMessage: result.exception.toString());
                    break;
                  case CustomQueryResult.LOADING:
                    return BuildWidgets.queryLoadingWidget(context);
                    break;
                  case CustomQueryResult.FAIL:
                    return BuildWidgets.queryFailWidget(context);
                    break;
                  case CustomQueryResult.SUCCESS:
                    _parseQueryResult(result);
                    if (myShoppingLists.isEmpty) {
                      return BuildWidgets.queryNoDataWidget(context);
                    }
                    return _buildShoppingListsList(myShoppingLists);
                    break;
                }
                return BuildWidgets.queryFailWidget(context);
              },
              options: QueryOptions(
                document: gql(Queries.getUserShoppingListsQuery),
              ),
            );
      }),
      floatingActionButton: BuildWidgets.floatingButtonAddItem(
          context,
          nextRoute: MaterialPageRoute(builder: (context) => ShoppingListDetailPage(
              pageType: ShoppingListDetailPageTypeEnum.CREATE_PAGE,
              shoppingList: ShoppingList()
          )),
          onReturn: () {
            myRefetch();
            setState(() {});
          }
      ),
    );
  }

  ///Parse data (assignedTasks and otherCreatedTasks) from query result, apply filters and sort and save
  ///the data into local variables myTasks and otherCreatedTasks
  _parseQueryResult(QueryResult result) {
    myShoppingLists = [];
    result.data[Queries.GET_USER_SHOPPING_LISTS]['data']['createdShoppingLists']?.forEach((shoppingList) {
      var shoppingListObject = ShoppingList.fromApiObject(shoppingList);
      myShoppingLists.add(shoppingListObject);
    });

    result.data[Queries.GET_USER_SHOPPING_LISTS]['data']['assigneeShoppingLists']?.forEach((shoppingList) {
      var shoppingListObject = ShoppingList.fromApiObject(shoppingList);
      myShoppingLists.add(shoppingListObject);
    });

    myShoppingLists = _shoppingListService.filterVisibleShoppingLists(shoppingLists: myShoppingLists, appliedFilters: appliedFilters, appliedSort: appliedSort, searchString: searchString);
  }

  ListView _buildShoppingListsList(List<ShoppingList> shoppingLists) {
    return ListView.builder(
        itemCount: shoppingLists.length,
        itemBuilder: (context, index) {
          return Widgets.buildMyShoppingListCard(
              context,
              shoppingList: shoppingLists[index],
              onCardPressed: () {

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ShoppingListDetailPage(
                            shoppingList: shoppingLists[index],
                            pageType: ShoppingListDetailPageTypeEnum.VIEW_PAGE
                        )
                    )
                ).then((needRefetch) {
                  myRefetch();
                  setState(() {});
                });
              }
          );
        }
    );
  }

  void applyFilters({List<dynamic> appliedFilters, dynamic appliedSort, String searchString}) {
    if (appliedFilters != null)
      this.appliedFilters = appliedFilters;
    if (appliedSort != null)
      this.appliedSort = appliedSort;
    if (searchString != null)
      this.searchString = searchString;
    setState(() {});
  }
}