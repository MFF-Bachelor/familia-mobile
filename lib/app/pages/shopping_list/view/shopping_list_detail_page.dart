import 'package:community_material_icon/community_material_icon.dart';
import '../../../services/graphql_service/graphql_helper.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/shopping_list.dart';
import 'package:familia_mobile/app/entities/shopping_list_item.dart';
import 'package:familia_mobile/app/entities/user.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/shopping_list/service/shopping_list_service.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart';

import '../widgets.dart';

class ShoppingListDetailPage extends StatefulWidget {
 final ShoppingListDetailPageTypeEnum pageType;
 ShoppingList shoppingList;

 ShoppingListDetailPage({Key key, @required this.pageType, @required this.shoppingList}) : super(key: key);

 @override
 _ShoppingListDetailPageState createState() => _ShoppingListDetailPageState();
}

class _ShoppingListDetailPageState extends State<ShoppingListDetailPage> {
 ShoppingList _shoppingList;
 Map<String, dynamic> data = {};
 var dateFormat = DateFormat('dd.MM.yyyy');
 var timeFormat = DateFormat('HH:mm');
 var dateTimeFormat = DateFormat('HH:mm dd.MM.yyyy');
 List<DropdownMenuItem> assigneeDropdownItems = [];
 final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 ShoppingListService _shoppingListService = ShoppingListService();
 GraphGLHelper _graphGLHelper = GraphGLHelper();
 TZDateTime _dueDateValue = TZDateTime.now(currentTimezone);


 //Data controllers
 TextEditingController _titleController = TextEditingController();
 TextEditingController _descriptionController = TextEditingController();
 TextEditingController _dueDateController = TextEditingController();
 User _assignee;
 List<ShoppingListItem> _shoppingListItems = [];
 List<TextEditingController> _shoppingListItemControllers = [];
 List<bool> _shoppingListItemCheckedValues = [];

 bool isChecked = false;

 @override
 void initState() {
  _shoppingList = widget.shoppingList;
  // relationships where userTo is not null (removes all relationships with inactive users)
  var relationshipsWithUserTo = List.from(apiAuthService.currentUser.confirmedRelationships);
  relationshipsWithUserTo.removeWhere((element) => element.userTo == null);

  assigneeDropdownItems = relationshipsWithUserTo.map<DropdownMenuItem>((relationship) {
   return DropdownMenuItem(
       value: relationship.userTo,
       child: Text(relationship.userTo.firstName + " " + relationship.userTo.lastName)
   );
  }).toList();
  assigneeDropdownItems.add(
      DropdownMenuItem(
          value: null,
          child: Text("Nezdieľať")
      )
  );

  _initFormValues();

  super.initState();
 }

 _initFormValues() {
   switch (widget.pageType) {
     case ShoppingListDetailPageTypeEnum.CREATE_PAGE:
       _titleController = TextEditingController(text:'');
       _descriptionController = TextEditingController(text: '');
       _dueDateController = TextEditingController(text: '');
       _shoppingListItems = [];
       break;
     case ShoppingListDetailPageTypeEnum.UPDATE_PAGE:
       _titleController = TextEditingController(text: _shoppingList.title);
       _descriptionController = TextEditingController(text: _shoppingList.description);
       _dueDateController = TextEditingController(text: _shoppingList.dueTime == null ? null : dateFormat.format(_shoppingList.dueTime));
       _shoppingListItems = _shoppingList.shoppingListItems;
       _assignee =  _shoppingList.assignee == null ? null : assigneeDropdownItems.firstWhere(
          (DropdownMenuItem element) => element.value != null && element.value.uid == _shoppingList.assignee.uid,
          orElse: () => null
       )?.value as User;

       _shoppingList.shoppingListItems?.forEach((shoppingListItem) {
         _shoppingListItemControllers.add(TextEditingController(text: shoppingListItem.title == null ? '' : shoppingListItem.title.toString()));
         _shoppingListItemCheckedValues.add(shoppingListItem.checked != null ? shoppingListItem.checked : false);
       });
       break;
     case ShoppingListDetailPageTypeEnum.VIEW_PAGE:
       _titleController = TextEditingController(text: _shoppingList.title);
       _descriptionController = TextEditingController(text: _shoppingList.description);
       _dueDateController = TextEditingController(text: _shoppingList.dueTime == null ? null : dateFormat.format(_shoppingList.dueTime));
       _shoppingListItems = _shoppingList.shoppingListItems;

       _shoppingList.shoppingListItems?.forEach((shoppingListItem) {
         _shoppingListItemControllers.add(TextEditingController(text: shoppingListItem.title == null ? '' : shoppingListItem.title.toString()));
         _shoppingListItemCheckedValues.add(shoppingListItem.checked != null ? shoppingListItem.checked : false);
       });

       break;
   }
 }

 @override
 Widget build(BuildContext context) {
   double _statusBarHeight = MediaQuery
       .of(context)
       .padding
       .top;

   return GestureDetector(
     onTap: () => FocusScope.of(context).unfocus(),
     child: Scaffold(
         body: Center(
             child: Container(
               padding: EdgeInsets.only(top: _statusBarHeight),
               child: Column(
                 children: <Widget>[
                   OneRowHeader(
                     title:
                        widget.pageType == ShoppingListDetailPageTypeEnum.VIEW_PAGE
                          ? TextConstants.shopping_list_detail_page_view_shopping_list_title
                          : TextConstants.shopping_list_detail_page_new_shopping_list_title,
                     rightSidedWidget:
                       (widget.pageType == ShoppingListDetailPageTypeEnum.VIEW_PAGE)
                          ? _buildEditButton()
                          : _buildSubmitButton(),
                   ),
                   Expanded(
                       child: _buildShoppingListDetailPageBody()
                   ),
                 ],
               ),
             )
         )
     ),
   );
 }

 _buildShoppingListDetailPageBody() {
   return SingleChildScrollView(
     child: Padding(
       padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
       child: Form(
         key: _formKey,
         child: Column(
           children: [
             SizedBox( height: 20,),
             //_buildFormBody(),
             (widget.pageType == ShoppingListDetailPageTypeEnum.VIEW_PAGE)
                 ? _buildViewBody()
                 : _buildFormBody(),
             SizedBox( height: 20,)
           ],
         ),
       ),
     ),
   );
 }

 _buildFormBody() {
   return Wrap(
     spacing: 20.0,
     children: [
       Visibility(
         visible: true,
         child: BuildWidgets.buildInputField(
           context,
           label: TextConstants.shopping_list_title_label,
           inputField: BuildWidgets.buildBasicTextFormField(
               context,
               controller: _titleController,
               enabled: true,
               validator: (String value) {
                 return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
               }
           ),
         ),
       ),
       SizedBox( height: 20,),
       Visibility(
         visible: true,
         child: BuildWidgets.buildInputField(
           context,
           label: TextConstants.shopping_list_description_label,
           inputField: BuildWidgets.buildBasicTextFormField(
               context,
               controller: _descriptionController,
               enabled: true,
               maxLines: 5,
               validator: (String value) {
                 return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
               }
           ),
         ),
       ),
       SizedBox( height: 20,),
       Visibility(
         visible: true,
         child: InkWell(
           child: AbsorbPointer(
             child:
             BuildWidgets.buildInputField(
               context,
               label: TextConstants.shopping_list_due_date_label,
               inputField: BuildWidgets.buildBasicTextFormField(
                 context,
                 controller: _dueDateController,
                 enabled: true,
               ),
             ),
           ),
           onTap: () => BuildWidgets.selectDate(
               birthDateValue: _dueDateValue,
               context: context,
               onPickedDate:(picked) {
                 setState(() {
                   _dueDateValue = picked;
                   _dueDateController.text = dateFormat.format(_dueDateValue);
                 });
               }
           ),
         ),
       ),
       SizedBox( height: 20,),
       Row(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: [
           Text(
             "Zoznam",
             style: Theme.of(context).textTheme.headline5,
           ),
         ],
       ),
       Widgets.buildShoppingListItemsInput(
         context,
         ShoppingListItems: _shoppingListItems,
         shoppingListTitleControllers: _shoppingListItemControllers,
         onAddTap: () {
           setState(() {
             _shoppingListItems.add(ShoppingListItem());
             _shoppingListItemControllers.add(TextEditingController());
             _shoppingListItemCheckedValues.add(false);
           });
           //scrollController.jumpTo(scrollController.position.maxScrollExtent + 70);
         },
         onRemoveTap: (ShoppingListItem shoppingListItem) {
           setState(() {
             var index = _shoppingListItems.indexOf(shoppingListItem);
             _shoppingListItems.removeAt(index);
             _shoppingListItemControllers.removeAt(index);
           });
         },
       ),
       SizedBox( height: 20,),
       BuildWidgets.buildInputField(
           context,
           label: TextConstants.shopping_list_assignees_label,
           inputField: BuildWidgets.buildBasicDropdownFormField(
               context,
               items: assigneeDropdownItems,
               value: _assignee,
               enabled: true,
               onChanged: (value) {
                 setState(() {
                   _assignee = value;
                 });
               }
           ),
           rightSideWidget: SizedBox()
       ),
       SizedBox( height: 20,),
       Visibility(
           visible: widget.pageType == ShoppingListDetailPageTypeEnum.UPDATE_PAGE,
           child: Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               _buildRemoveShoppingListButton(_shoppingList)
             ],
           )
       )
     ],
   );
 }

 _buildViewBody() {
   return Column(
     crossAxisAlignment: CrossAxisAlignment.start,
     children: [
       Text(
         _shoppingList.title,
         style: Theme.of(context).textTheme.headline3,
       ),
       SizedBox( height: 20,),
       Row(
         crossAxisAlignment: CrossAxisAlignment.center,
         children: [
           Icon(
             Icons.access_time,
             color: Theme.of(context).primaryColorLight,
           ),
           SizedBox( width: 5,),
           Text(
             dateFormat.format(_shoppingList.created),
             style: Theme.of(context).textTheme.bodyText1,
           ),
           SizedBox( width: 20,),
           Icon(
             Icons.edit,
             color: Theme.of(context).primaryColorLight,
           ),
           SizedBox( width: 5,),
           Text(
             dateFormat.format(_shoppingList.modified),
             style: Theme.of(context).textTheme.bodyText1,
           ),
         ],
       ),
       SizedBox( height: 10,),
       Visibility(
         visible: _shoppingList.assignee == null ? false : true,
         child: Row(
           crossAxisAlignment: CrossAxisAlignment.center,
           children: [
             Text(
               "Zdieľané s: ",
               style: Theme.of(context).textTheme.bodyText1,
             ),
             SizedBox( width: 5,),
             Text(
               _shoppingList.assignee != null ? _shoppingList.assignee.firstName + " " + _shoppingList.assignee.lastName : " ",
               style: Theme.of(context).textTheme.bodyText1,
             ),
           ],
         ),
       ),
       SizedBox( height: 10,),
       _shoppingList.dueTime == null ? SizedBox() : Row(
         crossAxisAlignment: CrossAxisAlignment.center,
         children: [
           Text(
             "Nakúpiť do: ",
             style: Theme.of(context).textTheme.bodyText1,
           ),
           SizedBox( width: 5,),
           Text(
             dateFormat.format(_shoppingList.dueTime),
             style: Theme.of(context).textTheme.bodyText1,
           ),
         ],
       ),
       SizedBox( height: 20,),
       BuildWidgets.buildSingleTitledField(
           context,
           title: "Popis",
           body: _shoppingList.description,
       ),
       SizedBox( height: 20,),
       Mutation(
           options: _graphGLHelper.entityMutationOptions(
               context,
               entityService: _shoppingListService,
               mutationDefinition: Mutations.setShoppingListItemCheckedValue,
               mutationName: Mutations.SET_SHOPPING_LIST_ITEM_CHECKED_VALUE,
               onSuccess: (_) { },
               onError: (_) {
                 BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
               }
           ),
           builder: (RunMutation runMutation, QueryResult result) {
             return Widgets.buildShoppingListItemsList(
               context,
               shoppingListItems: _shoppingListItems,
               shoppingListTitleControllers: _shoppingListItemControllers,
               shoppingListItemsCheckedValues: _shoppingListItemCheckedValues,
               onCheckedChange: (int index, bool value) {
                 setState(() {
                   if (_formKey.currentState.validate()) {
                     _shoppingListService.runSetShoppingListItemCheckedValueMutation(
                       runMutation,
                       shoppingListItemId: _shoppingListItems[index].shoppingListItemId,
                       shoppingListItemCheckedValue: value,
                       context: context,
                     );
                   }

                   _shoppingListItemCheckedValues[index] = value;
                 });
                 //scrollController.jumpTo(scrollController.position.maxScrollExtent + 70);
               },
             );
           }
       ),
     ],
   );
 }

 _buildPriorityWidget({@required int priority, @required Color activeColor, @required Color inactiveColor}) {
  return InkWell(
   onTap: () {
    data['priority'] = priority;
    setState(() {

    });
   },
   child: Container(
    width: 70,
    height: 70,
    decoration: BoxDecoration(
        color: data['priority'] == priority ? activeColor : inactiveColor,
        borderRadius: BorderRadius.all(Radius.circular(20))
    ),
    child: Center(child: Text(priority.toString(), style: Theme.of(context).textTheme.headline3.copyWith(color: Colors.white, fontSize: 36),)),
   ),
  );
 }

 _buildSubmitButton() {
   return Mutation (
       options: _mutationOptionsBasedOnPageType(),
       builder: (RunMutation runMutation, QueryResult result) {
         return BuildWidgets.buildDoneButton(
             onPressed: () {
               if (_formKey.currentState.validate()) {
                 _runMutationBasedOnPageType(runMutation);
               }
             });
       });
 }

 _buildEditButton() {
   return IconButton(
       icon: Icon(
         Icons.edit,
         color: Colors.white,
       ),
       onPressed: () {
         Navigator.push(
           context,
           MaterialPageRoute(
               builder: (context) => ShoppingListDetailPage(
                   pageType: ShoppingListDetailPageTypeEnum.UPDATE_PAGE,
                   shoppingList: _shoppingList
               )
           ),
         ).then((value) => Navigator.pop(context, true));
       }
   );
 }

 _buildRemoveShoppingListButton(ShoppingList shoppingList) {
   return Mutation(
       options: _graphGLHelper.entityMutationOptions(
           context,
           entityService: _shoppingListService,
           mutationDefinition: Mutations.deleteShoppingListMutation,
           mutationName: Mutations.DELETE_SHOPPING_LIST,
           onError: (_) {
             BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
           },
           onSuccess: (result) {
             Navigator.of(context).pop();
             BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
           }),
       builder: (RunMutation runMutation, QueryResult result) {
         return BuildWidgets.buildBasicOutlinedButton(
             context,
             text: TextConstants.delete_shopping_list_button_text,
             onPressed: () {
               if (_formKey.currentState.validate()) {
                 _shoppingListService.onDeleteButtonTap(
                     runMutation,
                     id: shoppingList.shoppingListId,
                     onError: () {
                       throw Exception("Custom error: on delete failed");
                     }
                 );
               }
             }
         );
       }
   );
 }

 _mutationOptionsBasedOnPageType() {
   switch (widget.pageType) {
     case ShoppingListDetailPageTypeEnum.CREATE_PAGE:
       return _graphGLHelper.entityMutationOptions(
           context,
           entityService: _shoppingListService,
           mutationDefinition: Mutations.createShoppingListMutation,
           mutationName: Mutations.CREATE_SHOPPING_LIST,
           onError: (_) {
             BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
           },
           onSuccess: (result) {
             Navigator.of(context).pop();
             BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
           }
       );
       break;
     case ShoppingListDetailPageTypeEnum.UPDATE_PAGE:
       return _graphGLHelper.entityMutationOptions(
           context,
           entityService: _shoppingListService,
           mutationDefinition: Mutations.setShoppingListMutation,
           mutationName: Mutations.SET_SHOPPING_LIST,
           onError: (_) {
             BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
           },
           onSuccess: (result) {
             Navigator.of(context).pop();
             BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
           }
       );
       break;
     default: return null;
   }
 }

 _runMutationBasedOnPageType(RunMutation runMutation) {
   var dueDate = null;
   if (_dueDateController.text != null && _dueDateController.text.isNotEmpty)
     dueDate = TZDateTime.from(dateFormat.parse(_dueDateController.text), currentTimezone);

   _summariseShoppingListItems();

   switch (widget.pageType) {
     case ShoppingListDetailPageTypeEnum.CREATE_PAGE:
       _shoppingListService.runCreateShoppingListMutation(
         runMutation,
         title: _titleController,
         description: _descriptionController,
         dueDate: dueDate,
         assignee: _assignee,
         shoppingListItems: _convertObjectsToJsons(_shoppingListItems),
         context: context,
       );
       break;
     case ShoppingListDetailPageTypeEnum.UPDATE_PAGE:
       _shoppingListService.runSetShoppingListMutation(
         runMutation,
         shoppingListId: _shoppingList.shoppingListId,
         title: _titleController,
         description: _descriptionController,
         dueDate: dueDate,
         assignee: _assignee,
         shoppingListItems: _convertObjectsToJsons(_shoppingListItems),
         context: context
       );
       break;
     default: return null;
   }
 }

 _convertObjectsToJsons(arrayOfObjects) {
   var result = [];
   arrayOfObjects?.forEach((object) {
     result.add(object.toJson());
   });
   return result;
 }

 _summariseShoppingListItems() {
   for (int i = 0; i < _shoppingListItemControllers.length; i++) {
     _shoppingListItems[i].title = _shoppingListItemControllers[i].text.trim();
     _shoppingListItems[i].checked = _shoppingListItemCheckedValues[i];
   }
 }
}

enum ShoppingListDetailPageTypeEnum {
 CREATE_PAGE,
 UPDATE_PAGE,
 VIEW_PAGE
}