import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/abstract_user.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/relationship_role_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/family_tree/service/family_tree_service.dart';
import 'package:familia_mobile/app/pages/my_family/view/my_family_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphview/GraphView.dart';

class FamilyTreePageBody extends StatefulWidget {
  final userUid;
  FamilyTreePageBody({Key key, @required this.userUid}) : super(key: key);

  @override
  FamilyTreePageBodyState createState() => FamilyTreePageBodyState();
}

class FamilyTreePageBodyState extends State<FamilyTreePageBody> {
  VoidCallback myRefetch;

  final FamilyTreeService _familyTreeService = FamilyTreeService();
  TransformationController transformationController = TransformationController();

  @override
  void initState() {
    super.initState();
  }

  SugiyamaConfiguration builder = SugiyamaConfiguration()
    ..levelSeparation = 100
    ..nodeSeparation = 100
  ;

  @override
  Widget build(BuildContext context) {
    return Query(
      builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
        myRefetch = refetch;
        CustomQueryResult queryResult = _familyTreeService.checkQueryResult(result, refetch: refetch, fetchMore: fetchMore);
        switch (queryResult) {
          case CustomQueryResult.EXCEPTION:
            return BuildWidgets.queryExceptionWidget(context, exceptionMessage: result.exception.toString());
            break;
          case CustomQueryResult.LOADING:
            return BuildWidgets.queryLoadingWidget(context);
            break;
          case CustomQueryResult.FAIL:
            return BuildWidgets.queryFailWidget(context);
            break;
          case CustomQueryResult.SUCCESS:
            return _buildBody(result);
            break;
        }
        return BuildWidgets.queryFailWidget(context);
      },
      options: QueryOptions(
        variables: {
          'uid': widget.userUid
        },
        document: gql(Queries.getUserFamilyTreeData),
      ),
    );
  }

  Paint edgePaint({Color color}) {
    color ??= Theme.of(context).primaryColor;
    return Paint()
      ..color = color
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke;
  }

  Widget nodeWidget({String firstLine, String secondLine, Function onTap, Color backgroundColor, Color borderColor, Color firstLineTextColor, Color secondLineTextColor}) {
    firstLine ??= '?';
    backgroundColor ??= Colors.white;
    borderColor ??= Theme.of(context).primaryColor;
    var firstLineStyle = firstLineTextColor == null ? Theme.of(context).textTheme.headline4 : Theme.of(context).textTheme.headline4.copyWith(color: firstLineTextColor);
    var secondLineStyle = secondLineTextColor == null ? Theme.of(context).textTheme.bodyText1 : Theme.of(context).textTheme.bodyText1.copyWith(color: secondLineTextColor);

    return InkWell(
      onTap: onTap,
      child: Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 2, color: borderColor),
          ),
          child: Column(
            children: [
              Text(firstLine, style: firstLineStyle,),
              secondLine == null ? SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Text(secondLine, style: secondLineStyle,),
                    ),
            ],
          )
    ));
  }

  Widget _buildBody(QueryResult result) {
    User user = User.fromApiObject(result.data[Queries.GET_USER_FAMILY_TREE_DATA]['data']);
    return Stack(
      children: [
        InteractiveViewer(
            transformationController: transformationController,
            constrained: false,
            boundaryMargin: EdgeInsets.all(50),
            scaleEnabled: false,
            child: GraphView(
              graph: _familyTreeService.createGraph(user),
              animated: true,
              algorithm: SugiyamaAlgorithm(builder),
              paint: edgePaint(),
              builder: (Node node) {
                var value = node.key.value;
                if (value is Relationship){
                  return nodeWidget(
                      firstLine: '${value.anyUserTo.firstName} ${value.anyUserTo.lastName}',
                      secondLine: '${value.role.titleByGender(value.anyUserTo.gender)}',
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => MyFamilyDetailPage(
                            relationship: value,
                            showEditButton: false,
                            pageType: value.userTo != null
                                ? MyFamilyDetailPageTypeEnum.VIEW_TWO_WAY_RELATIONSHIP
                                : MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP
                        )));
                      }
                  );
                }
                if (value is AbstractUser) {    //this is the central user node
                  return nodeWidget(
                    firstLine: '${value.firstName} ${value.lastName}',
                    backgroundColor: Theme.of(context).accentColor,
                    borderColor: Theme.of(context).accentColor,
                    firstLineTextColor: Colors.white
                  );
                }
                if (value is String)
                  return nodeWidget(secondLine: value);
                return nodeWidget();
              },
            )),
        _buildScaleControlButtons()
          ],
    );
  }

  Widget _buildScaleControlButtons() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            BuildWidgets.buildBasicButton(context, text: '-', onPressed: () {
              transformationController.value = Matrix4.copy(transformationController.value)..scale(0.625);
            }),
            SizedBox(width: 10,),
            BuildWidgets.buildBasicButton(context, text: '+', onPressed: () {
              transformationController.value = Matrix4.copy(transformationController.value)..scale(1.6);
            }),
          ],
        ),
      ),
    );
  }

}