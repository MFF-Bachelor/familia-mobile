import '../../common/text_constants.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/family_tree/view/family_tree_page_body.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class FamilyTreePage extends StatefulWidget {
  final userUid;
  FamilyTreePage({Key key, this.title, @required this.userUid}) : super(key: key);

  final String title;

  @override
  _FamilyTreePageState createState() => _FamilyTreePageState();
}

class _FamilyTreePageState extends State<FamilyTreePage> {
  PanelController _panelController = new PanelController();
  final GlobalKey<FamilyTreePageBodyState> _familyTreePageBodyKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Column(
                  children: <Widget>[
                    OneRowHeader(
                      title: TextConstants.family_tree_title,
                      rightSidedWidget: SizedBox()
                    ),
                    Expanded(
                        child: FamilyTreePageBody(key: _familyTreePageBodyKey, userUid: widget.userUid,)
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }
}