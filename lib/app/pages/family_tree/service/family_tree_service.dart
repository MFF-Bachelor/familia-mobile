import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/abstract_user.dart';
import 'package:familia_mobile/app/entities/inactive_user.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:familia_mobile/app/enums/relationship_role_enum.dart';
import 'package:familia_mobile/app/enums/relationship_side_enum.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphview/GraphView.dart';

class FamilyTreeService {

  ///Returns type of query result
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException)
      return CustomQueryResult.EXCEPTION;

    if (result.isLoading)
      return CustomQueryResult.LOADING;

    var success = result.data[Queries.GET_USER_FAMILY_TREE_DATA][Queries.SUCCESS];
    if (success) {
      sessionService.saveCookieFromApiResponse(result);
      return CustomQueryResult.SUCCESS;
    }
    else
      return CustomQueryResult.FAIL;
  }

  ///creates family tree graph for given user
  Graph createGraph(User user) {
    final Graph graph = Graph();

    var userRelationships = user.confirmedRelationships;

    final Node me = Node.Id(user);

    Node fatherNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.PARENT, gender: GenderEnum.MALE);
    Node motherNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.PARENT, gender: GenderEnum.FEMALE);
    Node grandfatherFatherNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.GRAND_PARENT, gender: GenderEnum.MALE, side: RelationshipSideEnum.PATERNAL);
    Node grandmotherFatherNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.GRAND_PARENT, gender: GenderEnum.FEMALE, side: RelationshipSideEnum.PATERNAL);
    Node grandfatherMotherNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.GRAND_PARENT, gender: GenderEnum.MALE, side: RelationshipSideEnum.MATERNAL);
    Node grandmotherMotherNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.GRAND_PARENT, gender: GenderEnum.FEMALE, side: RelationshipSideEnum.MATERNAL);
    List<Node> children = _getFamilyMemberNodes(userRelationships: userRelationships, role: RelationshipRoleEnum.CHILD);
    List<Node> siblings = _getFamilyMemberNodes(userRelationships: userRelationships, role: RelationshipRoleEnum.SIBLING);
    Node spouseNode = _getFamilyMemberSingleNode(userRelationships: userRelationships, role: RelationshipRoleEnum.PARTNER);
    List<Node> grandChildren = _getFamilyMemberNodes(userRelationships: userRelationships, role: RelationshipRoleEnum.GRAND_CHILD);

    graph.addEdge(motherNode, me);
    graph.addEdge(fatherNode, me);
    graph.addEdge(grandfatherFatherNode, fatherNode);
    graph.addEdge(grandmotherFatherNode, fatherNode);
    graph.addEdge(grandfatherMotherNode, motherNode);
    graph.addEdge(grandmotherMotherNode, motherNode);
    children.forEach((child) {
      if (child.key.value is Relationship) {
        graph.addEdge(me, child);
      }
    });
    siblings.forEach((sibling) {
      if (sibling.key.value is Relationship) {
        graph.addEdge(motherNode, sibling, paint: Paint()..color = Color(0x962F4B61));
        graph.addEdge(fatherNode, sibling, paint: Paint()..color = Color(0x962F4B61));
      }
    });
    if (spouseNode.key.value is Relationship) {
      graph.addEdge(motherNode, spouseNode, paint: transparentPaint());
      graph.addEdge(fatherNode, spouseNode, paint: transparentPaint());
    }
    grandChildren.forEach((grandchild) {
      if (grandchild.key.value is Relationship) {
        User grandchildUser = (grandchild.key.value as Relationship).userTo;
        if (children.isNotEmpty && children.any((element) => element.key.value is Relationship)) {
          if (grandchildUser == null) {   //inactive grandChildUser
            graph.addEdge(children.first, grandchild, paint: transparentPaint());    //if there is at least one child, append the grandchild to him
          } else {
            bool grandChildAdded = false;
            children.forEach((child) {
              if (child.key.value is Relationship) {
                User childUser = (child.key.value as Relationship).userTo;
                if (childUser != null && childUser.confirmedRelationships.any((relationship)
                  => relationship.userTo != null //is active user
                      ? relationship.userTo.uid == grandchildUser.uid
                      : false))
                {
                  graph.addEdge(child, grandchild);
                  grandChildAdded = true;
                  return;
                }
              }
            });
            if (grandChildAdded == false) {
              graph.addEdge(children.first, grandchild, paint: transparentPaint());
            }
          }
        } else {
          graph.addEdge(me, grandchild);    //if there is no child, append the grandchild to me
        }
      }
    });

    return graph;
  }

  Paint transparentPaint() {
    return Paint()..color = Colors.red.withOpacity(0);
  }

  ///Returns all graph nodes of given type of family member
  List<Node> _getFamilyMemberNodes({@required List<Relationship> userRelationships, @required RelationshipRoleEnum role, GenderEnum gender, RelationshipSideEnum side}) {
    final Iterable<Relationship> relationships = _getFamilyMembersRelationships(family: userRelationships, role: role, gender: gender, side: side);
    if (relationships.isNotEmpty) {
      return relationships.map((element) => Node.Id(element)).toList();
    } else {
      return [Node.Id('${role.titleByGender(gender)}')];
    }
  }

  ///Returns graph node
  Node _getFamilyMemberSingleNode({@required List<Relationship> userRelationships, @required RelationshipRoleEnum role, GenderEnum gender, RelationshipSideEnum side}) {
    final Iterable<Relationship> relationships = _getFamilyMembersRelationships(family: userRelationships, role: role, gender: gender, side: side);
    if (relationships.isNotEmpty) {
      return relationships.map((element) => Node.Id(element)).first;
    } else {
      return Node.Id('${role.titleByGender(gender)}' + (side == null ? '' : ' zo strany ${side.titleSK}'));
    }
  }

  ///Returns given type of family member like mom or sisters
  Iterable<Relationship> _getFamilyMembersRelationships({@required List<Relationship> family, @required RelationshipRoleEnum role, GenderEnum gender, RelationshipSideEnum side}) {
    return family.where((relationship) {
      if (side != null && relationship.side != side)
        return false;
      if (relationship.role != role)
        return false;
      if (relationship.inactiveUserTo != null) {
        if (gender != null && relationship.inactiveUserTo.gender != gender)
          return false;
      } else {
        if (gender != null && relationship.userTo.gender != gender)
          return false;
      }
      return true;
    });
  }
}