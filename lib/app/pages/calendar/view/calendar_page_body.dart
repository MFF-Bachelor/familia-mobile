import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/calendar_item.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/pages/calendar/service/calendar_service.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/shopping_list/view/shopping_list_detail_page.dart';
import 'package:familia_mobile/app/pages/tasks/view/tasks_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';


class CalendarPageBody extends StatefulWidget {
  CalendarPageBody({Key key}) : super(key: key);

  @override
  CalendarPageBodyState createState() => CalendarPageBodyState();
}

class CalendarPageBodyState extends State<CalendarPageBody> {
  List<CalendarItem> myCalendarData = [];
  VoidCallback myRefetch;
  //List<RecipesFilterEnum> appliedFilters;
  //RecipesSortEnum appliedSort = RecipesSortEnum.CREATED_DATE_DESC;

  final CalendarService _calendarService = CalendarService();

  @override
  Widget build(BuildContext context)  {


    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        return  Query(
          builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
            myRefetch = refetch;
            CustomQueryResult queryResult = _calendarService.checkQueryResult(result, refetch: refetch, fetchMore: fetchMore);
            switch (queryResult) {
              case CustomQueryResult.EXCEPTION:
                return BuildWidgets.queryExceptionWidget(context, exceptionMessage: result.exception.toString());
                break;
              case CustomQueryResult.LOADING:
                return BuildWidgets.queryLoadingWidget(context);
                break;
              case CustomQueryResult.FAIL:
                return BuildWidgets.queryFailWidget(context);
                break;
              case CustomQueryResult.SUCCESS:
                return _buildBody(result);
                break;
            }
            return BuildWidgets.queryFailWidget(context);
          },
          options: QueryOptions(
            document: gql(Queries.getUserCalendarDataQuery),
          ),
        );
      }),
      // floatingActionButton: BuildWidgets.floatingButtonAddItem(
      //     context,
      //     nextRoute: MaterialPageRoute(builder: (context) => ShoppingListDetailPage(
      //         pageType: ShoppingListDetailPageTypeEnum.CREATE_PAGE,
      //         shoppingList: ShoppingList()
      //     )),
      //     onReturn: () {
      //       myRefetch();
      //       setState(() {});
      //     }
      // ),
    );
  }

  ///Parse data (assignedTasks and otherCreatedTasks) from query result, apply filters and sort and save
  ///the data into local variables myTasks and otherCreatedTasks
  _parseQueryResult(QueryResult result) {
    myCalendarData = _calendarService.fromApiObject(result.data[Queries.GET_USER_CALENDAR_DATA]['data']);

    // myRecipes = _recipeService.filterVisibleTasks(tasks: myRecipes, appliedFilters: appliedFilters, appliedSort: appliedSort);
    // willCookRecipes = _recipeService.filterVisibleTasks(tasks: willCookRecipes, appliedFilters: appliedFilters, appliedSort: appliedSort);
  }

  _buildBody(QueryResult result) {
    _parseQueryResult(result);

    //return SizedBox();
    return _buildCalendar(myCalendarData);
  }

  _buildCalendar(List<CalendarItem> calendarItems) {
    return SfCalendar(
      headerStyle: CalendarHeaderStyle(textStyle: Theme.of(context).textTheme.headline3),
      view: CalendarView.month,
      dataSource: MeetingDataSource(calendarItems),
      firstDayOfWeek: 1,
      appointmentTimeTextFormat: 'HH:mm',
      onTap: (CalendarTapDetails details)  {
        if (details.targetElement != CalendarElement.appointment)
          return null;

        if (details.appointments.length == 1) {
          CalendarItem item = details.appointments[0];
          switch (item.eventType) {
            case CalendarItemType.TASK:
              return Navigator.push(context, MaterialPageRoute(builder: (context) => TasksDetailPage(
                  pageType: TasksDetailPageTypeEnum.VIEW_PAGE,
                  task: item.entity
              ))).then((value) {
                myRefetch();
                setState(() {});
              });
            case CalendarItemType.SHOPPING_LIST:
              return Navigator.push(context, MaterialPageRoute(builder: (context) => ShoppingListDetailPage(
                  pageType: ShoppingListDetailPageTypeEnum.VIEW_PAGE,
                  shoppingList: item.entity
              ))).then((value) {
                myRefetch();
                setState(() {});
              });
            default:
              return null;
          }
        }
      },
      monthViewSettings: MonthViewSettings(
          showAgenda: true,
          appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
    );
  }
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<CalendarItem> source){
    appointments = source;
  }

  @override
  bool isAllDay(int index) {
    return appointments[index].isAllDay;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments[index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments[index].to;
  }

  @override
  String getSubject(int index) {
    return appointments[index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments[index].background;
  }
}