 import '../../common/text_constants.dart';
import 'package:familia_mobile/app/pages/calendar/view/calendar_page_body.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter/foundation.dart';

class CalendarPage extends StatefulWidget {
  CalendarPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  PanelController _panelController = new PanelController();
  //final GlobalKey<ShoppingListPageBodyState> _shoppingListPageBodyKey = GlobalKey();
  final GlobalKey<CalendarPageBodyState> _calendarPageBodyKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Column(
                  children: <Widget>[
                    OneRowHeader(
                        title: TextConstants.calendar_page_header_text,
                        rightSidedWidget: SizedBox()
                    ),
                    Expanded(
                        child: CalendarPageBody(key: _calendarPageBodyKey,)
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }

}


