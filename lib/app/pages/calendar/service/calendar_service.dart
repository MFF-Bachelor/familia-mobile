import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/calendar_item.dart';
import 'package:familia_mobile/app/entities/shopping_list.dart';
import 'package:familia_mobile/app/entities/task.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';
import 'package:familia_mobile/main.dart';


class CalendarService {

  ///Returns type of query result
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return CustomQueryResult.EXCEPTION;
    }

    if (result.isLoading) {
      return CustomQueryResult.LOADING;
    }

    var success = result.data[Queries.GET_USER_CALENDAR_DATA][Queries.SUCCESS];
    if (success) {
      sessionService.saveCookieFromApiResponse(result);
      return CustomQueryResult.SUCCESS;
    } else {
      return CustomQueryResult.FAIL;
    }
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success) {
        return CustomMutationResult.SUCCESS;
      } else {
        return CustomMutationResult.FAIL;
      }
    }
  }

  ///Converts json object from result to object in app
  fromApiObject(Map<String, dynamic> apiObject) {
    List<CalendarItem> calendarItems = [];

    apiObject['createdShoppingLists']?.forEach((shoppingList) {
      if (shoppingList['dueTime'] != null) {
        calendarItems.add(CalendarItem<ShoppingList>(
            CalendarItemType.SHOPPING_LIST,
            ShoppingList.fromApiObject(shoppingList),
            shoppingList['title'],
            true,
            TZDateTime.parse(currentTimezone, shoppingList['dueTime']),
            TZDateTime.parse(currentTimezone, shoppingList['dueTime']).add(const Duration(seconds: 1)),
            Colors.black
        ));
      }
    });

    apiObject['asigneeShoppingLists']?.forEach((shoppingList) {
      if (shoppingList['dueTime'] != null) {
        calendarItems.add(CalendarItem<ShoppingList>(
            CalendarItemType.SHOPPING_LIST,
            ShoppingList.fromApiObject(shoppingList),
            shoppingList['title'],
            true,
            TZDateTime.parse(currentTimezone, shoppingList['dueTime']),
            TZDateTime.parse(currentTimezone, shoppingList['dueTime']).add(const Duration(seconds: 1)),
            Colors.blue
        ));
      }
    });

    apiObject['assignedTasks']?.forEach((task) {
      if (task['dueTime'] != null) {
        calendarItems.add(CalendarItem<Task>(
            CalendarItemType.TASK,
            Task.fromApiObject(task),
            task['title'],
            false,
            TZDateTime.parse(currentTimezone, task['dueTime']),
            TZDateTime.parse(currentTimezone, task['dueTime']).add(const Duration(seconds: 1)),
            Colors.red
        ));
      }
    });

    return calendarItems;
  }

}