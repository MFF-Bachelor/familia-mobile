
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/enums/relationship_state_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/my_family/service/my_family_service.dart';
import 'package:familia_mobile/app/pages/my_family/view/my_family_detail_page.dart';
import 'package:familia_mobile/app/pages/my_family/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:simple_speed_dial/simple_speed_dial.dart';

class MyFamilyMainSection extends StatefulWidget {
  const MyFamilyMainSection({Key key}) : super(key: key);

  @override
  _MyFamilyMainSectionState createState() => _MyFamilyMainSectionState();
}

class _MyFamilyMainSectionState extends State<MyFamilyMainSection> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<Relationship> relationships = [];
  List<Relationship> notConfirmedRelationships = [];
  List<Relationship> myFamilyRelationships = [];
  List<Relationship> sentRelationships = [];
  VoidCallback myRefetch;

  var _subHeaderTextStyle;

  final MyFamilyService _myFamilyService = MyFamilyService();

  void categorizeRelationships() {
    notConfirmedRelationships = [];
    myFamilyRelationships = [];
    sentRelationships = [];
    relationships.forEach((relationship) {
      switch(relationship.state) {
        case RelationshipStateEnum.ACTIVE: {
          myFamilyRelationships.add(relationship);
        }
        break;
        case RelationshipStateEnum.NOT_CONFIRMED: {
          sentRelationships.add(relationship);
        }
        break;
        case RelationshipStateEnum.TO_CONFIRM: {
          notConfirmedRelationships.add(relationship);
        }
        break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _subHeaderTextStyle = Theme.of(context).textTheme.headline3.copyWith(color: Theme.of(context).primaryColor);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: Query(
          builder: (QueryResult result,
              { VoidCallback refetch, FetchMore fetchMore }) {
            myRefetch = refetch;
            if (result.hasException) {
              return Text(result.exception.toString());
            }

            if (result.isLoading) {
              return BuildWidgets.queryLoadingWidget(context);
            }

            var success = result.data[Queries.GET_USER_RELATIONSHIPS][Queries.SUCCESS];
            if (success) {
              relationships = [];
              result.data[Queries.GET_USER_RELATIONSHIPS]['data']['relationshipsFrom']?.forEach((relationship) =>
              {
                relationships.add(Relationship.fromApiObject(relationship))
              });
              apiAuthService.currentUser.relationships = relationships;
              if (relationships.isEmpty) {
                return BuildWidgets.queryNoDataWidget(context);
              }

              relationships?.sort((Relationship r1, Relationship r2) {
                return r2.created.compareTo(r1.created);
              });

              categorizeRelationships();
            } else {
              return BuildWidgets.queryFailWidget(context);
            }

            return SingleChildScrollView(
              child: Column(
                  children: [
                    Visibility(
                      visible: notConfirmedRelationships.isNotEmpty,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(TextConstants
                                .my_family_page_not_confirm_relationships_text,
                              style: _subHeaderTextStyle,),
                          ),
                          Column(
                            children: buildNotConfirmedRelationships(
                                notConfirmedRelationships),
                          )
                        ],
                      ),
                    ),
                    Visibility(
                        visible: myFamilyRelationships.isNotEmpty,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(TextConstants.my_family_page_my_family_text,
                                style: _subHeaderTextStyle,),
                            ),
                            Column(
                              children: buildMyFamily(myFamilyRelationships),
                            ),
                          ],
                        )
                    ),
                    Visibility(
                        visible: sentRelationships.isNotEmpty,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(TextConstants.my_family_page_sent_request_text,
                                style: _subHeaderTextStyle,),
                            ),
                            Column(
                              children: buildSentRelationship(sentRelationships),
                            ),
                          ],
                        )
                    ),
                  ]),
            );
          },
          options: QueryOptions(
            document: gql(Queries.getUserRelationshipsQuery),
          ),
        ),
        floatingActionButton: SpeedDial(
          child: Icon(Icons.add),
          closedForegroundColor: Colors.white,
          openForegroundColor: Colors.white,
          closedBackgroundColor: Theme.of(context).accentColor,
          openBackgroundColor: Theme.of(context).accentColor,
          speedDialChildren: <SpeedDialChild>[
            _createOneWayRelationshipButton(),
            _createTwoWayRelationshipButton()
            //  Your other SpeeDialChildren go here.
          ],
        ),
      ),
    );
  }

  List<Widget> buildMyFamily(List<Relationship> relationships) {
    List<Widget> myFamily = [];

    relationships?.forEach((relationship) {
      var widget = Padding(
          padding:EdgeInsets.all(5.0),
          child: Widgets.buildMyFamilyCard(
              context,
              fullName: relationship.userTo != null
                  ? relationship.userTo.firstName + " " + relationship.userTo.lastName
                  : relationship.inactiveUserTo.firstName + " " + relationship.inactiveUserTo.lastName,
              relationship: relationship,
              isOneWayRelationship: relationship.userTo != null ? false : true,
              onCardPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyFamilyDetailPage(
                        relationship: relationship,
                        pageType: relationship.userTo != null
                            ? MyFamilyDetailPageTypeEnum.VIEW_TWO_WAY_RELATIONSHIP
                            : MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP
                      )
                  ),
                ).then((needRefetch) {
                  if (needRefetch == null || needRefetch == true) {
                    myRefetch();
                    setState(() {});
                  }
                });
              })
      );

      myFamily.add(widget);
    });

    return myFamily;
  }

  List<Widget> buildNotConfirmedRelationships(List<Relationship> notConfirmedRelationships) {
    List<Widget> myFamily = [];

    notConfirmedRelationships?.forEach((relationship) {
      var widget = Padding(
          padding:EdgeInsets.all(5.0),
          child: Widgets.buildFamilyNonConfirmedRequestCard(
              context,
              relationship: relationship,
              fullName: relationship.userTo != null
                ? relationship.userTo.firstName + " " + relationship.userTo.lastName
                : relationship.inactiveUserTo.firstName + " " + relationship.inactiveUserTo.lastName,
              myFamilyService: _myFamilyService,
              isOneWayRelationship: relationship.userTo != null ? false : true,
              refetch: myRefetch,
              setState: setState,
          ));

      myFamily.add(widget);
    });

    return myFamily;
  }

  List<Widget> buildSentRelationship(List<Relationship> relationships) {
    List<Widget> myFamily = [];

    relationships?.forEach((relationship) {
      var widget = Padding(
          padding:EdgeInsets.all(5.0),
          child: Widgets.buildMyFamilyRequestCard(
              context,
              relationship: relationship,
              fullName: relationship.userTo != null
                  ? relationship.userTo.firstName + " " + relationship.userTo.lastName
                  : relationship.inactiveUserTo.firstName + " " + relationship.inactiveUserTo.lastName,
              isOneWayRelationship: relationship.inactiveUserTo != null ? true : false,
              refetch: myRefetch,
              setState: setState
          )
      );

      myFamily.add(widget);
    });

    return myFamily;
  }

  SpeedDialChild _createOneWayRelationshipButton() {
    return SpeedDialChild(
      child: Icon(Icons.perm_identity),
      foregroundColor: Theme.of(context).primaryColor,
      backgroundColor: Colors.white,
      label: 'Pridať neregistrovaného člena rodiny',
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MyFamilyDetailPage(
                  pageType: MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP,
                  relationship: Relationship()
              )),
        ).then((value) {
          myRefetch();
          setState(() {});
        });
      },
      closeSpeedDialOnPressed: true,
    );
  }

  SpeedDialChild _createTwoWayRelationshipButton() {
   return SpeedDialChild(
     child: Icon(Icons.person),
     foregroundColor: Theme.of(context).primaryColor,
     backgroundColor: Colors.white,
     label: 'Vytvoriť vzťah s registrovaným používateľom',
     onPressed: () {
       Navigator.push(
         context,
         MaterialPageRoute(
             builder: (context) => MyFamilyDetailPage(
                 pageType: MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP,
                 relationship: Relationship()
             )),
       ).then((value) {
         myRefetch();
         setState(() {});
       });
     },
     closeSpeedDialOnPressed: true,
   );
  }
}