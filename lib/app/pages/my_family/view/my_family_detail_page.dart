import 'package:community_material_icon/community_material_icon.dart';
import '../../../services/graphql_service/graphql_helper.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/contact.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:familia_mobile/app/enums/relationship_role_enum.dart';
import 'package:familia_mobile/app/enums/relationship_side_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/contacts/view/contact_detail_page.dart';
import 'package:familia_mobile/app/pages/family_tree/view/family_tree_page.dart';
import 'package:familia_mobile/app/pages/my_family/service/my_family_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart';


class MyFamilyDetailPage extends StatefulWidget {
  final MyFamilyDetailPageTypeEnum pageType;
  Relationship relationship;
  bool isOneWayRelationship;
  bool isViewPage;
  bool showEditButton;

  MyFamilyDetailPage({Key key, @required this.pageType, @required this.relationship, this.showEditButton}) : super(key: key) {
    showEditButton ??= true;
    if (
      pageType == MyFamilyDetailPageTypeEnum.VIEW_TWO_WAY_RELATIONSHIP
        || pageType == MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP
    ) isViewPage = true;
    else isViewPage = false;

    if (
    pageType == MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP
        || pageType == MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP
        || pageType == MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP
    ) {
      isOneWayRelationship = true;

      if (
        pageType != MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP
          && relationship.inactiveUserTo == null
      ) throw Exception("Custom Error: inactiveUserTo is null");
    }
    else {
      isOneWayRelationship = false;
      if (
        pageType != MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP
          && relationship.userTo == null
      ) throw Exception("Custom Error: userTo is null");
    }
  }

  @override
  _MyFamilyDetailPageState createState() => _MyFamilyDetailPageState();
}

class _MyFamilyDetailPageState extends State<MyFamilyDetailPage> {
  Relationship _relationship;
  Map<String, dynamic> data = {};
  var dateFormat = DateFormat('dd.MM.yyyy');
  var timeFormat = DateFormat('HH:mm');
  var dateTimeFormat = DateFormat('HH:mm dd.MM.yyyy');
  List<DropdownMenuItem> assignees;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  MyFamilyService _myFamilyService = MyFamilyService();
  GraphGLHelper _graphGLHelper = GraphGLHelper();
  TZDateTime _birthDateValue = TZDateTime.now(currentTimezone);
  bool _sideFieldVisibility = false;

  //Data controllers
  TextEditingController _emailController;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _birthDateController;
  RelationshipRoleEnum _roleActualValue;
  GenderEnum _genderActualValue;
  RelationshipSideEnum _sideActualValue;
  TextEditingController _bioController;
  List<DropdownMenuItem> _genderValues;
  List<DropdownMenuItem> _roleValues;
  List<DropdownMenuItem> _sideValues;

  @override
  void initState() {
    _relationship = widget.relationship;
    
    _genderValues = GenderEnum.values.map<DropdownMenuItem>((gender) {
      return DropdownMenuItem(
          value: gender,
          child: Text(gender.titleSK)
      );
    }).toList();

    _roleValues = RelationshipRoleEnum.values.map<DropdownMenuItem>((role) {
      return DropdownMenuItem(
          value: role,
          child: Text(role.titleSK)
      );
    }).toList();

    _sideValues = RelationshipSideEnum.values.map<DropdownMenuItem>((role) {
      return DropdownMenuItem(
          value: role,
          child: Text(role.titleSK)
      );
    }).toList();

    _initFormValues();
    /*_emailController = TextEditingController(text: _relationship.userTo == null ? '' : _relationship.userTo.emails.first);
    _firstNameController = TextEditingController(text: _relationship.userTo == null ? '' : _relationship.userTo.firstName);
    _lastNameController = TextEditingController(text: _relationship.userTo == null ? '' : _relationship.userTo.lastName);
    _birthDateController = _relationship.userTo == null ? TextEditingController(text: '') : TextEditingController(text: dateFormat.format(_relationship.userTo.birthDate));
    _roleActualValue = _relationship.role == null ? null : _relationship.role;
    _genderActualValue = _relationship.userToGender == null ? null : _relationship.userToGender;
    _sideActualValue = _relationship.side == null ? null : _relationship.side;
    _bioController = TextEditingController(text: _relationship.bio == null ? '' : _relationship.bio);
    data = {
    'recipePreparationTimeUnit': widget.recipe.recipePreparationTimeUnit == null ? null : widget.recipe.recipePreparationTimeUnit,
      'titleController': TextEditingController(text: widget.task.title == null ? '' : widget.task.title),
      'assignee': widget.task.assignee == null ? apiAuthService.currentUser : widget.task.assignee,
      'textController': TextEditingController(text: widget.task.text == null ? '' : widget.task.text),
      'dueDateController': widget.task.dueTime == null ? null : TextEditingController(text: dateFormat.format(widget.task.dueTime)),
      'dueTimeController': widget.task.dueTime == null ? null : TextEditingController(text: timeFormat.format(widget.task.dueTime)),
      'priority': widget.task.priority == null ? 1 : widget.task.priority,
      'canAssigneeEdit': widget.task.canAssigneeEdit == null ? true : widget.task.canAssigneeEdit
    }; */

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery
        .of(context)
        .padding
        .top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Column(
                  children: <Widget>[
                    OneRowHeader(
                      title: _buildTitle(),
                      rightSidedWidget:
                      (widget.pageType == MyFamilyDetailPageTypeEnum.VIEW_TWO_WAY_RELATIONSHIP
                      || widget.pageType == MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP)
                        ? widget.showEditButton ? _buildEditButton() : SizedBox()
                        : _buildSubmitButton(),
                    ),
                    Expanded(
                        child: _buildMyFamilyDetailPageBody()
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }

  String _buildTitle() {
    switch (widget.pageType) {
      case MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP:
      case MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP:
        return TextConstants.my_family_new_relationship_page_form_title;
      case MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP:
      case MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP:
        return TextConstants.my_family_edit_page_form_title;
      case MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP:
      case MyFamilyDetailPageTypeEnum.VIEW_TWO_WAY_RELATIONSHIP:
        return TextConstants.my_family_view_relationship_page_form_title;
    }
  }

  _buildMyFamilyDetailPageBody() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              SizedBox( height: 20,),
              //_buildFormBody(),
              (widget.pageType == MyFamilyDetailPageTypeEnum.VIEW_ONE_WAY_RELATIONSHIP
              || widget.pageType == MyFamilyDetailPageTypeEnum.VIEW_TWO_WAY_RELATIONSHIP)
                  ? _buildViewBody()
                  : _buildFormBody(),
              SizedBox( height: 20,)
            ],
          ),
        ),
      ),
    );
  }

  _buildViewBody() {
    return Column(
      children: [
        Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  widget.isOneWayRelationship
                    ? _relationship.inactiveUserTo.firstName + " " + _relationship.inactiveUserTo.lastName
                    : _relationship.userTo.firstName + " " + _relationship.userTo.lastName,
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox( height: 20,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.person, color: Theme.of(context).primaryColorLight,),
                    SizedBox( width: 5,),
                    Text(
                      _relationship.role.sideValueVisibility && _relationship.side != null
                          ? _relationship.role.titleByGender(_relationship.userToGender)
                            + " zo strany " + _relationship.side.titleSK
                          : _relationship.role.titleByGender(_relationship.userToGender),
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
                SizedBox( height: 5,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      _relationship.userToGender == GenderEnum.MALE
                          ? CommunityMaterialIcons.gender_male
                          : CommunityMaterialIcons.gender_female,
                      color: Theme.of(context).primaryColorLight,
                    ),
                    SizedBox( width: 5,),
                    Text(
                      _relationship.userToGender.titleSK,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
                SizedBox( height: 5,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(CommunityMaterialIcons.cake, color: Theme.of(context).primaryColorLight,),
                    SizedBox( width: 5,),
                    Container(
                      child: Center(
                        child: Text(
                          widget.isOneWayRelationship
                              ? "Narodený " + dateFormat.format(_relationship.inactiveUserTo.birthDate)
                                + " (" + _getActualAge(_relationship.inactiveUserTo.birthDate) + ")"
                              : "Narodený " + dateFormat.format(_relationship.userTo.birthDate)
                              + " (" + _getActualAge(_relationship.userTo.birthDate) + ")",
                          style: Theme.of(context).textTheme.bodyText1,
                          textAlign: TextAlign.end,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
        SizedBox(height: 20,),
        widget.isOneWayRelationship ? SizedBox() : _buildContacts(),
        Visibility(
          visible: _relationship.bio != null && _relationship.bio != '',
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Bio', style: Theme.of(context).textTheme.headline4),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10,),
                      Container(
                        width: 320,
                        child: Text(
                          _relationship.bio == null ? "" : _relationship.bio,
                          style: Theme.of(context).textTheme.bodyText1,
                          overflow: TextOverflow.fade,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: 20,),
        widget.isOneWayRelationship
            ? _buildCreateContactFromRelationshipButton(
              name: _relationship.inactiveUserTo.firstName + " " + _relationship.inactiveUserTo.lastName,
              phoneNumber: null,
              email: null
            )
            : _buildCreateContactFromRelationshipButton(
              name: _relationship.userTo.firstName + " " + _relationship.userTo.lastName,
              phoneNumber: _relationship.userTo.phoneNumbers == null || _relationship.userTo.phoneNumbers.isEmpty ? null : _relationship.userTo.phoneNumbers[0],
              email: _relationship.userTo.emails == null || _relationship.userTo.emails.isEmpty ? null : _relationship.userTo.emails[0]
            ),
        SizedBox(height: 20,),
        !widget.isOneWayRelationship
            ? _buildShowUserFamilyTree(_relationship.userTo.uid)
            : SizedBox()

      ],
    );
  }

  _buildFormBody() {
    return Wrap(
      spacing: 20.0,
      children: [
        Visibility(
          visible: (widget.pageType == MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP) ||
                   (widget.pageType == MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP),
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_email,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _emailController,
                enabled: widget.pageType == MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP,
                validator: (String value) {
                  return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: widget.pageType != MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_first_name,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _firstNameController,
                enabled: widget.pageType != MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
                validator: (String value) {
                  return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: widget.pageType != MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP,
          child:  BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_last_name,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _lastNameController,
                enabled: widget.pageType != MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
                validator: (String value) {
                  return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: widget.pageType != MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP,
          child: InkWell(
            child: AbsorbPointer(
              child:
                BuildWidgets.buildInputField(
                  context,
                  label: TextConstants.relationship_birth_date,
                  inputField: BuildWidgets.buildBasicTextFormField(
                      context,
                      controller: _birthDateController,
                      enabled: widget.pageType != MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
                      validator: (String value) {
                        return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                      }
                  ),
                ),
              ),
              onTap: () => BuildWidgets.selectDate(
                  birthDateValue: _birthDateValue,
                  context: context,
                  onPickedDate:(picked) {
                    setState(() {
                      _birthDateValue = picked;
                      _birthDateController.text = dateFormat.format(_birthDateValue);
                    });
                  }
              ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_gender,
            inputField: BuildWidgets.buildBasicDropdownFormField(
                context,
                items: _genderValues,
                value: _genderActualValue,
                enabled: widget.pageType != MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
                validator: (value) {
                  return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                },
                onChanged: (value) {
                  setState(() {
                    _genderActualValue = value;

                    _roleValues = RelationshipRoleEnum.values.map<DropdownMenuItem>((role) {
                      return DropdownMenuItem(
                          value: role,
                          child: Text(role.titleByGender(_genderActualValue))
                      );
                    }).toList();
                  });
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_role,
            inputField: BuildWidgets.buildBasicDropdownFormField(
                context,
                items: _roleValues,
                value: _roleActualValue,
                enabled: widget.pageType != MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
                validator: (value) {
                  return value == null || value == '' ? TextConstants.non_empty_input_alert : null;
                },
                onChanged: (value) {
                  setState(() {
                    _roleActualValue = value;
                    _sideFieldVisibility = _roleActualValue.sideValueVisibility;
                    _sideActualValue = null;
                  });
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: _sideFieldVisibility ?? true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_side,
            inputField: BuildWidgets.buildBasicDropdownFormField(
                context,
                items: _sideValues,
                value: _sideActualValue,
                enabled: true,
                onChanged: (value) {
                  setState(() {
                    _sideActualValue = value;
                  });
                }
            ),
          ),
        ),
        SizedBox( height: 20,),
        Visibility(
          visible: true,
          child: BuildWidgets.buildInputField(
            context,
            label: TextConstants.relationship_bio,
            inputField: BuildWidgets.buildBasicTextFormField(
                context,
                controller: _bioController,
                enabled: true,
                maxLines: 5
            ),
          ),
        ),
        SizedBox( height: 30,),
        Visibility(
          visible: widget.pageType == MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP
            || widget.pageType == MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildRemoveRelationshipButton(_relationship)
          ],
          )
        )
      ],
    );
  }

  _buildSubmitButton() {
    return Mutation (
        options: _mutationOptionsBasedOnPageType(),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildDoneButton(
              onPressed: () {
                var dueTime = null;
                if (data['dueTimeController'] != null && data['dueDateController'] != null)
                  dueTime = TZDateTime.from(dateTimeFormat.parse(data['dueTimeController'].text + " " + data['dueDateController'].text), currentTimezone);

                if (_formKey.currentState.validate()) {
                  _runMutationBasedOnPageType(runMutation);
                }
              });
        });
  }

  _buildEditButton() {
    return IconButton(
        icon: Icon(
          Icons.edit,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MyFamilyDetailPage(
                    pageType: widget.isOneWayRelationship
                      ? MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP
                      : MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP,
                    relationship: _relationship
                )
            ),
          ).then((value) => Navigator.pop(context, true));
        }
    );
  }

  _buildRemoveRelationshipButton(Relationship relationship) {
    return Mutation(
        options: _graphGLHelper.entityMutationOptions(context,
            entityService: _myFamilyService,
            mutationDefinition: Mutations.deleteRelationshipMutation,
            mutationName: Mutations.DELETE_RELATIONSHIP,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildBasicOutlinedButton(
              context,
              text: TextConstants.delete_relationship_button_text,
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _myFamilyService.onDeleteButtonTap(
                      runMutation,
                      id: relationship.id,
                      onError: () {
                        throw Exception("Custom error: on delete failed");
                      }
                  );
                }
              }
          );
        }
    );
  }

  _mutationOptionsBasedOnPageType() {
    switch (widget.pageType) {
      case MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _myFamilyService,
            mutationDefinition: Mutations.createOneWayRelationshipMutation,
            mutationName: Mutations.CREATE_ONE_WAY_RELATIONSHIP,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      case MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _myFamilyService,
            mutationDefinition: Mutations.createTwoWayRelationshipMutation,
            mutationName: Mutations.CREATE_TWO_WAY_RELATIONSHIP,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      case MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _myFamilyService,
            mutationDefinition: Mutations.updateOneWayRelationshipMutation,
            mutationName: Mutations.UPDATE_ONE_WAY_RELATIONSHIP,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      case MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP:
        return _graphGLHelper.entityMutationOptions(
            context,
            entityService: _myFamilyService,
            mutationDefinition: Mutations.updateTwoWayRelationshipMutation,
            mutationName: Mutations.UPDATE_TWO_WAY_RELATIONSHIP,
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
            },
            onSuccess: (result) {
              Navigator.of(context).pop();
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
            }
        );
        break;
      default: return null;
    }
  }

  _runMutationBasedOnPageType(RunMutation runMutation) {
    switch (widget.pageType) {
      case MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP:
        _myFamilyService.runCreateOneWayRelationshipMutation(
          runMutation,
          firstName: _firstNameController.text.trim(),
          lastName: _lastNameController.text.trim(),
          birthDate: dateTimeFormat.format(_birthDateValue),
          bio: _bioController.text.trim(),
          dropDownRole: (_roleActualValue as RelationshipRoleEnum).title,
          dropDownSide: (_sideActualValue as RelationshipSideEnum).title,
          dropDownGender: (_genderActualValue as GenderEnum).title,
          onError: () {
            BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.registration_failed_alert);
          },
          context: context,
        );
        break;
      case MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP:
        _myFamilyService.runCreateTwoWayRelationshipMutation(
          runMutation,
          userToEmail: _emailController.text.trim(),
          bio: _bioController.text.trim(),
          dropDownRole: (_roleActualValue as RelationshipRoleEnum).title,
          dropDownSide: (_sideActualValue as RelationshipSideEnum).title,
          dropDownGender: (_genderActualValue as GenderEnum).title,
          onError: () {
            BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.registration_failed_alert);
          },
          context: context,
          userTo: null,
        );
        break;
      case MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP:
        _myFamilyService.runUpdateOneWayRelationshipMutation(
            runMutation,
            relationshipId: _relationship.id,
            firstName: _firstNameController.text.trim(),
            lastName: _lastNameController.text.trim(),
            birthDate: dateTimeFormat.format(_birthDateValue),
            gender: (_genderActualValue as GenderEnum).title,
            role: (_roleActualValue as RelationshipRoleEnum).title,
            side:  (_sideActualValue as RelationshipSideEnum).title,
            bio: _bioController.text.trim(),
            onError: () {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.registration_failed_alert);
            },
            context: context
        );
        break;
      case MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP:
        _myFamilyService.runUpdateTwoWayRelationshipMutation(
            runMutation,
            relationshipId: _relationship.id,
            side:  (_sideActualValue as RelationshipSideEnum).title,
            bio: _bioController.text.trim(),
            onError: () {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.registration_failed_alert);
            },
            context: context
        );
        break;
      default: return null;
    }
  }

  _initFormValues() {
    switch (widget.pageType) {
      case MyFamilyDetailPageTypeEnum.CREATE_ONE_WAY_RELATIONSHIP:
      case MyFamilyDetailPageTypeEnum.CREATE_TWO_WAY_RELATIONSHIP:
        _emailController = TextEditingController(text:'');
        _firstNameController = TextEditingController(text: '');
        _lastNameController = TextEditingController(text: '');
        _birthDateController = TextEditingController(text: '');
        _roleActualValue = null;
        _genderActualValue = null;
        _sideActualValue = null;
        _bioController = TextEditingController(text: '');
        break;
      case MyFamilyDetailPageTypeEnum.UPDATE_ONE_WAY_RELATIONSHIP:
        _emailController = TextEditingController(text: '');
        _firstNameController = TextEditingController(text: _relationship.inactiveUserTo.firstName);
        _lastNameController = TextEditingController(text: _relationship.inactiveUserTo.lastName);
        _birthDateController = TextEditingController(text: dateFormat.format(_relationship.inactiveUserTo.birthDate));
        _roleActualValue = _relationship.role;
        _genderActualValue = _relationship.userToGender;
        _sideActualValue = _relationship.side;
        _bioController = TextEditingController(text: _relationship.bio);
        break;
      case MyFamilyDetailPageTypeEnum.UPDATE_TWO_WAY_RELATIONSHIP:
          _emailController = TextEditingController(text: _relationship.userTo.emails.first);
          _firstNameController = TextEditingController(text: _relationship.userTo.firstName);
          _lastNameController = TextEditingController(text: _relationship.userTo.lastName);
          _birthDateController = TextEditingController(text: dateFormat.format(_relationship.userTo.birthDate));
          _roleActualValue = _relationship.role;
          _genderActualValue = _relationship.userToGender;
          _sideActualValue = _relationship.side;
          _bioController = TextEditingController(text: _relationship.bio);
        break;
    }

    _sideFieldVisibility = _roleActualValue.sideValueVisibility;
  }

  _getActualAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }

  _buildCreateContactFromRelationshipButton({name, phoneNumber, email}) {
    return BuildWidgets.buildBasicOutlinedButton(
        context,
        text: TextConstants.create_contact_from_relationship_button_text,
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ContactDetailPage(
                  pageType: ContactDetailPageTypeEnum.CREATE_PAGE,
                  contact: Contact.create(name, phoneNumber, email)
              ))
          );
        }
    );




  }

  _buildShowUserFamilyTree(String userUid) {
    return BuildWidgets.buildBasicButton(
        context,
        text: TextConstants.show_user_family_tree_button_title,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FamilyTreePage(userUid: userUid)),
          );
        }
    );
  }
  
  Widget _buildContacts() {
    return Column(
      children: [
        Row(
          children: [
            Text('Kontakt', style: Theme.of(context).textTheme.headline3),
          ],
        ),
        Visibility(
            visible: !(_relationship.userTo.emails.length > 0 || _relationship.userTo.phoneNumbers.length > 0),
            child: SizedBox(height: 20,)
        ),
        Visibility(
          visible: !(_relationship.userTo.emails.length > 0 || _relationship.userTo.phoneNumbers.length > 0),
          child: Row(
            children: [
              SizedBox(width: 10,),
              Text('Nie sú dostupné žiadne kontakty', style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
        SizedBox(height: 20,),
        Visibility(
          visible: _relationship.userTo.emails.length != 0,
          child: Row(
            children: [
              SizedBox(width: 10,),
              Text('Email', style: Theme.of(context).textTheme.headline5),
              SizedBox(width: 10,),
              Text(_relationship.userTo.emails.isEmpty
                      ? ""
                      : _relationship.userTo.emails.first, style: Theme.of(context).textTheme.bodyText1
              ),
            ],
          ),
        ),
        Visibility(
            visible: _relationship.userTo.emails.length != 0,
            child: SizedBox(height: 20,)
        ),
        Visibility(
          visible: _relationship.userTo.phoneNumbers.length != 0,
          child: Row(
            children: [
              SizedBox(width: 10,),
              Text('Telefón', style: Theme.of(context).textTheme.headline5),
              SizedBox(width: 10,),
              Text(
                  _relationship.userTo.phoneNumbers.length != 0
                    ? _relationship.userTo.phoneNumbers.first
                    : "",
                  style: Theme.of(context).textTheme.bodyText1
              ),
            ],
          ),
        ),
        Visibility(
            visible: _relationship.userTo.phoneNumbers.length != 0,
            child: SizedBox(height: 20,)),
      ],
    );
  }
}

enum MyFamilyDetailPageTypeEnum {
  CREATE_ONE_WAY_RELATIONSHIP,
  CREATE_TWO_WAY_RELATIONSHIP,
  UPDATE_ONE_WAY_RELATIONSHIP,
  UPDATE_TWO_WAY_RELATIONSHIP,
  VIEW_ONE_WAY_RELATIONSHIP,
  VIEW_TWO_WAY_RELATIONSHIP
}