import 'package:enum_to_string/enum_to_string.dart';
import 'package:familia_mobile/app/pages/common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/text_constants.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/enums/relationship_role_enum.dart';
import 'package:familia_mobile/app/pages/my_family/service/my_family_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class Widgets {
  static final MyFamilyService _myFamilyService = MyFamilyService();

  static Widget _buildDeleteButton(int relationshipId) {
    return Mutation(
        options: MutationOptions(
            document: gql(Mutations.deleteRelationshipMutation),
            onCompleted: (resultData) {
              _myFamilyService.onDeleteCompleted(
                  resultData,
                  onSuccess: () {
                  },
                  onError: (context) {
                    BuildWidgets.showCustomSnackBar(context, error: true, message: "Odstránenie bolo neúspešné");
                  });
            },

        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return IconButton(
              icon: Icon(Icons.highlight_remove),
              onPressed: () {
                _myFamilyService.onDeleteButtonTap(
                    runMutation,
                    id: relationshipId,
                    onError: (context) {
                      BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
                    });
              }
          );
        }
    );
  }

  static Widget buildMyFamilyCard(BuildContext context, {
    @required Relationship relationship,
    @required String fullName,
    @required bool isOneWayRelationship,
    @required Function onCardPressed
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
      child: InkWell(
        child: BuildWidgets.buildBasicTitledCard(
          context,
          cardTitle: fullName,
          widgetBeforeTitle: isOneWayRelationship
              ? Icon(Icons.perm_identity, color: Theme.of(context).primaryColorLight,)
              : Icon(Icons.person, color: Theme.of(context).primaryColorLight,),
          bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
          firstRowWidget: Text(
              relationship.role.titleByGender(relationship.userToGender),
              style: Theme.of(context).textTheme.bodyText1
          ),
        ),
        onTap: () => onCardPressed(),
      ),
    );
  }

  static Widget buildMyFamilyRequestCard(BuildContext context, {
    @required Relationship relationship,
    @required String fullName,
    @required bool isOneWayRelationship,
    @required refetch,
    @required setState
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
        child: BuildWidgets.buildBasicTitledCard(
          context,
          cardTitle: fullName,
          widgetBeforeTitle: isOneWayRelationship
              ? Icon(Icons.perm_identity, color: Theme.of(context).primaryColorLight,)
              : Icon(Icons.person, color: Theme.of(context).primaryColorLight,),
          bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
          leftSidedVerticalWidget:SizedBox(width: 0),
          rightTopWidget: BuildWidgets.buildDeleteButton(
              elementId: relationship.id,
              service: _myFamilyService,
              mutationType: Mutations.deleteRelationshipMutation,
              refreshPage: () {
                refetch();
                setState(() {});
              },
              button: Icon(Icons.highlight_remove)
          ),
          firstRowWidget: Text(
              relationship.role.titleByGender(relationship.userToGender),
              style: Theme.of(context).textTheme.bodyText1
          ),
          secondRowWidget: SizedBox( height: 10,),
        ),
    );
  }

  static Widget buildFamilyNonConfirmedRequestCard(BuildContext context, {
    @required Relationship relationship,
    @required String fullName,
    @required bool isOneWayRelationship,
    @required MyFamilyService myFamilyService,
    @required refetch,
    @required setState
  }) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
      child: BuildWidgets.buildBasicTitledCard(
        context,
        cardTitle: fullName,
        widgetBeforeTitle: isOneWayRelationship
            ? Icon(Icons.perm_identity, color: Theme.of(context).primaryColorLight,)
            : Icon(Icons.person, color: Theme.of(context).primaryColorLight,),
        bodyPadding: EdgeInsets.fromLTRB(8, 17, 8, 17),
        leftSidedVerticalWidget:SizedBox(width: 0),
        rightTopWidget: SizedBox(),
        firstRowWidget: SizedBox(),
        secondRowWidget: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Text(
                    relationship.role.titleByGender(relationship.userToGender),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 16)
                ),
                SizedBox(height: 10,),
              ],
            ),
            Column(
              children: [
                SizedBox(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Mutation(
                      builder: (MultiSourceResult Function(Map<String, dynamic>, {Object optimisticResult}) runMutation, QueryResult result) {
                        return InkWell(
                          child: Text(TextConstants.add_text, style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 15)),
                          onTap: () {
                            runMutation({
                              "relationshipId": relationship.id
                            });
                          },
                        );
                      },
                      options: MutationOptions(
                        document: gql(Mutations.confirmRelationshipMutation),
                        onCompleted: (resultData) {
                          myFamilyService.onConfirmRelationshipCompleted(resultData,
                              onSuccess: (relationships) {
                                BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
                                refetch();
                                setState(() {});
                              },
                              onError: () {
                                BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
                              });
                        },
                      ),
                    ),
                    SizedBox(width: 15,),
                    BuildWidgets.buildDeleteButton(
                        elementId: relationship.id,
                        service: _myFamilyService,
                        mutationType: Mutations.deleteRelationshipMutation,
                        refreshPage: () {
                          refetch();
                          setState(() {});
                        },
                        button: Text(
                          TextConstants.reject_text,
                          style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 15, color: Colors.red)
                        )
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}