import 'package:community_material_icon/community_material_icon.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/entities/relationship.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MyFamilyService {
  Future<void>  runCreateTwoWayRelationshipMutation(
      RunMutation runCreateTwoWayRelationshipMutation,
      {
        @required onError,
        @required context,
        @required User userTo,
        @required userToEmail,
        @required bio,
        @required dropDownRole,
        @required dropDownSide,
        @required dropDownGender
      }) async {
    if (userTo == null) {
      runCreateTwoWayRelationshipMutation({
        "inputData": {
          "userToEmail": userToEmail,
          "role": dropDownRole,
          "gender": dropDownGender,
          "side": dropDownSide,
          "bio": bio,
        }
      });
    } else
      onError();
  }

  Future<void>  runCreateOneWayRelationshipMutation(
      RunMutation runCreateTwoWayRelationshipMutation,
      {
        @required onError,
        @required context,
        @required firstName,
        @required lastName,
        @required birthDate,
        @required dropDownRole,
        @required dropDownSide,
        @required dropDownGender,
        @required bio
      }) async {
      runCreateTwoWayRelationshipMutation({
        "inputData": {
          "firstName": firstName,
          "lastName": lastName,
          "birthDate": birthDate,
          "role": dropDownRole,
          "gender": dropDownGender,
          "side": dropDownSide,
          "bio": bio,
        }
      });
  }

  void onCreateTwoWayRelationshipCompleted(dynamic resultData, {@required void Function() onSuccess, @required onError}) {
    var success = resultData[Mutations.CREATE_TWO_WAY_RELATIONSHIP][Mutations.SUCCESS];
    if (success) {
      //var user = User.fromApiObject(resultData[Mutations.createTwoWayRelationshipMutation]['data']);
      onSuccess();
    } else {
      onError();
    }
  }

  void onConfirmRelationshipCompleted(dynamic resultData, {@required void Function(List<Relationship>) onSuccess, @required onError}) {
    var success = resultData[Mutations.CONFIRM_RELATIONSHIP][Mutations.SUCCESS];
    if (success) {
      List<Relationship> relationships = [];
      resultData[Mutations.CONFIRM_RELATIONSHIP]['data']?.forEach((relationship) => {
        relationships.add(Relationship.fromApiObject(relationship))
      });
      onSuccess(relationships);
    } else {
      onError();
    }
  }

  Future<void> onDeleteButtonTap(
      RunMutation runDeleteRelationshipMutation,
      {
          @required id,
          @required onError,
      }) async {
    runDeleteRelationshipMutation({
      "relationshipId": id
    });
  }

  void onDeleteCompleted(dynamic resultData, {@required void Function() onSuccess, @required onError}) {
    var success = resultData[Mutations.DELETE_RELATIONSHIP][Mutations.SUCCESS];
    if (success) {
      //apiAuthService.setCurrentUser(User.fromApiObject(resultData[Mutations.UPDATE_USER]['data']));
      onSuccess();
    } else {
      onError();
    }
  }

  void onUpdateCompleted(
      dynamic resultData, {@required void Function() onSuccess, @required onError}
      ) {
    var success = resultData[Mutations.UPDATE_TWO_WAY_RELATIONSHIP][Mutations.SUCCESS];
    if (success) {
      onSuccess();
    } else {
      onError();
    }
  }

  Future<void>  runUpdateTwoWayRelationshipMutation(
      RunMutation runUpdateTwoWayRelationshipMutation,
      {
        @required onError,
        @required context,
        @required relationshipId,
        @required side,
        @required bio
      }) async {
    if (relationshipId != null) {
      runUpdateTwoWayRelationshipMutation({
        "inputData": {
          "relationshipId": relationshipId,
          "side": side,
          "bio": bio
        }
      });
    } else
      onError();
  }

  Future<void>  runUpdateOneWayRelationshipMutation(
      RunMutation runUpdateOneWayRelationshipMutation,
      {
        @required onError,
        @required context,
        @required relationshipId,
        @required firstName,
        @required lastName,
        @required birthDate,
        @required gender,
        @required role,
        @required side,
        @required bio
      }) async {
    if (relationshipId != null) {
      runUpdateOneWayRelationshipMutation({
        "inputData": {
          "relationshipId": relationshipId,
          "firstName": firstName,
          "lastName": lastName,
          "birthDate": birthDate,
          "gender": gender,
          "role": role,
          "side": side,
          "bio": bio
        }
      });
    } else
      onError();
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success) {
        return CustomMutationResult.SUCCESS;
      } else {
        return CustomMutationResult.FAIL;
      }
    }
  }
}