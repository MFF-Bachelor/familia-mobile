import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:familia_mobile/app/services/firebase_service.dart';

class RegistrationService {
  Future<void> onSubmitButtonTap(RunMutation runRegistrationMutation, _data,
      _initDropdownValue, {@required onError, @required context}) async {
    var result = await FirebaseService.registrationWithEmailAndPass(
        email: _data['login']['controller'].text.trim(),
        pass: _data['password']['controller'].text,
        onEmailInUse: (email) {
          GraphQLProvider.of(context).value.mutate(
              MutationOptions(
                document: gql(Mutations.deleteUserFromFirebaseMutation),
                variables: {
                  "email": email
                },
                onCompleted: (dynamic resultData) {
                  var success = resultData[Mutations.DELETE_USER_FROM_FIREBASE][Mutations.SUCCESS];
                  if (success) {
                    onSubmitButtonTap(runRegistrationMutation, _data, _initDropdownValue, onError: onError, context: context);
                  } else {
                    onError();
                  }
                }
              )
          );
        }
    );
    var success = result[0];
    var user = result[1];
    if (user != null) {
      runRegistrationMutation(
          {
            "inputData": {
              "uid": user.uid,
              "state": "ACTIVE",
              "firstName": _data['name']['controller'].text.trim(),
              "lastName": _data['surname']['controller'].text.trim(),
              "emails": [_data['email']['controller'].text.trim()],
              "phoneNumbers": [_data['phoneNumber']['controller'].text.trim()],
              "birthDate": _data['birthDate']['controller'].text.trim(),
              "gender": _initDropdownValue,
              "street": _data['street']['controller'].text.trim(),
              "descriptiveNumber": _data['descriptiveNumber']['controller'].text.trim(),
              "routingNumber": _data['routingNumber']['controller'].text.trim(),
              "postcode": _data['postcode']['controller'].text.trim(),
              "city": _data['city']['controller'].text.trim(),
              "country": _data['country']['controller'].text.trim()
            }
          });
    } else {
      if (!success)
        onError();
    }
    // Process data.
  }

  void onRegistrationCompleted(dynamic resultData, {@required void Function(User user) onSuccess, @required onError}) {
    var success = resultData[Mutations.CREATE_USER][Mutations.SUCCESS];
    if (success) {
      var user = User.fromApiObject(resultData[Mutations.CREATE_USER]['data']);
      onSuccess(user);
    } else {
      onError();
    }
  }
}