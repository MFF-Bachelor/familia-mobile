import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import '../../common/text_constants.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import '../../common/build_widgets.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/login/view/login_view.dart';
import 'package:familia_mobile/app/pages/registration/service/registration_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:timezone/timezone.dart';
import 'package:intl/src/intl/date_format.dart';

class RegistrationFormPage extends StatefulWidget {
  final User user;
  const RegistrationFormPage({Key key, this.user}) : super(key: key);

  @override
  _RegistrationFormPageState createState() => _RegistrationFormPageState();
}

class _RegistrationFormPageState extends State<RegistrationFormPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  User _user;

  static const _arrowSize = 35.0;
  var _bottomBarColor;
  static const _textColor = Color(0xFFF2F4B61);
  var _nextArrowVisibility = true;

  var _data;
  GenderEnum _initDropdownValue;
  final RegistrationService _registrationService = RegistrationService();
  TZDateTime _birthDateValue = TZDateTime.now(currentTimezone);
  var dateFormat = DateFormat('dd.MM.yyyy');
  List<DropdownMenuItem> genderList;

  PageController pageViewController = PageController();

  @override
  void initState() {
    super.initState();
    _data = initStaticLayoutValues();
    _user = widget.user;
  }

  Map<String, dynamic> initStaticLayoutValues() {
    genderList = GenderEnum.values.map<DropdownMenuItem>((genderEnumItem) {
      return DropdownMenuItem(
          value: genderEnumItem,
          child: Text(genderEnumItem.titleSK)
      );
    }).toList();

    Map<String, dynamic> data = {
      'login': {
        'label': TextConstants.registration_form_page_login,
        'required' : true,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_login'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$');
          return regex.hasMatch(value)
              ? null
              : TextConstants.wrong_email_format_alert;
        }
      },
      'password': {
        'label': TextConstants.registration_form_page_pass,
        'required' : true,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_password'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          return null;
        }
      },
      'confirm_password': {
        'label': TextConstants.registration_form_page_confirm_pass,
        'required' : true,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_confirm_password'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          if (value != _data['password']['controller'].text)
            return TextConstants.not_match_input_alert;
          return null;
        }
      },
      'name': {
        'label': TextConstants.registration_form_page_first_name,
        'required' : true,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_name'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          return null;
        }
      },
      'surname': {
        'label': TextConstants.registration_form_page_last_name,
        'required' : true,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_surname'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          return null;
        }
      },
      'birthDate': {
        'label': TextConstants.registration_form_page_birth_date,
        'required' : true,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_birthDate'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          return null;
        }
      },
      'email': {
        'label': TextConstants.registration_form_page_email,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_email'),
      },
      'phoneNumber': {
        'label': TextConstants.registration_form_page_phone_number,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_phoneNumber'),
      },
      'gender': {
        'label': TextConstants.registration_form_page_gender,
        'required' : true,
        'values': <String>['MALE', 'FEMALE'],
        'key': GlobalKey(debugLabel: 'registration_form_page_gender'),
        'validator': (String value) {
          if (value == null || value.isEmpty) {
            return TextConstants.non_empty_input_alert;
          }
          return null;
        }
      },
      'street': {
        'label': TextConstants.registration_form_page_street,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_street'),
      },
      'descriptiveNumber' : {
        'label': TextConstants.registration_form_page_descriptive_number,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_descriptiveNumber'),
      },
      'routingNumber' : {
        'label': TextConstants.registration_form_page_routing_number,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_routingNumber'),
      },
      'postcode' : {
        'label': TextConstants.registration_form_page_postcode,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_postcode'),
        'validator': (String value) {
          if (value == null || value == '')
            return null;
          return (value.length > 5) ? TextConstants.wrong_number_of_characters_psc : null;
        },
      },
      'city' : {
        'label': TextConstants.registration_form_page_city,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_city'),
      },
      'country' : {
        'label': TextConstants.registration_form_page_country,
        'required' : false,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_country'),
      },
      'submit' : {
        'text': TextConstants.registration_form_page_submit,
        'controller': TextEditingController(text: ''),
        'key': GlobalKey(debugLabel: 'registration_form_page_submit'),
      },
    };

    return data;
  }

  bool _lastPage = false;
  bool _firstPage = true;
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    _bottomBarColor = Theme.of(context).primaryColorLight;
    var _pages = [
      {
        'keys': [
        _data['login']['key'],
        _data['password']['key'],
        _data['confirm_password']['key']
        ],
        'body': SingleChildScrollView(
          child: Wrap(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children:
                  [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                          TextConstants.registration_form_page_login_information_section,
                          style: Theme.of(context).textTheme.headline4
                      ),
                    ),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['login']['required'],
                      label: _data['login']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['login']['controller'],
                          validator: _data['login']['validator'],
                          key: _data['login']['key']
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['password']['required'],
                      label: _data['password']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['password']['controller'],
                          validator: _data['password']['validator'],
                          key: _data['password']['key'],
                          obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['confirm_password']['required'],
                      label: _data['confirm_password']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['confirm_password']['controller'],
                          validator: _data['confirm_password']['validator'],
                          key: _data['confirm_password']['key'],
                          obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      },
      {
        'keys': [
          _data['name']['key'],
          _data['surname']['key'],
          _data['birthDate']['key'],
          _data['gender']['key']
        ],
        'body': SingleChildScrollView (
          child: Wrap(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children:
                  [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                          TextConstants.registration_form_page_personal_information_section,
                          style: Theme.of(context).textTheme.headline4
                      ),
                    ),

                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['name']['required'],
                      label: _data['name']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['name']['controller'],
                          validator: _data['name']['validator'],
                          key: _data['name']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['surname']['required'],
                      label: _data['surname']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['surname']['controller'],
                          validator: _data['surname']['validator'],
                          key: _data['surname']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    InkWell(
                      child: AbsorbPointer(
                        child:
                        BuildWidgets.buildInputField(
                          context,
                          isRequired: _data['birthDate']['required'],
                          label: _data['birthDate']['label'],
                          inputField: BuildWidgets.buildBasicTextFormField(
                            context,
                            controller: _data['birthDate']['controller'],
                            validator: _data['birthDate']['validator'],
                            key: _data['birthDate']['key'],
                            enabled: true,
                          ),
                        ),
                      ),
                      onTap: () => BuildWidgets.selectDate(
                          birthDateValue: _birthDateValue,
                          context: context,
                          onPickedDate:(picked) {
                            setState(() {
                              _birthDateValue = picked;
                              _data['birthDate']['controller'].text = dateFormat.format(_birthDateValue);
                            });
                          }
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['gender']['required'],
                      label: _data['gender']['label'],
                      inputField: BuildWidgets.buildBasicDropdownFormField(
                          context,
                          items: genderList,
                          //validator: _data['gender']['validator'],
                          value: _initDropdownValue,
                          enabled: true,
                          key: _data['gender']['key'],
                          onChanged: (value) {
                            _initDropdownValue = value;
                            setState(() {
                              _initDropdownValue = value;
                            });
                          }
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['phoneNumber']['required'],
                      label: _data['phoneNumber']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['phoneNumber']['controller'],
                          validator: _data['phoneNumber']['validator'],
                          key: _data['phoneNumber']['key'],
                        //obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      },
      {
        'keys': [
        ],
        'body': SingleChildScrollView(
          child: Wrap(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  children:
                  [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                          TextConstants.registration_form_page_address_section,
                          style: Theme.of(context).textTheme.headline4
                      ),
                    ),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['street']['required'],
                      label: _data['street']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['street']['controller'],
                          validator: _data['street']['validator'],
                          key: _data['street']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['descriptiveNumber']['required'],
                      label: _data['descriptiveNumber']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['descriptiveNumber']['controller'],
                          validator: _data['descriptiveNumber']['validator'],
                          key: _data['descriptiveNumber']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['routingNumber']['required'],
                      label: _data['routingNumber']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['routingNumber']['controller'],
                          validator: _data['routingNumber']['validator'],
                          key: _data['routingNumber']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['postcode']['required'],
                      label: _data['postcode']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['postcode']['controller'],
                          validator: _data['postcode']['validator'],
                          key: _data['postcode']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['city']['required'],
                      label: _data['city']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['city']['controller'],
                          validator: _data['city']['validator'],
                          key: _data['city']['key'],
                        //obscureText: true,
                      ),
                    ),
                    SizedBox( height: 20,),
                    BuildWidgets.buildInputField(
                      context,
                      isRequired: _data['country']['required'],
                      label: _data['country']['label'],
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _data['country']['controller'],
                          validator: _data['country']['validator'],
                          key: _data['country']['key'],
                        //obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      }
    ];

    return Column(
      children: [
        OneRowHeader(title: TextConstants.registration_form_page_title),
        Expanded(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Expanded(
                  flex: 90,
                  child: PageView.builder(
                    controller: pageViewController,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: _pages.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _pages[index]['body'];
                    },
                    onPageChanged: (index) {
                      setState(() {
                        _firstPage = index == 0;
                        _lastPage = index == _pages.length - 1;
                        _selectedIndex = index;

                        if (_lastPage)
                         _nextArrowVisibility = false;
                        else
                          _nextArrowVisibility = true;

                      });
                    },
                  ),
                ),
                !_lastPage ? SizedBox() : Expanded(
                  flex: 10,
                  child: _buildSubmitButton(text: TextConstants.registration_form_page_submit),
                ),
                Expanded(
                  flex: 10,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(width: 1, color: _bottomBarColor),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                            flex: 40,
                            child: IconButton(
                              onPressed: () {
                                if (_firstPage)
                                  Navigator.pop(context);
                                pageViewController.animateToPage(pageViewController.page.toInt() - 1,
                                  duration: Duration(milliseconds: 400),
                                  curve: Curves.easeIn
                              );},
                              icon: Icon(Icons.arrow_left, size: _arrowSize, color: _bottomBarColor),
                            ),
                          ),
                          Expanded(
                              flex: 20,
                              child: _buildPageIndicator(_pages.length)
                          ),
                          Expanded(
                            flex: 40,
                            child: Visibility(
                              visible: _nextArrowVisibility,
                              child: IconButton(
                                onPressed: () {
                                  var currentPage = pageViewController.page.round();
                                  bool isValid = true;

                                  List keys  = _pages[currentPage]['keys'];
                                  keys.forEach((element) {
                                     if (!element.currentState.validate())
                                       isValid = false;
                                  });

                                  if (isValid) {
                                    pageViewController.animateToPage(pageViewController.page.toInt() + 1,
                                        duration: Duration(milliseconds: 400),
                                        curve: Curves.easeIn
                                    );
                                  }

                                  },
                                icon: Icon(Icons.arrow_right, size: _arrowSize, color: _bottomBarColor),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSubmitButton({@required String text}) {
    return Mutation(
      options: MutationOptions(
        document: gql(Mutations.createUserMutation),
        onCompleted: (resultData) {
          _registrationService.onRegistrationCompleted(resultData,
              onSuccess: (User user) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
              onError: () {
                BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.login_failed_alert);
              });
        },
      ),
      builder: (RunMutation runMutation, QueryResult result)
      {
        return BuildWidgets.buildBasicButton(context, text: TextConstants.registration_form_page_submit, onPressed: () {
          if (_formKey.currentState.validate()) {
            _registrationService.onSubmitButtonTap(
              runMutation,
              _data,
              (_initDropdownValue as GenderEnum).title,
              onError: () {
                BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.registration_failed_alert);
              },
              context: context,
            );
          }});
      },
    );
  }

  Widget _buildPageIndicator(int pageLength) {
    List<Widget> list = [];
    for (int i = 0; i < pageLength; i++) {
      list.add(i == _selectedIndex ? _indicator(true) : _indicator(false));
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: list,
    );
  }

  Widget _indicator(bool isActive) {
    return Container(
      height: 10,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 150),
        margin: EdgeInsets.symmetric(horizontal: 4.0),
        height: isActive
            ? 10:8.0,
        width: isActive
            ? 12:8.0,
        decoration: BoxDecoration(
          boxShadow: [
            isActive
                ? BoxShadow(
              color: _bottomBarColor,
              blurRadius: 4.0,
              spreadRadius: 1.0,
              offset: Offset(
                0.0,
                0.0,
              ),
            )
                : BoxShadow(
              color: Colors.transparent,
            )
          ],
          shape: BoxShape.circle,
          color: isActive ? _bottomBarColor : Color(0XFFEAEAEA),
        ),
      ),
    );
  }

}