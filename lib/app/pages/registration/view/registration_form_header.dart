import '../../common/text_constants.dart';
import 'package:familia_mobile/app/pages/about_us/view/about_us.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegistrationFormHeader extends StatefulWidget {
  const RegistrationFormHeader({Key key}) : super(key: key);

  @override
  _RegistrationFormHeaderState createState() => _RegistrationFormHeaderState();
}

class _RegistrationFormHeaderState extends State<RegistrationFormHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 5),
      color: Theme.of(context).primaryColor,
      child: Center(
        child: Text(
          TextConstants.registration_form_page_title,
          style: Theme.of(context).textTheme.headline2,
        ),
      ),
    );
  }
}
