import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/custom_mutation_result.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/app/services/session_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class SettingsService {
  Future<void>  onSubmitButtonTap(
      RunMutation runUpdateUserMutation,
      {
        @required onError,
        @required context,
        @required User user,
        @required data,
        @required initDropdownValue,
      }
    ) async {
    if (user != null) {
      runUpdateUserMutation({
        "inputData": {
          "uid": user.uid,
          "state": "ACTIVE",
          "firstName": data['name']['controller'].text.trim(),
          "lastName": data['surname']['controller'].text.trim(),
          "emails": [data['email']['controller'].text.trim()],
          "phoneNumbers": [data['phoneNumber']['controller'].text.trim()],
          "birthDate": data['birthDate']['controller'].text.trim(),
          "gender": initDropdownValue,
          "street": data['street']['controller'].text.trim(),
          "descriptiveNumber": data['descriptiveNumber']['controller'].text.trim(),
          "routingNumber": data['routingNumber']['controller'].text.trim(),
          "postcode": data['postcode']['controller'].text.trim(),
          "city": data['city']['controller'].text.trim(),
          "country": data['country']['controller'].text.trim()
        }
      });
    } else
      onError();
  }

  Future<void>  runUpdateUserMutation(
      RunMutation runUpdateUserMutation,
      {
        @required context,
        @required User user,
        @required firstName,
        @required lastName,
        @required email,
        @required phoneNumber,
        @required birthDate,
        @required gender,
        @required street,
        @required descriptiveNumber,
        @required routingNumber,
        @required postcode,
        @required city,
        @required country,
        @required onError,
      }) async {
      if (user != null) {
      runUpdateUserMutation({
        "inputData": {
          "uid": user.uid,
          "state": "ACTIVE",
          "firstName": firstName.text.trim(),
          "lastName": lastName.text.trim(),
          "emails": [email.text.trim()],
          "phoneNumbers": [phoneNumber.text.trim()],
          "birthDate": birthDate,
          "gender": gender,
          "street": street.text.trim(),
          "descriptiveNumber": descriptiveNumber.text.trim(),
          "routingNumber": routingNumber.text.trim(),
          "postcode": postcode.text.trim(),
          "city": city.text.trim(),
          "country": country.text.trim()
        }
      });
    } else
      onError();
  }

  ///Returns type of mutation result
  checkMutationResult(dynamic result, {@required mutation}) {
    if (result != null) {
      var success =  result[mutation][Mutations.SUCCESS];
      if (success) {
        return CustomMutationResult.SUCCESS;
      } else {
        return CustomMutationResult.FAIL;
      }
    }
  }

  ///Returns type of query result
  checkQueryResult(QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
    if (result.hasException) {
      return CustomQueryResult.EXCEPTION;
    }

    if (result.isLoading) {
      return CustomQueryResult.LOADING;
    }

    var success = result.data[Queries.GET_USER][Queries.SUCCESS];
    if (success) {
      sessionService.saveCookieFromApiResponse(result);
      return CustomQueryResult.SUCCESS;
    } else {
      return CustomQueryResult.FAIL;
    }
  }
}