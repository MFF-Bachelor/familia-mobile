import 'package:familia_mobile/app/pages/common/build_widgets.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_helper.dart';
import 'package:familia_mobile/app/services/graphql_service/graphql_queries.dart';
import 'package:familia_mobile/app/entities/user.dart';
import 'package:familia_mobile/app/enums/custom_query_result.dart';
import 'package:familia_mobile/app/enums/gender_enum.dart';
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/settings/service/settings_service.dart';
import 'package:familia_mobile/app/services/api_auth_service.dart';
import 'package:familia_mobile/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:timezone/timezone.dart';
import 'package:intl/src/intl/date_format.dart';

import 'package:familia_mobile/app/services/graphql_service/graphql_mutations.dart';
import '../../common/text_constants.dart';

class SettingPageBody extends StatefulWidget {
  SettingPageBody({Key key}) : super(key: key);

  @override
  _SettingPageBodyState createState() => _SettingPageBodyState();
}

class _SettingPageBodyState extends State<SettingPageBody> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GenderEnum _initDropdownValue;
  TZDateTime _birthDateValue = TZDateTime.now(currentTimezone);
  var dateFormat = DateFormat('dd.MM.yyyy');
  TZDateTime _dueDateValue = TZDateTime.now(currentTimezone);
  List<DropdownMenuItem> genderList;
  GraphGLHelper _graphGLHelper = GraphGLHelper();
  var dateTimeFormat = DateFormat('HH:mm dd.MM.yyyy');

  bool updating = false;
  User _user;

  TextEditingController _loginController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _sureNameController = TextEditingController();
  TextEditingController _birthDateController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _streetController = TextEditingController();
  TextEditingController _descriptiveNumberController = TextEditingController();
  TextEditingController _postcodeController = TextEditingController();
  TextEditingController _routingNumberController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _countryController = TextEditingController();

  final SettingsService _settingsService = SettingsService();

  void _initControllers() {
    if (_user.emails.length > 0)
      _loginController.text = _user.emails[0];
    _firstNameController.text = _user.firstName;
    _sureNameController.text = _user.lastName;
    _birthDateController.text = dateFormat.format(_user.birthDate);
    if (_user.phoneNumbers.length > 0)
      _phoneNumberController.text = _user.phoneNumbers[0];
    _streetController.text = _user.street;
    _descriptiveNumberController.text = _user.descriptiveNumber;
    _routingNumberController.text = _user.routingNumber;
    _postcodeController.text = _user.postcode;
    _cityController.text = _user.city;
    _countryController.text = _user.country;

    _initDropdownValue = _user.gender;
  }

  @override
  void initState() {
    _user = apiAuthService.currentUser;
    _initControllers();
    genderList = GenderEnum.values.map<DropdownMenuItem>((genderEnumItem) {
      return DropdownMenuItem(
          value: genderEnumItem,
          child: Text(genderEnumItem.titleSK)
      );
    }).toList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: Center(
              child: Container(
                padding: EdgeInsets.only(top: _statusBarHeight),
                child: Stack(
                  children: [
                    Column(
                      children: <Widget>[
                        OneRowHeader(
                            title: TextConstants.setting_header_text,
                            rightSidedWidget: _buildSubmitButton()
                        ),
                        Expanded(
                            child: _buildSettingsPageForm(context)
                        ),
                      ],
                    ),
                    Visibility(
                      visible: updating,
                      child: Container(
                        color: Colors.black12,
                        child: BuildWidgets.queryLoadingWidget(context)
                      )
                    ),
                  ],
                ),
              )
          )
      ),
    );
  }
  Widget _buildSettingsPageForm(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
        child: Form(
          key: _formKey,
          child: Wrap(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Column(
                  children:
                  [
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_login,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _loginController,
                          enabled: false,
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return TextConstants.non_empty_input_alert;
                            }
                            RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$');
                            return regex.hasMatch(value)
                                ? null
                                : TextConstants.wrong_email_format_alert;
                          }
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_first_name,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _firstNameController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_last_name,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _sureNameController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    InkWell(
                      child: AbsorbPointer(
                        child:
                        BuildWidgets.buildInputField(
                          context,
                          label: TextConstants.registration_form_page_birth_date,
                          inputField: BuildWidgets.buildBasicTextFormField(
                            context,
                            controller: _birthDateController,
                            enabled: true,
                          ),
                        ),
                      ),
                      onTap: () => BuildWidgets.selectDate(
                          birthDateValue: _dueDateValue,
                          context: context,
                          onPickedDate:(picked) {
                            setState(() {
                              _dueDateValue = picked;
                              _birthDateController.text = dateFormat.format(_dueDateValue);
                            });
                          }
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_phone_number,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller: _phoneNumberController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_gender,
                      inputField: BuildWidgets.buildBasicDropdownFormField(
                          context,
                          items: genderList,
                          value: _initDropdownValue,
                          enabled: true,
                          onChanged: (value) {
                            setState(() {
                              _initDropdownValue = value;
                            });
                          }
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_street,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _streetController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_descriptive_number,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _descriptiveNumberController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_routing_number,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _routingNumberController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_postcode,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          validator: (String value) {
                            if (value == null || value == '')
                              return null;
                            return (value.length > 5) ? TextConstants.wrong_number_of_characters_psc : null;
                          },
                          controller:  _postcodeController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_city,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _cityController,
                          enabled: true
                      ),
                    ),
                    SizedBox( height: 20),
                    BuildWidgets.buildInputField(
                      context,
                      label: TextConstants.registration_form_page_country,
                      inputField: BuildWidgets.buildBasicTextFormField(
                          context,
                          controller:  _countryController,
                          enabled: true
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildSubmitButton() {
    var birthDate;
    if (_birthDateController != null && _birthDateController.text.isNotEmpty)
      birthDate = TZDateTime.from(dateTimeFormat.parse("00:00" + " " + _birthDateController.text), currentTimezone);

    return Mutation(
        options: _graphGLHelper.entityMutationOptions(
            context,
            entityService: _settingsService,
            mutationDefinition: Mutations.updateUserMutation,
            mutationName: Mutations.UPDATE_USER,
            onSuccess:  (data) {
              BuildWidgets.showCustomSnackBar(context, error: false, message: TextConstants.general_success_text);
              var user = User.fromApiObject(data['updateUser']['data']);
              apiAuthService.setCurrentUser(user);
              setState(() {
                updating = false;
                Navigator.of(context).pop();
              });
            },
            onError: (_) {
              BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.general_error_text);
              setState(() {
                updating = false;
              });
            }
        ),
        builder: (RunMutation runMutation, QueryResult result) {
          return BuildWidgets.buildDoneButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    updating = true;
                  });
                  _settingsService.runUpdateUserMutation(
                    runMutation,
                    context: context,
                    email: _loginController,
                    firstName: _firstNameController,
                    lastName: _sureNameController,
                    birthDate: birthDate == null ? null : birthDate.toString(),
                    phoneNumber: _phoneNumberController,
                    city: _cityController,
                    country: _countryController,
                    descriptiveNumber: _descriptiveNumberController,
                    gender: (_initDropdownValue as GenderEnum).title,
                    postcode: _postcodeController,
                    routingNumber: _routingNumberController,
                    street: _streetController,
                    user: apiAuthService.currentUser,
                    onError: () {
                      BuildWidgets.showCustomSnackBar(context, error: true, message: TextConstants.registration_failed_alert);
                    },
                  );
                }
              });
        });
  }
}