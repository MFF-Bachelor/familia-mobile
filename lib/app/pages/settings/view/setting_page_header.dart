import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../common/text_constants.dart';

class SettingPageHeader extends StatefulWidget {
  const SettingPageHeader({Key key}) : super(key: key);

  @override
  _SettingPageHeaderState createState() => _SettingPageHeaderState();
}

class _SettingPageHeaderState extends State<SettingPageHeader> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(0.0),
          child: IconButton(
              icon: Icon(
                Icons.chevron_left,
                size: 25,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              }
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 110.0),
          child: Text(
            TextConstants.setting_header_text,
            style: TextStyle(
              color: Colors.white
            ),
          ),
        ),
      ],
    );
  }
}