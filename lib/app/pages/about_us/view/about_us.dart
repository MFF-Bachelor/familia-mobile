
import 'package:familia_mobile/app/pages/common/headers.dart';
import 'package:familia_mobile/app/pages/common/text_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutUsPage extends StatefulWidget {
  AboutUsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  @override
  Widget build(BuildContext context) {

    double _statusBarHeight = MediaQuery.of(context).padding.top;

    return Scaffold(
        body: Center(
            child: Container(
              padding: EdgeInsets.only(top: _statusBarHeight),
              child: Column(
                children: <Widget>[
                  OneRowHeader(
                      title: TextConstants.about_us_title,
                      rightSidedWidget: SizedBox()
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Text(
                          'Táto aplikácia slúži pre rodiny a ich členov ako komunikačný prostriedok a prostriedok pre ukladanie dát o rodine.',
                          style: Theme.of(context).textTheme.bodyText1,),
                    ),
                  ),
                ],
              ),
            )
        )
    );
  }

}